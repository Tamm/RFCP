USE [RF_ItemsDB]
GO
/****** Object:  Table [dbo].[tbl_code_unmannedminer]    Script Date: 06/05/2014 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_code_unmannedminer](
	[item_id] [int] NOT NULL,
	[item_code] [varchar](50) NULL,
	[item_name] [varchar](255) NULL,
	[item_client_id] [int] NULL,
	[item_icon_id] [int] NULL,
	[item_level] [int] NULL,
	[item_min_attack] [int] NULL,
	[item_max_attack] [int] NULL,
	[item_adr] [float] NULL,
	[item_adp] [float] NULL,
	[item_dsr] [float] NULL,
	[ability_desc] [varchar](255) NULL,
 CONSTRAINT [PK_tbl_code_unmannedminer] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (0, N'unbb000', N'Advanced UnmannedMiningTool(Bellato)', -2130640896, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (1, N'unbb001', N'Advanced UnmannedMiningTool(BellatoPC)', -2130640640, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (2, N'uncc000', N'Advanced UnmannedMiningTool(Cora)', -2113798144, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (3, N'uncc001', N'Advanced UnmannedMiningTool(CoraPC)', -2113797888, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (4, N'unaa000', N'Advanced UnmannedMiningTool(Accretia)', -2147483648, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (5, N'unaa001', N'Advanced UnmannedMiningTool(AccretiaPC)', -2147483392, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (6, N'unbb002', N'Normal UnmannedMiningTool(Bellato)', -2130640384, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (7, N'uncc002', N'Normal UnmannedMiningTool(Cora)', -2113797632, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (8, N'unaa002', N'Normal UnmannedMiningTool(Accretia)', -2147483136, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (9, N'unbb003', N'Light UnmannedMiningTool(Bellato)', -2130640128, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (10, N'uncc003', N'Light UnmannedMiningTool(Cora)', -2113797376, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (11, N'unaa003', N'Light UnmannedMiningTool(Accretia)', -2147482880, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (12, N'uncsa01', N'Advanced Un-manned MineTool(Bellato)', -2112749567, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (13, N'uncsa02', N'Advanced Un-manned MineTool(Bellato Lan House Only)', -2112749566, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (14, N'uncsa03', N'Advanced Un-manned MineTool(Cora)', -2112749565, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (15, N'uncsa04', N'Advanced Un-manned MineTool(Cora Lan House Only)', -2112749564, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (16, N'uncsa05', N'Advanced Un-manned MineTool(Accretia)', -2112749563, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (17, N'uncsa06', N'Advanced Un-manned MineTool(Accretia Lan House Only)', -2112749562, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (18, N'uncsa07', N'Normal Un-manned MineTool(Bellato)', -2112749561, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (19, N'uncsa08', N'Normal Un-manned MineTool(Cora)', -2112749560, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (20, N'uncsa09', N'Normal Un-manned MineTool(Accretia)', -2112749559, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (21, N'uncsa10', N'Light Un-manned MineTool(Bellato)', -2112749552, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (22, N'uncsa11', N'Light Un-manned MineTool(Cora)', -2112749551, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (23, N'uncsa12', N'Light Un-manned MineTool(Accretia)', -2112749550, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (24, N'unbpc01', N'Cafe Only Advanced UMT', -2129722879, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (25, N'uncpc01', N'Cafe Only Advanced UMT', -2112945663, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_unmannedminer] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (26, N'unapc01', N'Cafe Only Advanced UMT', -2146500095, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
