ALTER DATABASE [RF_HISTORY] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RF_HISTORY].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RF_HISTORY] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [RF_HISTORY] SET ANSI_NULLS OFF
GO
ALTER DATABASE [RF_HISTORY] SET ANSI_PADDING OFF
GO
ALTER DATABASE [RF_HISTORY] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [RF_HISTORY] SET ARITHABORT OFF
GO
ALTER DATABASE [RF_HISTORY] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [RF_HISTORY] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [RF_HISTORY] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [RF_HISTORY] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [RF_HISTORY] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [RF_HISTORY] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [RF_HISTORY] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [RF_HISTORY] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [RF_HISTORY] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [RF_HISTORY] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [RF_HISTORY] SET  DISABLE_BROKER
GO
ALTER DATABASE [RF_HISTORY] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [RF_HISTORY] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [RF_HISTORY] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [RF_HISTORY] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [RF_HISTORY] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [RF_HISTORY] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [RF_HISTORY] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [RF_HISTORY] SET  READ_WRITE
GO
ALTER DATABASE [RF_HISTORY] SET RECOVERY FULL
GO
ALTER DATABASE [RF_HISTORY] SET  MULTI_USER
GO
ALTER DATABASE [RF_HISTORY] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [RF_HISTORY] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'RF_HISTORY', N'ON'
GO
USE [RF_HISTORY]
GO
/****** Object:  User [gamecp]    Script Date: 06/05/2014 21:39:25 ******/
CREATE USER [gamecp] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[ACC_ItemLogs]    Script Date: 06/05/2014 21:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACC_ItemLogs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[serial] [int] NOT NULL,
	[k_value] [int] NOT NULL,
	[d_value] [bigint] NOT NULL,
	[u_value] [int] NOT NULL,
	[t_value] [int] NOT NULL,
	[s_value] [bigint] NOT NULL,
	[s_column] [varchar](10) NOT NULL,
	[table] [varchar](50) NULL,
 CONSTRAINT [PK_ACC_ItemLogs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BELL_ItemLogs]    Script Date: 06/05/2014 21:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BELL_ItemLogs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[serial] [int] NOT NULL,
	[k_value] [int] NOT NULL,
	[d_value] [bigint] NOT NULL,
	[u_value] [int] NOT NULL,
	[t_value] [int] NOT NULL,
	[s_value] [bigint] NOT NULL,
	[s_column] [varchar](10) NOT NULL,
	[table] [varchar](50) NULL,
 CONSTRAINT [PK_BELL_ItemLogs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_runs]    Script Date: 06/05/2014 21:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_runs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NOT NULL,
	[records_processed] [int] NOT NULL,
	[LastConnTime] [int] NOT NULL,
	[records_deleted] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CORA_ItemLogs]    Script Date: 06/05/2014 21:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORA_ItemLogs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[serial] [int] NOT NULL,
	[k_value] [int] NOT NULL,
	[d_value] [bigint] NOT NULL,
	[u_value] [int] NOT NULL,
	[t_value] [int] NOT NULL,
	[s_value] [bigint] NOT NULL,
	[s_column] [varchar](10) NOT NULL,
	[table] [varchar](50) NULL,
 CONSTRAINT [PK_ItemLogs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[CORA_DUPES]    Script Date: 06/05/2014 21:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CORA_DUPES]
AS 
SELECT [id]
      ,[serial]
      ,CASE WHEN A.[table] = 'tbl_base' OR A.[table] = 'tbl_general' OR A.[table] = 'tbl_inven' THEN (SELECT Name FROM [RF_WORLD].dbo.tbl_base B Where B.Serial = A.Serial) ELSE '' END AS char_name 
      ,[k_value]
      ,[d_value]
      ,[u_value]
      ,[t_value]
      ,[s_value]
      ,[s_column]
      ,[table]
      ,(convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value)) AS s_unique
  FROM [RF_HISTORY].[dbo].[CORA_ItemLogs] A
  WHERE (convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value)) IN
  (
    SELECT (convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value)) AS s_unique
	FROM [RF_HISTORY].[dbo].[CORA_ItemLogs]
	GROUP BY (convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value))
	HAVING COUNT((convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value))) > 1
  )
GO
/****** Object:  View [dbo].[BELL_DUPES]    Script Date: 06/05/2014 21:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[BELL_DUPES]
AS 
SELECT [id]
      ,[serial]
      ,CASE WHEN A.[table] = 'tbl_base' OR A.[table] = 'tbl_general' OR A.[table] = 'tbl_inven' THEN (SELECT Name FROM [RF_WORLD].dbo.tbl_base B Where B.Serial = A.Serial) ELSE '' END AS char_name 
      ,[k_value]
      ,[d_value]
      ,[u_value]
      ,[t_value]
      ,[s_value]
      ,[s_column]
      ,[table]
      ,(convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value)) AS s_unique
  FROM [RF_HISTORY].[dbo].[BELL_ItemLogs] A
  WHERE (convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value)) IN
  (
    SELECT (convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value)) AS s_unique
	FROM [RF_HISTORY].[dbo].[BELL_ItemLogs]
	GROUP BY (convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value))
	HAVING COUNT((convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value))) > 1
  )
GO
/****** Object:  View [dbo].[ACC_DUPES]    Script Date: 06/05/2014 21:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ACC_DUPES]
AS 
SELECT [id]
      ,[serial]
      ,CASE WHEN A.[table] = 'tbl_base' OR A.[table] = 'tbl_general' OR A.[table] = 'tbl_inven' THEN (SELECT Name FROM [RF_WORLD].dbo.tbl_base B Where B.Serial = A.Serial) ELSE '' END AS char_name 
      ,[k_value]
      ,[d_value]
      ,[u_value]
      ,[t_value]
      ,[s_value]
      ,[s_column]
      ,[table]
      ,(convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value)) AS s_unique
  FROM [RF_HISTORY].[dbo].[ACC_ItemLogs] A
  WHERE (convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value)) IN
  (
    SELECT (convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value)) AS s_unique
	FROM [RF_HISTORY].[dbo].[ACC_ItemLogs]
	GROUP BY (convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value))
	HAVING COUNT((convert(varchar, ([k_value] & 0xffffff00)) + CONVERT(varchar, s_value))) > 1
  )
GO
