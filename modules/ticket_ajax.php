<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "HIDDEN";
  $moduleLabel = "ticket ajax";
  $permission = "restricted";
  return;
}
/**
 * @author Mauro Tamm   @ mauro.tamm@gmail.com
 * @author Agony @ agony@nxtdeveloper.com
 * @copyright 2014 http://nxtdeveloper.com/
 * @ver 1.0
 */
$out_type = OUTPUT_JSON;
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $status = isset($_POST['status']) && ctype_digit($_POST['status']) ? intval($_POST['status']) : "";
    $id = isset($_POST['id']) ? $_POST['id'] : "";

    if (($status != "" && $status > -1 && $status < 4 && $id != "") || $status === 0)
    {

      $sql = "UPDATE gamecp_ticket_entry SET status = ? WHERE id = ?";
      $param = array ($status, $id);
      $tdata = sqlsrv_query(connectdb(RFCP), $sql, $param);
      if ($tdata === false)
      {
        $out_json["statuserror"] = 'Error Updating Status';
      }
      else
      {
        $out_json["error"] = False;
      }
    }

    $page = isset($_POST['page']) ? $_POST['page'] : "";
    if ($page == "update")
    {
      $permission = isset($_POST['permission']) ? $_POST['permission'] : "";
      $category = isset($_POST['category']) ? $_POST['category'] : "";
      $id = isset($_POST['id']) ? $_POST['id'] : "";
      $defaultid = isset($_POST['defaultid']) ? $_POST['defaultid'] : "";
      $aserial = isset($_POST['aserial']) ? $_POST['aserial'] : "";
      $rank = isset($_POST['rank']) ? $_POST['rank'] : "";
      $defaultaserial = isset($_POST['defaultaserial']) ? $_POST['defaultaserial'] : "";
      $type = isset($_POST['type']) ? $_POST['type'] : 0;
      if ($id != "")
      {
        $csql = "UPDATE ticket_categories SET id = ? WHERE id = ?";
        $cdata = sqlsrv_query(connectdb(RFCP), $csql, array ($id, $defaultid));
        if ($cdata === false)
        {
          $out_json["error"] = get_notification_html(array ('Error updating caregory id'), ERROR);
        }
        else
        {
          $out_json["id"] = $id;
        }
        gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - UPDATED Ticket Category id", 1);
      }
      if ($category != "")
      {
        $csql = "UPDATE ticket_categories SET category = ? WHERE id = ?";
        $cdata = sqlsrv_query(connectdb(RFCP), $csql, array ($category, $defaultid));
        if ($cdata === false)
        {
          $out_json["error"] = get_notification_html(array ('Error updating caregory name'), ERROR);
        }
        else
        {
          $out_json["category"] = $category;
        }
        gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - UPDATED Ticket Category", 1);
      }
      if ($permission != "")
      {
        $psql = "UPDATE ticket_categories SET permission = ? WHERE id = ?";
        $ticketsql = "UPDATE gamecp_ticket_entry SET permission = ? WHERE type = ?";
        $pdata = sqlsrv_query(connectdb(RFCP), $psql, array ($permission, $defaultid));
        $ticketstmt = sqlsrv_query(connectdb(RFCP), $ticketsql, array ($permission,
          $defaultid));

        if ($pdata === false)
        {
          $out_json["error"] = get_notification_html(array ('Error updating caregory permission'), ERROR);
        }
        else
        {
          $out_json["permission"] = $permission;
        }
        gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - UPDATED Ticket Category permission", 1);
      }

      if ($aserial != "")
      {
        $sql = "UPDATE ticket_permission SET aserial = ? WHERE aserial = ?";
        $stmt = sqlsrv_query(connectdb(RFCP), $sql, array ($aserial, $defaultaserial));
        if ($stmt === false)
        {
          $out_json["error"] = get_notification_html(array ('Error updating permission aserial'), ERROR);
        }
        else
        {
          $out_json["aserial"] = $aserial;
        }
        gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - UPDATED Ticket Permission aserial", 1);
      }
      if ($rank != "")
      {
        $sql = "UPDATE ticket_permission SET rank = ? WHERE aserial = ?";
        $stmt = sqlsrv_query(connectdb(RFCP), $sql, array ($rank, $defaultaserial));
        if ($stmt === false)
        {
          $out_json["error"] = get_notification_html(array ('Error updating permission rank'), ERROR);
        }
        else
        {
          $out_json["rank"] = $rank;
        }
        gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - UPDATED Ticket Permission rank for " . $aserial, 1);
      }
    }
  }
}
