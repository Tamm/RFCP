<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Administration";
  $moduleLabel = "Configuration";
  $permission = "administrator";
  return;
}
if ($this_script == $script_name)
{
  $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
  if ($userdata -> loggedin == True && $userdata -> is_superadmin())
  {
    if (empty($page))
    {
      $out .= "<form method=\"post\" class=\"ink-form config_form\">";
      $out .= "<table width=\"100%\" border=\"0\">";

      $config_items = get_configuration();

      if ($config_items["error"] === True)
      {
        $out .= get_notification_html($config_items["errorMessage"], ERROR);
      }
      else
      {
        foreach ($config_items["data"] as $key => $row)
        {
          $out .= "<tr>";
          $out .= "<td width=\"60%\"><b>" . ltrim(rtrim(str_replace("_", " ", $row['config_name']))) . "</b><br/><p class=\"tip\">" . $row['config_description'] . "</p></i></td>";
          $out .= "<td width=\"40%\"><input type=\"text\" name=\"config[" . $row['config_name'] . "]\" value=\"" . $row['config_value'] . "\" style=\"width:100%;\"/></td>";
          $out .= "</tr>";
        }
      }

      $out .= "<tr>";
      $out .= "<td colspan=\"2\" style=\"text-align: center;\"><input type=\"hidden\" name=\"page\" value=\"update\"/><input type=\"submit\" name=\"submit\" value=\"Update Config\" class=\"ink-button\"/></td>";
      $out .= "</tr>";
      $out .= "</table>";
      $out .= "</form>";
    }
    else
    {
      if ($page == "update")
      {
        $config_form = isset($_POST['config']) ? $_POST['config'] : "";
        if (is_array($config_form))
        {
          $config_attempt = update_configuration($config_form);
          if ($config_attempt["error"] == True)
          {
            $out .= get_notification_html($config_attempt["errorMessage"], ERROR);
          }
          else
          {
            $out .= get_notification_html("Configuration has been successfully updated.", SUCCESS);
            $redirect = PREVIOUS_PAGE_SHORT;
            gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - Updated the GameCP config", 1);
          }
        }
      }
      else
      {
        $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
        $redirect = INDEX_PAGE_SHORT;
      }
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
