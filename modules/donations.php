<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Novus Market";
  $moduleLabel = "Recharge Credits";
  $permission = "player";
  return;
}

if ($this_script == $script_name)
{
  $exit_stage = 0;
  if ($userdata -> loggedin == True)
  {
    $bonus_enabled = isset($config['bonus_enabled']) ? ($config['bonus_enabled'] == 1) : False;
    $attempt = get_bonus_items_from_config();
    $bonus_items = array ();
    if ($attempt["error"] == True || count($attempt["rows"]) < 1)
    {
      $bonus_enabled = False;
    }
    else
    {
      $bonus_items = $attempt["rows"];
    }

    $bonus_width = count($bonus_items) > 3 ? ((50 * count($bonus_items)) . "px") : "170px";

    $tottime = time() - $userdata -> createtime -> getTimestamp();
    $username = !( $tottime <= 604800 || $userdata -> createtime == "" ) && isset($_POST['username']) ? $_POST['username'] : "";

    if ($username == "" || !preg_match(REGEX_USERNAME, $username))
    {
      $custom = $userdata -> serial;
    }
    else
    {
      $attempt = get_account_serial($username);

      if ($attempt["error"] == True)
      {
        $exit_stage = 1;
        get_notification_html("SQL Error while trying to query the database", ERROR);
      }
      else
      {
        $custom = $attempt["serial"];
        if ($custom == -1)
        {
          $username = "[USER NOT FOUND]";
          $custom = $userdata -> serial;
        }
      }
    }

    $bonus_serial_select = isset($_POST["character_select"]) ? $_POST["character_select"] : "";
    if ($bonus_serial_select != "")
    {
      $serial_select = explode("_", $bonus_serial_select);
      $serial_select = $serial_select[0];
      $serial_select = intval($serial_select);
      $attempt = set_bonus_item_character($userdata -> serial, $serial_select);
      if ($attempt["error"] == True)
      {
        $out .= get_notification_html($attempt["errorMessage"], ERROR);
      }
    }

    $selected_char_race = 1;

    $custom = antiject($custom);
    if ($exit_stage == 0)
    {
      $out .= "<center>";
      $out .= "<div class=\"market_current_points_text\" style=\"width:100%; font-size: 14pt; text-align: center;\">You currently have <span style=\"color: #4259FF; font-weight:bold;\">" . number_format($userdata -> credits, 2) . "</span> Novus Credits( <img src=\"./framework/img/currency.png\"> )</div>";
      $out .= "<form class=\"ink-form\" method=\"post\" action=\"" . $script_name . "?action=" . $_GET['action'] . "\">";
      $out .= "If you'r donating Item Mall Credits for a friend, enter their <b><u>username</u></b> below.<br /><br />";
      $out .= "<input type=\"text\" name=\"username\"> <input class=\"ink-button\" type=\"submit\" class=\"submit\" value=\"Change\">";
      $out .= "</form>";
      $out .= "<br>";
      $out .= "<div style=\"color:red;\"><b>Note: Donations are final and are not eligible for refund.</b></div>";
      $out .= "<div style=\"color:red;\"><b>Note: Donators are rewarded virtual credits for their support.</b></div>";
      $out .= "<br>";
      $out .= "</center>";
      if ($bonus_enabled)
      {
        $current_selected = get_bonus_item_character($userdata -> serial);
        $current_selected = $current_selected["char_serial"];
        if ($current_selected == -1)
        {
          $attempt = get_characters($userdata -> serial);
          if (count($attempt["rows"]) > 0)
          {
            $attempt2 = set_bonus_item_character($userdata -> serial, $attempt["rows"][0][0]);
            if ($attempt2["error"] == False)
            {
              $current_selected = $attempt["rows"][0][0];
            }
          }
        }
        if ($current_selected > -1)
        {
          $char_info = get_character_info($current_selected);
          $char_info = $char_info["char"];
          $out .= "<div class=\"bonus_char_selection\">";
          $out .= "<form class=\"ink-form\" method=\"post\">";
          $out .= get_bonus_character_select_menu();
          $out .= "<input class=\"ink-button\" name=\"change_bonus_char_submit\" type=\"submit\" value=\"Change\" style=\"display: inline-block;\"/></form>";
          $out .= "<div style=\"margin-top: 0px;\">Currently selected for bonus: <span style=\"font-weight: bold; color: green;\">" . $char_info["Name"] . "</span></div>";
          $out .= "</div>";

          $selected_char_race = parse_base_race($char_info["RaceNum"]);
        }
      }

      calculate_credits($config['donations_credit_muntiplier'], $config['donations_number_of_pay_options'], $config['donations_start_price'], $config['donations_start_credits'], false);
      $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"60%\" align=\"center\">";
      if ($username != "")
      {
        $out .= "<tr>";
        $out .= "<td colspan=\"5\" style=\"text-align: center; font-size: 15px;\">You are Donating these credits for <b>" . $username . "</b></td>";
        $out .= "</tr>";
      }
      $out .= "<tr>";
      $out .= "<td class=\"\" align=\"center\">Amount</td>";
      $out .= "<td class=\"\" align=\"center\">Credits</td>";
      $out .= "<td class=\"\" align=\"center\">Bonus Credits</td>";
      $out .= "<td class=\"\" style=\"text-align: center;\">Total Credits</td>";
      if ($bonus_enabled)
      {
        $out .= "<td class=\"\" align=\"center\" width=\"" . $bonus_width . "\">Bonus Item(s)</td>";
      }
      $out .= "<td class=\"\" style=\"text-align: center; width: 125px;\"></td>";
      $out .= "</tr>";
      $i = 1;
      while ($i < count($c_price))
      {
        $bonus_html = "";
        foreach ($bonus_items as $key => $row)
        {
          if ($row['bonus_race'] == 0 || $row['bonus_race'] == $selected_char_race ||
          (($selected_char_race == 1 || $selected_char_race == 2) && $row['bonus_race'] == 4))
          {
            $k_value = $row['bonus_item_k'];
            $u_value = $row['bonus_item_u'];
            $item_details = get_item_details($k_value, $u_value);
            $upgrades = $item_details["images"];
            $attempt = get_item_info_from_id($item_details["type"], $item_details["id"]);
            $item_code = $attempt["item"]["item_code"];
            $item_name = str_replace("_", " ", $attempt["item"]["item_name"]);
            $image = get_item_image($attempt["item"]["item_icon_id"], $item_details["type"]);

            $temp_amount = $row['bonus_item_d'] == 0 ? $row['bonus_item_d'] + 1 : $row['bonus_item_d'];
            $quantity = $temp_amount > 0 ? ("<br>Quantity: " . ($temp_amount * $i)) : "";
            $time = $row['bonus_item_t'] > 0 ? ("<br>Rental Time: " . ($row['bonus_item_t'] / 3600) . " Hrs") : "";

            $bonus_html .= "<div class=\"tooltip no-select bonus_item_block\" ";
            $bonus_html .= "data-tip-html=\"" . filter_string_for_html(filter_string_for_html($item_name)) . $quantity . $time . "\" data-tip-color: \"blue\">";
            $bonus_html .= "<img class=\"market_item_image\" src=\"" . $image . "\" style='height: 40px; width: 40px;' />";
            $bonus_html .= "<div class=\"bonus_item_quantity\">x" . ($temp_amount * $i) . "</div>";
            $bonus_html .= "</div>";
          }
        }

        $out .= "<tr>";
        $out .= "<td class=\"\" align=\"center\">\$" . number_format($c_price[$i], 2, ".", "") . "</td>";
        $out .= "<td class=\"\" align=\"center\">" . number_format($c_credits[$i]) . "</td>";
        $out .= "<td class=\"\" align=\"center\">" . number_format($c_bonus[$i]) . "</td>";
        $out .= "<td class=\"\" align=\"center\"><b>" . number_format($c_total[$i]) . "</b></td>";
        if ($bonus_enabled)
        {
          $out .= "<td class=\"\" align=\"center\">" . $bonus_html . "</td>";
        }
        $out .= "<td class=\"\" align=\"center\"><form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\">" . get_paypal_html(number_format($c_price[$i], 2, ".", ""), $c_total[$i], $custom) . "</form></td>";
        $out .= "</tr>";
        ++$i;
      }
      $out .= "</table>";
      $out .= "<script type=\"text/javascript\">";
      $out .= "activate_tooltips();";
      $out .= "</script>";
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
