<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Novus Market";
  $moduleLabel = "Premium service";
  $permission = "player";
  return;
}
define("PRICE_LOW", 970);
define("PRICE_MED", 1750);
define("PRICE_HIGH", 2500);

if ($this_script == $script_name)
{
  $exit_stage = 0;
  if ($isuser == true)
  {
    $userCredits = $userdata -> credits;
    $date = new DateTime();

    if ($userdata -> premium)
    {
      $premiumStatusData = $userdata -> premiumData;
      $date = new DateTime($userdata -> premiumData['EndDate'] -> format('Y-m-d H:i:s'));
    }
    if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'premium_subscribe' && isset($_POST['subscribe']))
    {
      $out_type = OUTPUT_JSON;
      $price = 0;
      switch ($_POST['subscribe'])
      {
        case 30:
          $price = PRICE_LOW;
          $date -> add(new DateInterval('P30D'));
          break;
        case 60:
          $price = PRICE_MED;
          $date -> add(new DateInterval('P60D'));
          break;
        case 90:
          $price = PRICE_HIGH;
          $date -> add(new DateInterval('P90D'));
          break;
      }

      $error = array ();
      if ($price > $userCredits)
      {
        $error[] = 'Not enough credits';
      }
      else
      {
        $attempt = remove_user_credits($userdata -> serial, $price);
        $userdata -> credits = $userdata -> credits - $price;
        $userCredits = $userdata -> credits;
        if ($attempt["error"])
        {
          $error[] = $attempt['errorMessage'];
        }
        else
        {
          $subscribeStatus = updateSubscriptionStatus($userdata -> username, $date -> format('Y-m-d H:i:s'));

          if ($subscribeStatus['error'])
          {
            $error[] = $subscribeStatus['errorMessage'];
          }
        }
      }
      $out_json = array (
        'error' => $error ? true : false,
        'date' => $error ? implode(', ', $error) : $date -> format('Y-m-d H:i:s'),
        'ncTotal' => number_format($userCredits, 2)
      );
    }
    $out .= "<div class='ink-alert basic error' id='subscribeError' style='display:none;'><p></p></div>";
    $out .= "<div class='market_current_points_text' style='width:100%; font-size: 14pt; text-align: center;'>You currently have <span style='color: #4259FF; font-weight:bold;' id='ncAmount'>" . number_format($userCredits, 2) . "</span> Novus Credits( <img src='./framework/img/currency.png'> )</div>";
    $out .= "<b>Premium accounts recieve following bonuses:</b></br></br>";
    $out .= "<table class='ink-form' cellpadding='3' cellspacing='1' border='0' width='50%' style='font-size: 12pt;'>";
    $out .= "<tr>";
    $out .= "<td>Experience</td>";
    $out .= "<td>25%</td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td>Mine speed</td>";
    $out .= "<td>20%</td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td>Loot rate</td>";
    $out .= "<td>15%</td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td>Animus xp</td>";
    $out .= "<td>20%</td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td>Skill/force PT</td>";
    $out .= "<td>20%</td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td>Dark Hole reward</td>";
    $out .= "<td>20%</td>";
    $out .= "</tr>";
    $out .= "</table>";
    $out .= "</br></br>";
    $btnValue = 'Purchase premium service';

    $showPremiumNotice = 'none';
    if ($userdata -> premium)
    {
      $btnValue = 'Extend premium service';
      $showPremiumNotice = 'inline';
    }
    $out .= "<span id = premiumNotice style='display: " . $showPremiumNotice . ";'> <b>Premium service active until: </b><span id ='subscribeDate'>" . $date -> format('Y-m-d H:i:s') . "</span> (UTC +0)</br></br></span>";
    $out .= "<b>Choose your subscription plan:</b>";
    $out .= "</br></br>";
    $out .= "<form class='ink-form' id='subscribeForm'>";
    $out .= "<table class='ink-form' cellpadding='3' cellspacing='1' border='0' width='30%' style='font-size: 12pt;'>";
    $out .= "<tr>";
    $out .= "<td><input type='radio' name='subscribe' value='30' checked='checked'></td>";
    $out .= "<td><b style='color:	#A67D3D;'>30 days of premium service</b></td>";
    $out .= "<td><img src='./framework/img/currency.png'> " . PRICE_LOW . "</td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td><input type='radio' name='subscribe' value='60'></td>";
    $out .= "<td><b style='color: #868585;'>60 days of premium service</b></td>";
    $out .= "<td><img src='./framework/img/currency.png'> " . PRICE_MED . "</td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td><input type='radio' name='subscribe' value='90'></td>";
    $out .= "<td><b style='color: #bba439;'>90 days of premium service</b></td>";
    $out .= "<td><img src='./framework/img/currency.png'> " . PRICE_HIGH . "</td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "</tr>";
    $out .= "</table>";
    $out .= "</br></br>";
    $out .= "<input type='button' class='ink-button' id='premiumButton' value='" . $btnValue . "' onClick='premiumPurchase()'>";
    $out .= "</form>";
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}

