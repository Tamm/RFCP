<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Edit";
  $moduleLabel = "Manage Bans";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $max_pages = 10;
    $top_limit = 60;
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $search_fun = isset($_POST['search_fun']) ? $_POST['search_fun'] : "";
    $enable_exit = false;
    $enable_account = false;
    $query_p2 = "";
    $search_query = "";

    $links = array ();
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "'>View Ban List</a>";
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "&page=addedit'>Ban User</a>";

    if ($page == "")
    {
      $active_index = 0;
    }
    elseif ($page == "addedit")
    {
      $active_index = 1;
    }
    elseif ($page == "delete" || $page == "delete_user")
    {
      $active_index = 2;
      $links[] = "<a href='#'>Delete Ban</a>";
    }

    $out .= generate_module_nav_html($links, $active_index);

    if (empty($page))
    {
      $out .= "<form class=\"ink-form\" method=\"post\">";
      $out .= "<table class=\"tborder\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
      $out .= "<tr>";
      $out .= "<td  colspan=\"2\">Look up a banned user</td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td  width=\"1%\" nowrap>Account Name: </td>";
      $out .= "<td ><input type=\"text\" name=\"account_name\"/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td  width=\"1%\" nowrap>Account Serial: </td>";
      $out .= "<td ><input type=\"text\" name=\"account_serial\"/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td colspan=\"2\" nowrap><input class=\"ink-button\" type=\"submit\" name=\"search_fun\" value=\"Search\" style=\"margin-left: 0px;\"/></td>";
      $out .= "</tr>";
      $out .= "</table>";
      $out .= "</form>";

      $and = "";
      if ($search_fun != "")
      {
        $account_serial = isset($_POST['account_serial']) && ctype_digit($_POST['account_serial']) ? intval($_POST['account_serial']) : 0;
        $account_name = isset($_POST['account_name']) ? antiject($_POST['account_name']) : "";
        $chat_ban = isset($_POST['chat_ban']) ? antiject($_POST['chat_ban']) : "";
        if ($account_serial == 0 && $account_name == "")
        {
          $enable_exit = true;
          $out .= get_notification_html("You must enter a account name or serial to do a search", ERROR);
        }
        if ($enable_exit === false)
        {
          $search_query = "";
          if ($account_name != "" && $account_serial == 0)
          {
            $search_query .= " U.id = convert(binary,'{$account_name}') ";
          }
          else
          {
            $search_query .= " U.Serial = '{$account_serial}' ";
          }
        }
      }

      if ($search_query != "")
      {
        $and = " AND ";
      }

      include( "./core/pagination.php" );

      $sql = "SELECT CONVERT(varchar, U.id) AS username, B.nAccountSerial, B.dtStartDate, B.nPeriod, B.nKind, B.szReason, B.GMWriter,";
      $sql .= " U.Serial FROM tbl_UserBan AS B INNER JOIN tbl_UserAccount AS U ON U.serial = B.nAccountSerial WHERE " . $search_query . $and;
      $sql .= " U.Serial NOT IN ( SELECT TOP [OFFSET] U.Serial FROM tbl_UserBan AS B INNER JOIN tbl_UserAccount AS U ON U.serial = B.nAccountSerial ";
      if ($search_query != "")
      {
        $search_query = "WHERE " . $search_query;
      }
      $sql .= $search_query . " ORDER BY B.dtStartDate DESC) ORDER BY B.dtStartDate DESC";
      $sql_count = "SELECT COUNT(U.id) FROM tbl_UserBan AS B INNER JOIN tbl_UserAccount AS U ON U.serial = B.nAccountSerial " . $search_query;

      $url = "" . $script_name . "?action=" . $_GET['action'];

      $pager = new Pagination(USER, $sql, $sql_count, $url, array (), array (), $page_size = 50, $links_to_show = 11);
      $results = $pager -> get_data();
      $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
      $out .= "<tr>";
      $out .= "<td  style=\"text-align: center;\" nowrap>Serial</td>";
      $out .= "<td  nowrap>Start Date</td>";
      $out .= "<td  nowrap>Account Name</td>";
      $out .= "<td  nowrap>Period</td>";
      $out .= "<td  style=\"text-align: center;\" nowrap>Chat Ban</td>";
      $out .= "<td  nowrap>Reason</td>";
      $out .= "<td  style=\"text-align: center;\" nowrap>Game Master</td>";
      $out .= "<td  style=\"text-align: center;\" colspan=\"2\" nowrap>Options</td>";
      $out .= "</tr>";
      foreach ($results["rows"] as $key => $row)
      {
        $gmwriter = filter_string_for_html($row['GMWriter']);
        if ($gmwriter == "WS0")
        {
          $gmwriter = "-";
          $row['szReason'] = "Auto-banned by FireGuard";
        }
        $out .= "<tr>";
        $out .= "<td style=\"text-align: center;\" nowrap>" . $row['Serial'] . "</td>";
        $out .= "<td nowrap>" . $row['dtStartDate'] -> format('d/m/Y') . "</td>";
        $out .= "<td nowrap>" . antiject($row['username']) . "</td>";
        $out .= "<td nowrap>" . $row['nPeriod'] . "</td>";
        $out .= "<td style=\"text-align: center;\" nowrap>" . ( $row['nKind'] == 1 ? "Yes " : "No" ) . "</td>";
        $out .= "<td nowrap>" . antiject($row['szReason']) . "</td>";
        $out .= "<td nowrap>" . antiject($gmwriter) . "</td>";
        $out .= "<td style=\"text-align: center;\" nowrap><a href=\"./" . $script_name . "?action=" . $_GET['action'] . "&page=addedit&edit_ban_serial=" . $row['Serial'] . "\" style=\"text-decoration: none;\">Edit</a></td>";
        $out .= "<td style=\"text-align: center;\" nowrap><a href=\"./" . $script_name . "?action=" . $_GET['action'] . "&page=delete&ban_serial=" . $row['Serial'] . "\" style=\"text-decoration: none;\">Delete</a></td>";
        $out .= "</tr>";
      }
      if (count($results["rows"]) <= 0)
      {
        $out .= "<tr>";
        $out .= "<td  colspan=\"9\" style=\"text-align: center; font-weight: bold;\">No banned accounts found.</td>";
        $out .= "</tr>";
      }
      else
      {
        $out .= "<tr>";
        $out .= "<td  colspan=\"9\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
        $out .= "</tr>";
      }

      $out .= "</table>";
    }
    else if ($page == "addedit")
    {
      $display_form = true;
      $do_process = 0;
      $exit_process = false;
      $exit_text = "";
      $add_submit = isset($_POST['add_submit']) ? 1 : 0;
      $edit_submit = isset($_POST['edit_submit']) ? 1 : 0;
      $messages = array ();

      if (isset($_POST['edit_ban_serial']) || isset($_GET['edit_ban_serial']))
      {
        $edit_ban_serial = isset($_POST['edit_ban_serial']) ? intval($_POST['edit_ban_serial']) : intval($_GET['edit_ban_serial']);
        if (!( $edit_ban_serial ))
        {
          $edit_ban_serial = "";
        }
      }
      else
      {
        $edit_ban_serial = "";
      }
      $ban_serial = isset($_POST['ban_serial']) ? $_POST['ban_serial'] : 0;
      $ban_name = isset($_POST['ban_name']) ? $_POST['ban_name'] : "";
      $ban_period = isset($_POST['ban_period']) && ctype_digit($_POST['ban_period']) ? antiject($_POST['ban_period']) : 119988;
      $ban_period = 119988 < $ban_period ? "119988" : $ban_period;
      $ban_reason = isset($_POST['ban_reason']) ? antiject($_POST['ban_reason']) : "";
      if (isset($_GET['ban_reason']))
      {
        $ban_reason = isset($_GET['ban_reason']) ? antiject($_GET['ban_reason']) : "";
      }
      $ban_chat = isset($_POST['ban_chat']) ? antiject($_POST['ban_chat']) : 0;
      if (isset($_GET['serial']))
      {
        $ban_serial = $_GET['serial'];
      }
      if ($ban_name != "")
      {
        $ban_serial = get_account_serial($ban_name);
        $ban_serial = $ban_serial['serial'];
      }
      if ($add_submit == 1 || $edit_submit == 1)
      {
        $do_process = 1;
      }
      if ($edit_ban_serial != "")
      {
        $page_mode = "edit_submit";
        $submit_name = "Update Account";
        $this_mode_title = "Edit Banned Account";
        $disable = " disabled";
        if ($do_process == 0)
        {
          $attempt = get_ban_record($edit_ban_serial);
          if ($attempt["error"] == True)
          {
            $display_form = false;
            $out .= get_notification_html($attempt["errorMessage"], ERROR);
          }
          else
          {
            $info = $attempt["row"];
            $ban_period = $info['nPeriod'];
            $ban_reason = $info['szReason'];
            $ban_chat = $info['nKind'];
          }
        }
        $ban_serial = $edit_ban_serial;
      }
      else
      {
        $page_mode = "add_submit";
        $submit_name = "Ban Account";
        $this_mode_title = "New Account Ban";
        $disable = "";
      }
      if ($do_process == 1)
      {
        if ($ban_serial == 0)
        {
          $messages[] = "You have not provided a user serial";
        }
        else
        {
          $ban_serial = explode(",", $ban_serial);
          foreach ($ban_serial as $key => $serial)
          {
            if (ctype_digit($serial))
            {
              $ban_serial[$key] = intval($serial);
            }
            else
            {
              $messages[] = filter_string_for_html($ban_serial) . " is not a valid serial number";
            }
          }
        }
        if ($ban_period == 0)
        {
          $messages[] = "You must provide a ban period (i.e. 119988)<br/>";
        }
        if ($ban_reason == "")
        {
          $messages[] = "You must provide a reason for this ban<br/>";
        }
        if (count($messages) == 0)
        {
          foreach ($ban_serial as $key => $serial)
          {
            $attempt = check_account_serial($serial);
            if ($attempt["error"] == True)
            {
              $messages[] = $attempt["errorMessage"];
            }
          }
        }
      }
      if ($exit_process != 1)
      {
        if ($add_submit == 1)
        {
          $gm_name = $userdata -> username;

          $success = array ();
          foreach ($ban_serial as $key => $serial)
          {
            $attempt = ban_user($serial, $ban_period, $ban_reason, $ban_chat, $gm_name);
            if ($attempt["error"] == True)
            {
              $messages[] = "This account has already been banned";
            }
            else
            {
              $display_form = false;
              $success[] = "Successfully banned the account: " . $serial;
              gamecp_log(4, $userdata -> username, "ADMIN - MANAGE BANS - ADDED - Account Serial: {$serial}", 1);
            }
          }

          if (count($success) > 0)
          {
            $out .= get_notification_html($success, SUCCESS);
            if (count($messages) == 0)
            {
              $redirect = CUSTOM_PAGE_SHORT;
              $redirecturl = $script_name . "?action=" . $_GET["action"];
            }
          }
        }
        else if ($edit_submit == 1)
        {
          $ban_serial = $ban_serial[0];
          $attempt = update_ban($ban_serial, $ban_period, $ban_reason, $ban_chat);
          if ($attempt["error"] == True)
          {
            $messages[] = "SQL Error while trying to update user ban";
          }
          else
          {
            $display_form = false;
            $out .= get_notification_html("Successfully updated the banned account: " . $edit_ban_serial, SUCCESS);
            $redirect = CUSTOM_PAGE_SHORT;
            $redirecturl = $script_name . "?action=" . $_GET["action"];
            gamecp_log(4, $userdata -> username, "ADMIN - MANAGE BANS - UPDATED - Account Serial: {$edit_ban_serial}", 1);
          }
        }
      }

      if (count($messages) > 0)
      {
        get_notification_html($messages, ERROR);
      }

      if ($display_form == true)
      {
        $preset_serials = isset($_GET["preset_serials"]) ? $_GET["preset_serials"] : "";
        if ($ban_serial == 0 || $ban_serial == "")
        {
          $ban_serial = $preset_serials;
        }
        $out .= "<form class=\"ink-form\" method=\"post\">";
        $out .= "<table class=\"tborder\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
        $out .= "<tr>";
        $out .= "<td  colspan=\"2\">" . $this_mode_title . "</td>";
        $out .= "</tr>";
        $out .= "<tr>";
        $out .= "<td  width=\"1\" nowrap>Account Name:</td>";
        $out .= "<td ><input type=\"text\" name=\"ban_name\" value=\"" . ( $ban_name != "" ? $ban_name : "" ) . "\"" . $disable . " /></td>";
        $out .= "</tr>";
        $out .= "<tr>";
        $out .= "<td  width=\"1\" nowrap>Account Serial:</td>";
        $out .= "<td ><input type=\"text\" name=\"ban_serial\" value=\"" . ( $ban_serial != 0 ? $ban_serial : "" ) . "\"" . $disable . " /></td>";
        $out .= "</tr>";
        $out .= "<tr>";
        $out .= "<td  width=\"1\" nowrap>Chat Ban:</td>";
        $checked_yes = $ban_chat == 1 ? " checked" : "";
        $checked_no = $ban_chat == 0 ? " checked" : "";
        $out .= "<td >Yes <input type=\"radio\" name=\"ban_chat\" value=\"1\" " . $checked_yes . "/> No <input type=\"radio\" name=\"ban_chat\" value=\"0\" " . $checked_no . "/></td>";
        $out .= "</tr>";
        $out .= "<tr>";
        $out .= "<td  width=\"1\" nowrap>Period:</td>";
        $out .= "<td >";
        $out .= "<select name=\"ban_period\">";
        $out .= "<option value=\"2\"" . ( $ban_period == 2 ? " selected=\"seelcted\"" : "" ) . ">2 Hrs</option>";
        $out .= "<option value=\"3\"" . ( $ban_period == 3 ? " selected=\"seelcted\"" : "" ) . ">3 Hrs</option>";
        $out .= "<option value=\"4\"" . ( $ban_period == 4 ? " selected=\"seelcted\"" : "" ) . ">4 Hrs</option>";
        $out .= "<option value=\"12\"" . ( $ban_period == 12 ? " selected=\"seelcted\"" : "" ) . ">12 Hrs</option>";
        $out .= "<option value=\"24\"" . ( $ban_period == 24 ? " selected=\"seelcted\"" : "" ) . ">1 Day</option>";
        $out .= "<option value=\"48\"" . ( $ban_period == 48 ? " selected=\"seelcted\"" : "" ) . ">2 Days</option>";
        $out .= "<option value=\"72\"" . ( $ban_period == 72 ? " selected=\"seelcted\"" : "" ) . ">3 Days</option>";
        $out .= "<option value=\"720\"" . ( $ban_period == 720 ? " selected=\"seelcted\"" : "" ) . ">1 Month</option>";
        $out .= "<option value=\"119988\"" . ( 720 < $ban_period ? " selected=\"seelcted\"" : "" ) . ">Forever</option>";
        $out .= "</select>";
        $out .= "</td>";
        $out .= "</tr>";
        $out .= "<tr>";
        $out .= "<td  width=\"1\" nowrap>Reason:</td>";
        $out .= "<td >";
        $out .= "<select name=\"ban_reason\">";
        $i = 0;
        while ($i < $reasons_count)
        {
          if ($ban_reasons[$i] == $ban_reason)
          {
            $selected = " selected=\"selected\"";
          }
          else if ($ban_reasons[$i] == "Multiple Account Voting")
          {
            $selected = " selected=\"selected\"";
          }
          else
          {
            $selected = "";
          }
          $out .= "<option" . $selected . ">" . $ban_reasons[$i] . "</option>";
          ++$i;
        }
        $out .= "</select>";
        $out .= "</td>";
        $out .= "</tr>";
        $out .= "<tr>";
        $out .= "<td  colspan=\"2\" nowrap>";
        $out .= "<input name=\"page\" type=\"hidden\" value=\"addedit\"/>";
        $out .= "<input class=\"ink-button\" name=\"" . $page_mode . "\" type=\"submit\" value=\"" . $submit_name . "\" style=\"margin-left:0px;\"/></td>";
        $out .= "</tr>";
        $out .= "</table>";
        $out .= "</form>";
      }
    }
    else if ($page == "delete")
    {
      $ban_serial = isset($_GET['ban_serial']) && ctype_digit($_GET['ban_serial']) ? intval($_GET['ban_serial']) : 0;
      if ($ban_serial <= 0)
      {
        $out .= get_notification_html("No such banned account found", ERROR);
      }
      else
      {
        $attempt = get_banned_username($ban_serial);
        if ($attempt["error"] == True)
        {
          $out .= get_notification_html("No such banned account found", ERROR);
        }
        else
        {
          $out .= "<form class=\"ink-form\" method=\"post\">";
          $out .= "<p style=\"text-align: center; font-weight: bold;\">Are you sure you want to UNBAN the Account: <u>" . antiject($attempt['username']) . "</u> (Serial: " . $ban_serial . ")?</p>";
          $out .= "<p style=\"text-align: center;\"><input type=\"hidden\" name=\"ban_serial\" value=\"" . $ban_serial . "\"/><input type=\"hidden\" name=\"page\" value=\"delete_user\"/><input type=\"submit\" name=\"yes\" value=\"Yes\"/> <input type=\"submit\" name=\"no\" value=\"No\"/></p>";
          $out .= "</form>";
        }
      }
    }
    else if ($page == "delete_user")
    {
      $yes = isset($_POST['yes']) ? "1" : "0";
      $no = isset($_POST['no']) ? "1" : "0";
      $ban_serial = isset($_GET['ban_serial']) && ctype_digit($_GET['ban_serial']) ? intval($_GET['ban_serial']) : 0;
      if ($no != 1 && $ban_serial >= 0)
      {
        $attempt = remove_ban($ban_serial);
        if ($attempt["error"] == True)
        {
          $out .= get_notification_html("Error while trying to remove the ban", ERROR);
        }
        else
        {
          $out .= get_notification_html("Successfully unbanned account: " . $ban_serial, SUCCESS);
          $redirect = CUSTOM_PAGE_SHORT;
          $redirecturl = $script_name . "?action=" . $_GET["action"];
          $attempt = get_banned_username($ban_serial);
          gamecp_log(5, $userdata -> username, "ADMIN - MANAGE BANS - UNBAN - Account Name:  " . antiject($attempt['username']) . " | Serial: " . $ban_serial, 1);
        }
      }
      else
      {
        $redirect = PREVIOUS_PAGE_SHORT;
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = INDEX_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}

