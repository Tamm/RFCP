<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Support";
  $moduleLabel = "Account Search";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{

  /**
   * @author Mauro Tamm   @ mauro.tamm@gmail.com
   * @author Agony @ agony@nxtdeveloper.com
   * @copyright 2014 http://nxtdeveloper.com/
   * @ver 1.0
   */
  if ($userdata -> has_permission($action))
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $do_search = isset($_GET['do_search']) ? $_GET['do_search'] : "";
    $page_size = isset($_GET['page_size']) && ctype_digit($_GET['page_size']) ? intval($_GET['page_size']) : 10;
    $account_serial = isset($_GET['account_serial']) && ctype_digit($_GET['account_serial']) ? intval($_GET['account_serial']) : 0;
    $account_name = isset($_GET['account_name']) ? antiject($_GET['account_name']) : "";
    $account_pwd = isset($_GET['account_pwd']) ? antiject($_GET['account_pwd']) : "";
    $bank_pwd = isset($_GET['bank_pwd']) ? antiject($_GET['bank_pwd']) : "";
    $bank_ans = isset($_GET['bank_ans']) ? antiject($_GET['bank_ans']) : "";
    $fg_pwd = isset($_GET['fg_pwd']) ? antiject($_GET['fg_pwd']) : "";
    $fg_ans = isset($_GET['fg_ans']) ? antiject($_GET['fg_ans']) : "";
    $create_ip = isset($_GET['create_ip']) ? antiject($_GET['create_ip']) : "";
    $last_ip = isset($_GET['last_ip']) ? antiject($_GET['last_ip']) : "";
    $email = isset($_GET['email']) ? antiject($_GET['email']) : "";
    $condensed = isset($_GET['condensed']) && $_GET['condensed'] != "" ? True : False;
    $search_query = "";
    $messages = array ();
    if (empty($page))
    {
      $out .= "<form class='ink-form' method='get' action='" . $script_name . "?action=" . $_GET['action'] . "'>";
      $out .= "<table class='ink-form' cellpadding='3' cellspacing='1' border='0' width='100%' style='font-size: 12pt;'>";

      $out .= "<tr>";
      $out .= "<td colspan='4' style=''><b>Search Accounts</b></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Account Serial:</td>";
      $out .= "<td ><input type='text' name='account_serial' size='40'/></td>";
      $out .= "<td>Account Name:<br/><span style='font-size: 12px;'>Use % as a wild card.</span></td>";
      $out .= "<td ><input type='text' name='account_name' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Account Password:<br/><span style='font-size: 12px;'>Use % as a wild card.</span></td>";
      $out .= "<td ><input type='text' name='account_pwd' size='40'/></td>";
      $out .= "<td>Email:</td>";
      $out .= "<td ><input type='text' name='email' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Bank Password:<br/><span style='font-size: 12px;'>Use % as a wild card.</span></td>";
      $out .= "<td ><input type='text' name='bank_pwd' size='40'/></td>";
      $out .= "<td>Bank Answer:<br/><span style='font-size: 12px;'>Use % as a wild card.</span></td>";
      $out .= "<td ><input type='text' name='bank_ans' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>FG Password:<br/><span style='font-size: 12px;'>Use % as a wild card.</span></td>";
      $out .= "<td ><input type='text' name='fg_pwd' size='40'/></td>";
      $out .= "<td>FG Answer:<br/><span style='font-size: 12px;'>Use % as a wild card.</span></td>";
      $out .= "<td ><input type='text' name='fg_ans' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<tr>";
      $out .= "<td>Create IP:<br/><span style='font-size: 12px;'>Use % as a wild card.</span></td>";
      $out .= "<td ><input type='text' name='create_ip' size='40'/></td>";
      $out .= "<td>Last login IP:<br/><span style='font-size: 12px;'>Use % as a wild card.</span></td>";
      $out .= "<td ><input type='text' name='last_ip' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<td>Accounts per page:</td>";
      $out .= "<td ><select name='page_size'>";
      $v10 = "";
      if ($page_size == 10)
      {
        $v10 = "selected";
      }
      $out .= "<option value='10'" . $v10 . ">10</option>";
      $v15 = "";
      if ($page_size == 15)
      {
        $v15 = "selected";
      }
      $out .= "<option value='15'" . $v15 . ">15</option>";
      $v25 = "";
      if ($page_size == 25)
      {
        $v25 = "selected";
      }
      $out .= "<option value='25'" . $v25 . ">25</option>";
      $v50 = "";
      if ($page_size == 50)
      {
        $v50 = "selected";
      }
      $out .= "<option value='50'" . $v50 . ">50</option>";
      $v100 = "";
      if ($page_size == 100)
      {
        $v100 = "selected";
      }
      $out .= "<option value='100'" . $v100 . ">100</option>";
      $out .= "</select></td> ";
      $out .= "<td>Condensed Version:<br/></td>";
      $out .= "<td ><select name=\"condensed\">";
      $out .= "<option value=''>No</option>";
      $out .= "<option value='YES'>Yes</option>";
      $out .= "</select></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td colspan='2'><input type='hidden' name='action' value='" . $_GET['action'] . "'><input class='ink-button' type='submit' value='Search' name='do_search' style='margin-left: 0px;'/></td>";
      $out .= "</tr>";
      $out .= "</table>";
      $out .= "</form>";

      if ($do_search != "")
      {
        $bankcheck = true;
        $account_add = "";
        $pwd_add = "";
        $serial_add = "";
        $email_add = "";
        $ipc_add = "";
        $ipl_add = "";
        $fgpw_add = "";
        $fgan_add = "";

        $or_pwd = "";
        $or_serial = "";
        $or_email = "";
        $or_ipc = "";
        $or_ipl = "";
        $or_fgp = "";
        $or_fga = "";
        $or_bs = "";

        $out .= "<br/><br/>";
        if ($account_serial == 0 && $account_pwd == "" && $account_name == "" && $create_ip == "" && $last_ip == "" && $email == "" && $fg_ans == "" && $fg_pwd == "" && $bank_ans == "" && $bank_pwd == "")
        {
          $messages[] = "Please fill in one of the search options.";
        }
        if ($account_serial != "" && is_int($account_serial) == false)
        {
          $messages[] = "Invalid Account Serial.";
        }
        if ($email != "" && !filter_var($email, FILTER_VALIDATE_EMAIL))
        {
          $messages[] = "Invalid Email Specified.";
        }
        if ($account_name != "")
        {
          if (!preg_match("/%/", $account_name))
          {
            $account_add = "P.id = CONVERT(binary,'" . $account_name . "')";
          }
          else
          {
            $account_add = "CAST(P.id AS varchar(255)) LIKE '" . $account_name . "'";
          }
        }
        if ($account_pwd != "")
        {
          if ($account_name != "")
          {
            $or_pwd = " OR ";
          }
          if (!preg_match("/%/", $account_pwd))
          {
            $pwd_add = "P.password = CONVERT(binary,'" . $account_pwd . "')";
          }
          else
          {
            $pwd_add = "CAST(P.password AS varchar(255)) LIKE '" . $account_pwd . "'";
          }
        }
        if ($account_serial != 0)
        {
          if ($account_name != "" || $account_pwd != "")
          {
            $or_serial = " OR ";
          }
          $serial_add = "U.serial = '" . $account_serial . "'";
        }
        if ($email != "")
        {
          if ($account_name != "" || $account_pwd != "" || $account_serial != 0)
          {
            $or_email = " OR ";
          }
          $email_add = "P.email LIKE '" . $email . "'";
        }
        if ($create_ip != "")
        {
          if ($account_name != "" || $account_pwd != "" || $account_serial != 0 || $email != "")
          {
            $or_ipc = " OR ";
          }
          if (!preg_match("/%/", $create_ip))
          {
            $ipc_add = "U.createip = '" . $create_ip . "'";
          }
          else
          {
            $ipc_add = "U.createip LIKE '" . $create_ip . "'";
          }
        }
        if ($last_ip != "")
        {
          if ($account_name != "" || $account_pwd != "" || $account_serial != 0 || $email != "" || $create_ip != "")
          {
            $or_ipl = " OR ";
          }
          if (!preg_match("/%/", $last_ip))
          {
            $ipl_add = "U.lastconnectip = '" . $last_ip . "'";
          }
          else
          {
            $ipl_add = "U.lastconnectip LIKE '" . $last_ip . "'";
          }
        }
        if ($fg_pwd != "")
        {
          if ($account_name != "" || $account_pwd != "" || $account_serial != 0 || $email != "" || $create_ip != "" || $last_ip != "")
          {
            $or_fgp = " OR ";
          }
          if (!preg_match("/%/", $fg_pwd))
          {
            $fgpw_add = "U.uilock_pw = CONVERT(binary,'" . $fg_pwd . "')";
          }
          else
          {
            $fgpw_add = "U.uilock_pw LIKE CONVERT(binary,'" . $fg_pwd . "')";
          }
        }
        if ($fg_ans != "")
        {
          if ($account_name != "" || $account_pwd != "" || $account_serial != 0 || $email != "" || $create_ip != "" || $last_ip != "" || $fg_pwd != "")
          {
            $or_fga = " OR ";
          }
          if (!preg_match("/%/", $fg_ans))
          {
            $fgan_add = "U.uilock_hintanswer = CONVERT(binary,'" . $fg_ans . "')";
          }
          else
          {
            $fgan_add = "U.uilock_hintanswer LIKE CONVERT(binary,'" . $fg_ans . "')";
          }
        }
        if ($bank_ans != "" || $bank_pwd != "")
        {
          $banksearch = "";
          if ($bank_ans != "")
          {
            $banksearch .= "HintAnswer = '" . $bank_ans . "'";
          }
          if ($bank_pwd != "")
          {
            if ($banksearch != "")
            {
              $banksearch .= " AND ";
            }
            $banksearch .= "TrunkPass = CONVERT(binary,'" . $bank_pwd . "')";
          }
          $sql = "SELECT AccountSerial FROM tbl_AccountTrunk WHERE {$banksearch}";

          $bank_result = sqlsrv_query(connectdb(DATA), $sql);
          $bank_serial = "";
          while ($bank = sqlsrv_fetch_array($bank_result, SQLSRV_FETCH_ASSOC))
          {
            if ($bank_serial != "")
            {
              $bank_serial .= ",";
            }
            $bank_serial .= $bank['AccountSerial'];
          }

          if ($bank_serial != "")
          {
            if ($account_name != "" || $account_pwd != "" || $account_serial != 0 || $email != "" || $create_ip != "" || $last_ip != "" || $fg_pwd != "" || $fg_ans != "")
            {
              $or_bs = " OR ";
            }
            $account_add = "U.serial IN (" . $bank_serial . ")";
            $bankcheck = true;
          }
          else
          {
            $bankcheck = false;
          }
        }

        if (!$bankcheck)
        {
          $messages[] = "Sorry, no results could be found";
        }
        if (count($messages) == 0)
        {
          include( "./core/pagination.php" );

          $search_query = "(" . $account_add . $or_pwd . $pwd_add . $or_serial . $serial_add . $or_email . $email_add . $or_ipc . $ipc_add . $or_ipl . $ipl_add . $or_fgp . $fgpw_add . $or_fga . $fgan_add . ")";

          $sql = "SELECT U.serial, U.createtime, U.createip, U.lastconnectip, U.lastlogofftime, id = CAST(P.id AS varchar(255)), ";
          $sql .= "password = CAST(P.password AS varchar(255)), P.email, U.uilock, uilock_pw = CAST(U.uilock_pw AS varchar(255)), ";
          $sql .= "uilock_hintanswer = CAST(U.uilock_hintanswer AS varchar(255)), convert(varchar, U.uilock_update) AS uilock_update, ";
          $sql .= "(SELECT nAccountSerial FROM tbl_UserBan WHERE nAccountSerial = U.serial) AS is_banned FROM " . TABLE_LUACCOUNT . " AS P ";
          $sql .= "INNER JOIN tbl_UserAccount AS U ON U.id = P.id WHERE {$search_query} AND U.serial NOT IN ( SELECT TOP [OFFSET] U.serial FROM " . TABLE_LUACCOUNT . " AS P ";
          $sql .= "INNER JOIN tbl_UserAccount AS U ON U.id = P.id WHERE {$search_query} ORDER BY U.lastlogofftime DESC) ORDER BY U.lastlogofftime DESC";

          $sql_count = "SELECT COUNT(U.id) AS Serial FROM " . TABLE_LUACCOUNT . " AS P INNER JOIN tbl_UserAccount AS U ON U.id = P.id WHERE {$search_query}";

          $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
          $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

          $pager = new Pagination(USER, $sql, $sql_count, $url, array (), array (), $page_size, $links_to_show = 11);
          $results = $pager -> get_data();

          if ($condensed)
          {
            $out .= "<table class='ink-table ink-table-condensed' cellpadding = '0' cellspacing = '0' border = '0' width = '100%''>";
            $out .= "<tr>";
            $out .= "<td><input type=\"checkbox\" class=\"check_all_box\" onclick=\"$('.account_checkboxes').prop('checked', this.checked);\" /></td>";
            $out .= "<td>Serial</td>";
            $out .= "<td>ID</td>";
            $out .= "<td>Pass</td>";
            $out .= "<td>Email</td>";
            $out .= "<td>Create IP</td>";
            $out .= "<td>Last Conn IP</td>";
            $out .= "<td>FG Pass</td>";
            $out .= "<td>FG Hint</td>";
            $out .= "<td>Bank Pass</td>";
            $out .= "<td>Bank Hint</td>";
            $out .= "<td>Last Vote</td>";
            $out .= "</tr>";
          }
          else
          {
            $out .= "<table class='ink-table' cellpadding = '0' cellspacing = '0' border = '0' width = '100%''>";
          }

          foreach ($results["rows"] as $key => $row)
          {
            $banksql = "SELECT EstSlot, CONVERT(varchar(255),TrunkPass) as BankPass, HintAnswer  FROM tbl_AccountTrunk WHERE AccountSerial = {$row['serial']}";
            $bank_result = sqlsrv_query(connectdb(DATA), $banksql);
            sqlsrv_fetch($bank_result);
            $last_vote = get_last_vote_any($row['serial']);

            if ($condensed)
            {
              $style = "";
              if ($row['is_banned'] == $row['serial'])
              {
                $style = "style=\"background-color:red; color: white;\"";
              }
              $out .= "<tr " . $style . ">";
              $out .= "<td><input type=\"checkbox\" class=\"account_checkboxes\" name=\"" . $row['serial'] . "\" /></td>";
              $out .= "<td>" . $row['serial'] . "</td>";
              $out .= "<td>" . $row['id'] . "</td>";
              $out .= "<td>" . $row['password'] . "</td>";
              $out .= "<td>" . $row['email'] . "</td>";
              $out .= "<td>" . $row['createip'] . "</td>";
              $out .= "<td>" . $row['lastconnectip'] . "</td>";
              $out .= "<td>" . $row['uilock_pw'] . "</td>";
              $out .= "<td>" . $row['uilock_hintanswer'] . "</td>";
              $out .= "<td>" . sqlsrv_get_field($bank_result, 1) . "</td>";
              $out .= "<td>" . sqlsrv_get_field($bank_result, 2) . "</td>";

              if (isset($last_vote["vote_log"]["log_time"]))
              {
                $out .= "<td>" . date("d/m/y h:i A", $last_vote["vote_log"]["log_time"]) . "</td>";
              }
              else
              {
                $out .= "<td>&nbsp;</td>";
              }

              $out .= "</tr>";
            }
            else
            {
              $out .= "<tr>";
              $out .= "<td class='thead' colspan='5' style='border-bottom-width:2px; border-bottom-color: #000;'>&nbsp;</td>";
              $out .= "</tr>";
              $out .= "<tr>";
              $out .= "<td colspan='5' style=' border-bottom: 0px !important;'><span style='font-weight: bold;'>Account Serial:</span> " . $row['serial'];
              $out .= "<br/><span style='font-weight: bold;'>Account Name:</span> " . $row['id'];
              $out .= "<br/><span style='font-weight: bold;'>Account Password:</span> " . $row['password'];
              $out .= "<br/><span style='font-weight: bold;'>Email:</span> " . $row['email'] . "</td>";
              $out .= "</tr>";
              $out .= "<tr>";
              $out .= "<th class='thead align-left' nowrap>Create Time</td>";
              $out .= "<th class='thead align-left' nowrap>Create IP</td>";
              $out .= "<th class='thead align-left' nowrap>Last Connect IP</td>";
              $out .= "<th class='thead align-left' nowrap>Logoff time</td>";
              $out .= "</tr>";
              $out .= "<tr>";
              $out .= "<td nowrap>" . $row["createtime"] -> format('Y-m-d H:i:s') . "</td>";
              $out .= "<td nowrap>" . $row["createip"] . "</td>";
              $out .= "<td nowrap>" . $row["lastconnectip"] . "</td>";
              $out .= "<td nowrap>" . $row["lastlogofftime"] -> format('Y-m-d H:i:s') . "</td>";
              $out .= "</tr>";
              $out .= "<tr>";
              $out .= "<th class='thead align-left' nowrap>FG lock</td>";
              $out .= "<th class='thead align-left' nowrap>FG Password</td>";
              $out .= "<th class='thead align-left' nowrap>FG Hint</td>";
              $out .= "<th class='thead align-left' nowrap>FG Password Update</td>";
              $out .= "</tr>";
              $out .= "<tr>";
              $out .= "<td nowrap>" . $row["uilock"] . "</td>";
              $out .= "<td nowrap>" . $row["uilock_pw"] . "</td>";
              $out .= "<td nowrap>" . $row["uilock_hintanswer"] . "</td>";
              $out .= "<td nowrap>" . $row["uilock_update"] . "</td>";
              $out .= "</tr>";
              $out .= "<tr>";
              $out .= "<th class='thead align-left' nowrap>Bank Slots </td>";
              $out .= "<th class='thead align-left' nowrap>Bank Password</td>";
              $out .= "<th class='thead align-left' nowrap>Bank Hint</td>";
              $out .= "</tr>";
              $out .= "<tr>";
              $out .= "<td nowrap>" . sqlsrv_get_field($bank_result, 0) . "</td>";
              $out .= "<td nowrap>" . sqlsrv_get_field($bank_result, 1) . "</td>";
              $out .= "<td nowrap>" . sqlsrv_get_field($bank_result, 2) . "</td>";
              $out .= "</tr>";
            }
          }

          if ($condensed && count($results["rows"]) >= 1)
          {
            $link = $script_name . "?action=ban_user&page=addedit&preset_serials=";
            $out .= "<tr>";
            $out .= "<td  colspan='50' style='text-align: left;'>";
            $out .= "<button class=\"ink-button\" onclick=\"var serials = ''; var first = true; $('.account_checkboxes').each(function(element){if(this.checked){if(!first){serials += ',';}else{first=false;} serials += $(this).attr('name');}}); window.open('" . $link . "' + serials, '_blank');\">Ban Selected Users</button>";
            $out .= "</td>";
            $out .= "</tr>";
          }

          $out .= "<tfoot>";
          if (count($results["rows"]) <= 0)
          {
            $out .= "<tr>";
            $out .= "<td  colspan='50' style='text-align: center; font-weight: bold;'>No items  found.</td>";
            $out .= "</tr>";
          }
          else
          {
            $out .= "<tr>";
            $out .= "<td  colspan='50' style='text-align: center; font-weight: bold;'>" . $pager -> renderFullNav() . "</td>";
            $out .= "</tr>";
          }
          $out .= "</tfoot>";

          $out .= "</table>";
          if (!isset($item_code))
          {
            $item_code = "";
          }
          gamecp_log(0, $userdata -> username, "ADMIN - Account SEARCH - Searched for: {$account_name} or {$account_serial}", 1);
        }
      }
      if (count($messages) > 0)
      {
        $out .= get_notification_html($messages, ERROR);
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = PREVIOUS_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}

