<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "User";
  $moduleLabel = "Character Info";
  $permission = "player";
  return;
}

if ($this_script == $script_name)
{
  $exit_stage = 0;
  if ($userdata -> loggedin == True)
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $equipment_names = array ("Upper", "Lower", "Gloves", "Shoes", "Head", "Shield", "Weapon", "Cloak");
    if (!isset($config['specialgp_gender']))
    {
      $config['specialgp_gender'] = 258;
    }
    if (!isset($config['specialnc_baseclass']))
    {
      $config['specialnc_baseclass'] = 258;
    }
    if (!isset($config['specialnc_currency']))
    {
      $config['specialnc_currency'] = 0;
    }

    if ($page == "")
    {
      $out .= "<p>You must be logged out in order to make any changes to your characters</p>";
      $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
      $out .= "<tr>";
      $out .= "<td class=\"thead\" nowrap>Character Name</td>";
      $out .= "<td class=\"thead\" style=\"text-align: center;\" nowrap>Race</td>";
      $out .= "<td class=\"thead\" nowrap>Level</td>";
      $out .= "<td class=\"thead\" nowrap>Class</td>";
      $out .= "<td class=\"thead\" nowrap>Time Spent</td>";
      $out .= "<td class=\"thead\" colspan=\"2\" nowrap>Options</td>";
      $out .= "</tr>";

      $characters = $userdata -> get_character_list_full();
      $classes = get_classes();
      $classes_by_id = $classes["classes"];
      $classes = $classes["classesByCode"];
      $currency = "Dalant";

      foreach ($characters as $key => $char)
      {
        $class_info = $classes[$char['Class']];

        $change_gender = "<span id=\"gender_form_" . $char['Serial'] . "\" style=\"display:none;\"><form class=\"ink-form\" method=\"GET\" action=\"" . $script_name . "\">";
        $change_gender .= "<input type=\"hidden\" name=\"action\" value=\"" . $_GET['action'] . "\">";
        $change_gender .= "<input type=\"hidden\" name=\"page\" value=\"change_gender\">";
        $change_gender .= "<input type=\"hidden\" name=\"serial\" value=\"{$char['Serial']}\"><input class=\"ink-button\" type=\"submit\" value=\"Submit Change Gender (" . $config['specialgp_gender'] . "NC)\" name=\"submit\" />";
        $change_gender .= "</form></span>";
        $onclick = "$('#gender_link_" . $char['Serial'] . "').hide(); $('#gender_form_" . $char['Serial'] . "').show(); $('#gender_text_" . $char['Serial'] . "').hide(); return false;";
        $change_gender .= "<span id=\"gender_link_" . $char['Serial'] . "\"> &nbsp;&nbsp; <span class=\"icon_link " . BASE_COLOR . "\" onclick=\"" . $onclick . "\" style=\"color: #0069d6; cursor:pointer;\">";
        if ($char['Race'] == 0)
        {
          $gender = "Male";
          $race = "<span style=\"color: #CC6699;\">Bell</span>";
          $change_gender .= "<i class=\"icon-female icon-medium\" title=\"Change to Female\"></i> " . $config['specialgp_gender'] . " NC</a>";
        }
        elseif ($char['Race'] == 1)
        {
          $gender = "Female";
          $race = "<span style=\"color: #CC6699;\">Bell</span>";
          $change_gender .= "<i class=\"icon-male icon-medium\" title=\"Change to Male\"></i> " . $config['specialgp_gender'] . " NC</a>";
        }
        elseif ($char['Race'] == 2)
        {
          $gender = "Male";
          $race = "<span style=\"color: #9933CC;\">Cora</span>";
          $change_gender .= "<i class=\"icon-female icon-medium\" title=\"Change to Female\"></i> " . $config['specialgp_gender'] . " NC</a>";
          $currency = "Disena";
        }
        elseif ($char['Race'] == 3)
        {
          $gender = "Female";
          $race = "<span style=\"color: #9933CC;\">Cora</span>";
          $change_gender .= "<i class=\"icon-male icon-medium\" title=\"Change to Male\"></i> " . $config['specialgp_gender'] . " NC</a>";
          $currency = "Disena";
        }
        elseif ($char['Race'] == 4)
        {
          $gender = "Robot";
          $race = "<span style=\"color: grey;\">Acc</span>";
          $change_gender = "";
          $currency = "CP";
        }

        if ($userdata -> credits < $config['specialgp_gender'])
        {
          $change_gender = "";
        }

        $out .= "<tr>";
        $out .= "<td style=\"font-weight: bold;\" nowrap>" . filter_string_for_html($char['Name']) . "</td>";
        $out .= "<td style=\"text-align: center;\" nowrap>" . $race . "</td>";
        $out .= "<td nowrap>" . $char['Lv'] . "</td>";
        $out .= "<td nowrap>" . $class_info . "</td>";
        $out .= "<td nowrap>" . ceil($char['TotalPlayMin'] / 60) . " Hours</td>";
        $out .= "<td style=\"text-align: left;\" nowrap>";
        $out .= "<a class=\"icon_link " . BASE_COLOR . "\" href=\"javascript:view_more('" . $char['Serial'] . "')\"><i id=\"view_more_" . $char["Serial"] . "\" class=\"icon-plus icon-large\" title=\"View More\"></i></a>";
        $out .= "</td>";
        $out .= "</tr>";
        $out .= "<tr class=\"extra_" . $char['Serial'] . "\" style=\"display: none; line-height: 1;\">";
        $out .= "<td style=\"border-bottom: 0px; line-height: 1;\">Gender</td>";
        $out .= "<td colspan=\"6\" style=\"border-bottom: 0px; line-height: 1;\"><span id=\"gender_text_" . $char['Serial'] . "\">" . $gender . "</span> " . $change_gender . "</td>";
        $out .= "</tr>";
        $out .= "<tr class=\"extra_" . $char['Serial'] . "\" style=\"display: none; line-height: 1;\">";
        $out .= "<td style=\"border-bottom: 0px; line-height: 1;\">PVP Points</td>";
        $out .= "<td colspan=\"6\" style=\"border-bottom: 0px; line-height: 1;\">" . ( ( $char['PvpPoint'] ) ) . "</td>";
        $out .= "</tr>";

        if ($config['specialnc_currency'] > 0)
        {
          $currency_form = "<form class=\"ink-form\" method=\"GET\" action=\"" . $script_name . "\">";
          $currency_form .= get_currency_slider($char['Serial'], $char['Dalant'], $config['specialnc_currency'], $currency, $userdata -> credits);
          $currency_form .= "<input type=\"hidden\" name=\"action\" value=\"" . $_GET['action'] . "\">";
          $currency_form .= "<input type=\"hidden\" name=\"page\" value=\"add_currency\">";
          $currency_form .= "<input type=\"hidden\" name=\"char_serial\" value=\"{$char['Serial']}\"><input id=\"currency_button_" . $char['Serial'] . "\" class=\"ink-button\" type=\"submit\" value=\"Submit (0 NC)\" name=\"submit\" />";
          $currency_form .= "</form>";

          $out .= "<tr class=\"extra_" . $char['Serial'] . "\" style=\"display: none; line-height: 1;\">";
          $out .= "<td style=\"border-bottom: 0px; line-height: 1;\">{$currency}</td>";
          $out .= "<td colspan=\"6\" style=\"border-bottom: 0px; line-height: 1;\"><span id=\"currency_amount_" . $char['Serial'] . "\">" . number_format($char['Dalant']) . "</span>";
          $out .= "<span id=\"currency_form_" . $char['Serial'] . "\" style=\"display: none;\">" . $currency_form . "</span>";
          $onclick = "$('#currency_amount_" . $char['Serial'] . "').hide(); $('#currency_link_" . $char['Serial'] . "').hide(); $('#currency_form_" . $char['Serial'] . "').show(); return false;";
          $out .= "<span id=\"currency_link_" . $char['Serial'] . "\">&nbsp;&nbsp; <span class=\"icon_link " . BASE_COLOR . "\" onclick=\"" . $onclick . "\" style=\"color: #0069d6; cursor:pointer;\">";
          $out .= "<i class=\"icon-dollar icon-medium\" title=\"Exchange NC for Currency\"></i> Convert NC to " . $currency . "(" . $config['specialnc_currency'] . " per 1 NC)</span></span>";
          $out .= "</td>";
          $out .= "</tr>";
        }
        else
        {
          $out .= "<tr class=\"extra_" . $char['Serial'] . "\" style=\"display: none; line-height: 1;\">";
          $out .= "<td style=\"border-bottom: 0px; line-height: 1;\">{$currency}</td>";
          $out .= "<td colspan=\"6\" style=\"border-bottom: 0px; line-height: 1;\"><span id=\"currency_amount_" . $char['Serial'] . "\">" . number_format($char['Dalant']);
          $out .= "</td>";
          $out .= "</tr>";
        }

        $out .= "<tr class=\"extra_" . $char['Serial'] . "\" style=\"display: none; line-height: 1;\">";
        $out .= "<td style=\"border-bottom: 0px; line-height: 1;\">Gold</td>";
        $out .= "<td colspan=\"6\" style=\"border-bottom: 0px; line-height: 1;\">" . number_format($char['Gold']) . "</td>";
        $out .= "</tr>";

        if ($char["Class0"] != -1)
        {
          $base_class_form = "<form class=\"ink-form\" method=\"GET\" action=\"" . $script_name . "\">";
          $base_class_form .= "<select name=\"base_class\">";
          $base_class_form .= get_base_class_options_html($char["Class0"]);
          $base_class_form .= "</select>";
          $base_class_form .= "<input type=\"hidden\" name=\"action\" value=\"" . $_GET['action'] . "\">";
          $base_class_form .= "<input type=\"hidden\" name=\"page\" value=\"change_baseclass\">";
          $base_class_form .= "<input type=\"hidden\" name=\"char_serial\" value=\"{$char['Serial']}\"><input class=\"ink-button\" type=\"submit\" value=\"Submit (" . $config['specialnc_baseclass'] . "NC)\" name=\"submit\" />";
          $base_class_form .= "</form>";
          $out .= "<tr class=\"extra_" . $char['Serial'] . "\" style=\"display: none; line-height: 1;\">";
          $out .= "<td style=\"border-bottom: 0px; line-height: 1;\">Base Class</td>";
          $out .= "<td colspan=\"6\" style=\"border-bottom: 0px; line-height: 1;\">";
          $out .= "<span id=\"class_text_" . $char['Serial'] . "\">" . $classes_by_id[$char['Class0']]["class_name"] . "</span>";
          $out .= "<span id=\"class_form_" . $char['Serial'] . "\" style=\"display:none;\">" . $base_class_form . "</span>";
          $onclick = "$('#class_text_" . $char['Serial'] . "').hide(); $('#class_link_" . $char['Serial'] . "').hide(); $('#class_form_" . $char['Serial'] . "').show(); return false;";
          $out .= "<span id=\"class_link_" . $char['Serial'] . "\">&nbsp;&nbsp; <span class=\"icon_link " . BASE_COLOR . "\" onclick=\"" . $onclick . "\" style=\"color: #0069d6; cursor:pointer;\"><i class=\"icon-exchange icon-medium\" title=\"Change Base Class\"></i> " . $config['specialnc_baseclass'] . " NC</span></span>";
          $out .= "</td>";
          $out .= "</tr>";
        }

        $i = 0;
        while ($i < 8)
        {
          $k_value = $char["EK{$i}"];
          $u_value = $char["EU{$i}"];

          $item_details = array ();

          if (-1 == $k_value)
          {
            $item_id = $k_value;
            $item_code = "-";
            $images = "";
            $item_image = "";
            $item_name = "<i>No item equipped</i>";
          }
          else
          {
            $item_details = get_item_details($k_value, $u_value, $i);
            if ($item_details["type"] < 36)
            {
              $item_info = get_item_info_from_id($item_details["type"], $item_details["id"]);
              $item_code = $item_info["item"]["item_code"];
              $item_name = $item_info["item"]["item_name"];
              $images = $item_details["images"];
              $icon_id = $item_info["item"]["item_icon_id"];
              $item_path = glob("./images/items/" . get_image_folder_name($item_details["type"]) . "/(" . $item_info["item"]["item_icon_id"] . "){.jpg,.JPG,.gif,.GIF,.png,.PNG}", GLOB_BRACE);
              if (file_exists(@$item_path[0]))
              {
                $item_image = "<img id=\"\" class=\"market_item_image\" src=\"" . $item_path[0] . "\" style=\"height: 40px; width: 40px;\">";
              }
              else
              {
                $item_image = "";
              }
            }
            else
            {
              $item_name = "<i>No item equipped</i>";
              $item_code = "-";
            }
          }

          if ($u_value == BASE_CODE)
          {
            $images = "No Upgrade";
          }

          $line_height = "1.5";
          if ($item_image != "")
          {
            $line_height = "2.5";
          }

          $border = "";
          if ($i < 7)
          {
            $border = "border-bottom: 0px;";
          }

          $out .= "<tr class=\"extra_" . $char['Serial'] . "\" style=\"display: none; line-height: 1.5;\">";
          $out .= "<td width=\"15%\" style=\"line-height: 1.5; " . $border . "\" nowrap>" . $equipment_names[$i] . "</td>";
          $out .= "<td colspan=\"3\" style=\"line-height: " . $line_height . "; " . $border . "\" nowrap>" . $item_image . " " . $item_name . "</td>";
          $out .= "<td colspan=\"3\" style=\"line-height: 1.5; " . $border . "\" nowrap>" . $images . "</td>";
          $out .= "</tr>";
          ++$i;
        }
      }
      if (count($characters) <= 0)
      {
        $out .= "<tr>";
        $out .= "<td colspan=\"7\" style=\"text-align: center;\" nowrap>No characters found for your account</td>";
        $out .= "</tr>";
      }
      $out .= "</table>";
    }
    elseif ($page == "change_gender")
    {
      $char_serial = isset($_GET['serial']) && ctype_digit($_GET['serial']) ? intval($_GET['serial']) : 0;

      if ($char_serial <= 0)
      {
        $out .= get_notification_html("Invalid Serial", ERROR);
      }
      elseif ($userdata -> status)
      {
        $out .= get_notification_html("Must be logged out", ERROR);
      }
      elseif ($config['specialgp_gender'] <= $userdata -> credits)
      {
        $attempt = get_character_info($char_serial);
        if ($attempt["error"] == True)
        {
          $out .= get_notification_html($attempt["errorMessage"], ERROR);
        }
        else
        {
          $char_info = $attempt["char"];
          if ($char_info['AccountSerial'] != $userdata -> serial)
          {
            $out .= get_notification_html("Not a valid serial", ERROR);
          }
          else
          {
            if ($char_info['RaceNum'] == 0)
            {
              $race = 1;
            }
            elseif ($char_info['RaceNum'] == 1)
            {
              $race = 0;
            }
            elseif ($char_info['RaceNum'] == 2)
            {
              $race = 3;
            }
            elseif ($char_info['RaceNum'] == 3)
            {
              $race = 2;
            }
            else
            {
              $race = $char_info['RaceNum'];
            }

            $attempt = update_character_race($char_serial, $race);
            if ($attempt["error"] == True)
            {
              $out .= get_notification_html($attempt["errorMessage"], ERROR);
            }
            else
            {
              $minus_credits = "-" . $config['specialgp_gender'];
              $username = $userdata -> username;
              $attempt = add_user_credits($userdata -> serial, $minus_credits);
              if ($attempt["error"] == True)
              {
                $out .= get_notification_html($attempt["errorMessage"], ERROR);
                gamecp_log(1, $userdata -> username, "GAMECP - ACCOUNT INFO - FAILED - Updated Gender - Char Serial: " . $char_serial);
                $redirect = PREVIOUS_PAGE_LONG;
              }
              else
              {
                $out .= get_notification_html("Successfully updated " . $char_info["Name"] . "'s gender", SUCCESS);
                gamecp_log(1, $userdata -> username, "GAMECP - ACCOUNT INFO - Updated Gender - Char Serial: " . $char_serial);
                item_redeem_log($userdata -> serial, $char_serial, $config['specialgp_gender'], "-1", "Gender Change", "1", "-1", $userdata -> credits - $config['specialgp_gender']);
                $redirect = CUSTOM_PAGE_SHORT;
                $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
              }
            }
          }
        }
      }
      else
      {
        $out .= get_notification_html("You do not have enough game points to change your gender", ERROR);
      }
    }
    elseif ($page == "change_baseclass")
    {
      $char_serial = isset($_GET['char_serial']) && ctype_digit($_GET['char_serial']) ? intval($_GET['char_serial']) : 0;
      $new_base = isset($_GET['base_class']) && ctype_digit($_GET['base_class']) ? intval($_GET['base_class']) : 0;
      $character = null;

      if ($char_serial <= 0)
      {
        $out .= get_notification_html("Invalid Serial", ERROR);
      }
      elseif ($userdata -> status)
      {
        $out .= get_notification_html("Must be logged out", ERROR);
      }
      elseif ($config['specialnc_baseclass'] <= $userdata -> credits)//$char["Class0"]
      {
        $attempt = $userdata -> get_character_list_full();
        foreach ($attempt as $key => $char)
        {
          if ($char["Serial"] == $char_serial)
          {
            $character = $char;
          }
        }

        if (!isset($character))
        {
          $out .= get_notification_html("Invalid Serial", ERROR);
        }
        else
        {
          $current_base = $character["Class0"];
          $valid_change = False;
          if ($current_base < 46 && in_array($new_base, array (0, 15, 30, 45)) && $current_base != $new_base)
          {
            $valid_change = True;
          }
          elseif ($current_base > 46 && $current_base < 106 && in_array($new_base, array (60, 75, 90, 105)) && $current_base != $new_base)
          {
            $valid_change = True;
          }
          elseif ($current_base > 106 && in_array($new_base, array (120, 135, 150)) && $current_base != $new_base)
          {
            $valid_change = True;
          }

          if (!$valid_change)
          {
            $out .= get_notification_html("Not a valid base class change", ERROR);
          }
          else
          {
            $attempt = update_character_base_class($char_serial, $new_base);
            if ($attempt["error"] == True)
            {
              $out .= get_notification_html($attempt["errorMessage"], ERROR);
            }
            else
            {
              $minus_credits = "-" . $config['specialnc_baseclass'];
              $username = $userdata -> username;
              $attempt = add_user_credits($userdata -> serial, $minus_credits);
              if ($attempt["error"] == True)
              {
                $out .= get_notification_html($attempt["errorMessage"], ERROR);
                gamecp_log(1, $userdata -> username, "GAMECP - ACCOUNT INFO - FAILED - Updated Base Class - Char Serial: " . $char_serial);
                $redirect = PREVIOUS_PAGE_LONG;
              }
              else
              {
                $out .= get_notification_html("Successfully updated " . $character["Name"] . "'s Base Class", SUCCESS);
                gamecp_log(1, $userdata -> username, "GAMECP - ACCOUNT INFO - Updated Base Class - Char Serial: " . $char_serial);
                item_redeem_log($userdata -> serial, $char_serial, $config['specialnc_baseclass'], "-1", "Base Class Change", "1", "-1", $userdata -> credits - $config['specialnc_baseclass']);
                $redirect = CUSTOM_PAGE_SHORT;
                $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
              }
            }
          }
        }
      }
      else
      {
        $out .= get_notification_html("Not enough credits", ERROR);
      }
    }
    elseif ($page == "add_currency")
    {
      $char_serial = isset($_GET['char_serial']) && ctype_digit($_GET['char_serial']) ? intval($_GET['char_serial']) : 0;
      $convert_nc = isset($_GET['nc_to_convert']) && ctype_digit($_GET['nc_to_convert']) ? intval($_GET['nc_to_convert']) : 0;
      $currency_to_add = $convert_nc * $config['specialnc_currency'];
      $character = null;

      if ($char_serial <= 0)
      {
        $out .= get_notification_html("Invalid Serial", ERROR);
      }
      elseif ($userdata -> status)
      {
        $out .= get_notification_html("Must be logged out", ERROR);
      }
      elseif ($convert_nc == 0)
      {
        $out .= get_notification_html("You aren't converting anything", ERROR);
      }
      elseif ($convert_nc <= $userdata -> credits)//$char["Class0"]
      {
        $attempt = $userdata -> get_character_list_full();
        foreach ($attempt as $key => $char)
        {
          if ($char["Serial"] == $char_serial)
          {
            $character = $char;
          }
        }

        if (!isset($character))
        {
          $out .= get_notification_html("Invalid Serial", ERROR);
        }
        else
        {
          $current_currency = $character['Dalant'];
          $new_currency = $current_currency + $currency_to_add;
          if ($new_currency > MAX_CURRENCY)
          {
            $out .= get_notification_html("Invalid currency amount", ERROR);
          }
          else
          {
            $attempt = update_character_currency($char_serial, $new_currency);
            if ($attempt["error"] == True)
            {
              $out .= get_notification_html($attempt["errorMessage"], ERROR);
            }
            else
            {
              delete_char_npcdata($char_serial);
              $minus_credits = "-" . $convert_nc;
              $username = $userdata -> username;
              $attempt = add_user_credits($userdata -> serial, $minus_credits);
              if ($attempt["error"] == True)
              {
                $out .= get_notification_html($attempt["errorMessage"], ERROR);
                gamecp_log(1, $userdata -> username, "GAMECP - ACCOUNT INFO - FAILED - Update Currency - Char Serial: " . $char_serial);
                $redirect = PREVIOUS_PAGE_LONG;
              }
              else
              {
                $out .= get_notification_html("Successfully added currency to " . $character["Name"] . "", SUCCESS);
                gamecp_log(1, $userdata -> username, "GAMECP - ACCOUNT INFO - Update Currency - Amount: " . $currency_to_add . " - Char Serial: " . $char_serial);
                item_redeem_log($userdata -> serial, $char_serial, $convert_nc, "-1", "Add Currency: " . number_format($currency_to_add), "1", "-1", $userdata -> credits - $convert_nc);
                $redirect = CUSTOM_PAGE_SHORT;
                $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
              }
            }
          }
        }
      }
      else
      {
        $out .= get_notification_html("Not enough credits", ERROR);
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = INDEX_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
