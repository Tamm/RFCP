<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Novus Market Admin";
  $moduleLabel = "Manage Categories";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  $categories_attempt = get_market_categories();
  $categories = $categories_attempt["categories"];
  if ($userdata -> has_permission($action))
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $cat_id = isset($_GET['cat_id']) && ctype_digit($_GET['cat_id']) ? antiject($_GET['cat_id']) : "";
    $exit_stage = 0;
    $sub_cat_id = isset($_GET['sub_cat_id']) && ctype_digit($_GET['sub_cat_id']) ? antiject($_GET['sub_cat_id']) : 0;
    $out .= "<center>";
    $out .= "<button type=\"button\" class=\"ink-button\" onclick=\"window.location='./" . $script_name . "?action=" . $_GET['action'] . "';\">View Categories</button>";
    $out .= "<button type=\"button\" class=\"ink-button\" onclick=\"window.location='./" . $script_name . "?action=" . $_GET['action'] . "&sub_cat_id=" . $sub_cat_id . "&page=addedit';\">New Category</button>";
    $out .= "</center>";

    if (empty($page))
    {
      $sub_cat_id = isset($_GET['sub_cat_id']) && ctype_digit($_GET['sub_cat_id']) ? $_GET['sub_cat_id'] : 0;
      if (isset($_POST['submit']))
      {
        $cat_ids = isset($_POST['cat_id']) && is_array($_POST['cat_id']) ? $_POST['cat_id'] : "";
        $cat_order = isset($_POST['cat_order']) && is_array($_POST['cat_order']) ? $_POST['cat_order'] : "";
        if ($cat_ids != "" && $cat_order != "")
        {
          $update_attempt = update_category_order($cat_ids, $cat_order);
          if ($update_attempt["error"] === True)
          {
            $exit_stage = 1;
            $out .= get_notification_html($update_attempt["errorMessage"], ERROR);
            $redirect = PREVIOUS_PAGE_LONG;
          }
          gamecp_log(0, $userdata -> username, "ADMIN - MANAGE CATEGORIES - ORDER - Changed category order", 1);
        }
      }

      $sub_cat_attempt = get_market_categories($sub_cat_id, $cat_id);
      $sub_categories = $sub_cat_attempt["categories"];
      $cat = $sub_cat_attempt["categories_in_order"];
      if ($sub_cat_attempt["error"] === True)
      {
        $exit_stage = 1;
        $out .= get_notification_html($sub_cat_attempt["errorMessage"], ERROR);
      }
      if ($exit_stage == 0)
      {
        $total_categories = count($cat);
        $out .= "<form method=\"post\" class=\"ink-form category_form\">";
        $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\" align=\"center\">";
        $out .= "<tr>";
        $out .= "<td class=\"thead\" style=\"text-align: center;\" nowrap>#</td>";
        $out .= "<td class=\"thead\" nowrap>Category Name</td>";
        $out .= "<td class=\"thead\" nowrap>Order</td>";
        $out .= "<td class=\"thead\" style=\"text-align: center;\" colspan=\"2\" width=\"30%\" nowrap>Options</td>";
        $out .= "</tr>";
        $i = 0;
        while ($i < $total_categories)
        {
          $out .= "<tr>";
          $out .= "<td class=\"\" style=\"text-align: center;\" nowrap>" . $cat[$i]['id'] . "</td>";
          $out .= "<td class=\"alt1\" nowrap><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&sub_cat_id=" . $cat[$i]['id'] . "\">" . $cat[$i]['label'] . " &raquo;</a></td>";
          $out .= "<td class=\"alt1\" nowrap><input type=\"hidden\" name=\"cat_id[]\" value=\"" . $cat[$i]['id'] . "\" /><input type=\"text\" name=\"cat_order[]\" value=\"" . $cat[$i]['order'] . "\" size=\"1\"/></td>";
          $out .= "<td class=\"alt1\" style=\"text-align: center;\" nowrap><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&page=addedit&edit_cat_id=" . $cat[$i]['id'] . "\">Edit Category</a></td>";
          $out .= "<td class=\"alt1\" style=\"text-align: center;\" nowrap><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&page=delete&cat_id=" . $cat[$i]['id'] . "\">Delete Category</a></td>";
          $out .= "</tr>";
          ++$i;
        }
        if ($total_categories != 0)
        {
          $out .= "<tr>";
          $out .= "<td colspan=\"5\" style=\"text-align: right;\">";
          $out .= "<input type=\"submit\" class=\"ink-button\" name=\"submit\" value=\"Update Order\" />";
          $out .= "</td>";
          $out .= "</tr>";
        }
        else
        {
          $out .= "<tr>";
          $out .= "<td colspan=\"5\" style=\"text-align: center; font-weight: bold;\" class=\"alt1\">";
          $out .= "No categories found in the database";
          $out .= "</td>";
          $out .= "</tr>";
        }
        $out .= "</table>";
        $out .= "</form>";
      }
    }
    else
    {
      if ($page == "addedit")
      {
        $add_submit = isset($_POST['add_submit']) ? 1 : 0;
        $edit_submit = isset($_POST['edit_submit']) ? 1 : 0;
        $exit_process = 0;
        $messages = array ();
        $display_form = 1;
        $do_process = 0;
        if (isset($_POST['edit_cat_id']) || isset($_GET['edit_cat_id']))
        {
          $edit_cat_id = isset($_POST['edit_cat_id']) && ctype_digit($_POST['edit_cat_id']) ? (int) $_POST['edit_cat_id'] : (int) $_GET['edit_cat_id'];
          if (!is_int($edit_cat_id))
          {
            $edit_cat_id = 0;
          }
        }
        else
        {
          $edit_cat_id = 0;
        }
        $cat_name = isset($_POST['cat_name']) ? antiject($_POST['cat_name']) : "";
        $cat_description = isset($_POST['cat_description']) ? antiject($_POST['cat_description']) : "";
        $cat_order = isset($_POST['cat_order']) && ctype_digit($_POST['cat_order']) ? antiject($_POST['cat_order']) : "";
        $cat_sub_id = isset($_POST['cat_sub_id']) && ctype_digit($_POST['cat_sub_id']) ? antiject($_POST['cat_sub_id']) : "";
        if ($add_submit == 1 || $edit_submit == 1)
        {
          $do_process = 1;
        }
        if ($edit_cat_id != 0)
        {
          $page_mode = "edit_submit";
          $submit_name = "Update Category";
          $this_mode_title = "Editing a category";
          if ($do_process == 0)
          {
            if (array_key_exists($edit_cat_id, $categories))
            {
              $cat = $categories[$edit_cat_id];
              $cat_name = $cat['label'];
              $cat_sub_id = $cat['parent'];
              $cat_description = $cat['description'];
              $cat_order = $cat['order'];
            }
            else
            {
              $display_form = 0;
              $out .= get_notification_html("Invalid cat id supplied", ERROR);
            }
          }
        }
        else
        {
          $page_mode = "add_submit";
          $submit_name = "Add Category";
          $this_mode_title = "Adding a new Category";
        }
        if ($do_process == 1)
        {
          if ($cat_name == "")
          {
            $exit_process = 1;
            $messages[] = "Category Name was left blank<br/>";
          }
          if ($cat_order == 0)
          {
            $exit_process = 1;
            $messages[] = "Category ORDER was left blank<br/>";
          }
          if ($exit_process == 1)
          {
            $out .= "<table class=\"tborder\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
            $out .= "<tr>";
            $out .= "<td>";
            $out .= get_notification_html($messages, ERROR);
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "</table>";
          }
          else
          {
            $display_form = 0;
            if ($add_submit == 1)
            {
              $attempt = add_category($cat_name, $cat_sub_id, $cat_description, $cat_order);
              if ($attempt["error"] == True)
              {
                $out .= get_notification_html($attempt["errorMessage"], ERROR);
              }
              else
              {
                $out .= get_notification_html("Successfully added the new category!", SUCCESS);
                $redirect = PREVIOUS_PAGE_SHORT;
                gamecp_log(0, $userdata -> username, "ADMIN - MANAGE CATEGORIES - ADDED - New category: {$cat_name}", 1);
              }
            }
            else if ($edit_submit == 1)
            {
              $attempt = edit_category($cat_name, $cat_sub_id, $cat_description, $cat_order, $edit_cat_id);

              if ($attempt["error"] == True)
              {
                $out .= get_notification_html($attempt["errorMessage"], ERROR);
              }
              else
              {
                $out .= get_notification_html("Successfully updated the category!", SUCCESS);
                $redirect = PREVIOUS_PAGE_SHORT;
                gamecp_log(0, $userdata -> username, "ADMIN - MANAGE CATEGORIES - UPDATE - Category: {$cat_name}", 1);
              }
            }
          }
        }
        if ($display_form == 1)
        {
          generate_menu($categories, 0, "", array (), $sub_cat_id);
          $subcategory_list = $options;
          $out .= "<center><form class=\"ink-form add_cat_form\" method=\"post\">";
          $out .= "<table class=\"\" border=\"0\" width=\"50%\" align=\"center\">";
          $out .= "<tr>";
          $out .= "<td class=\"\" colspan=\"2\" align=\"center\"><strong>" . $this_mode_title . "</strong></td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td class=\"\" width=\"30%\">Category Name</td>";
          $out .= "<td class=\"\"><input type=\"text\" name=\"cat_name\" value=\"" . $cat_name . "\" /></td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td class=\"\">Category Description</td>";
          $out .= "<td class=\"\"><input type=\"text\" name=\"cat_description\" value=\"" . $cat_description . "\" size=\"50\" /></td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td class=\"\">Category Order</td>";
          $out .= "<td class=\"\"><input type=\"text\" name=\"cat_order\" value=\"" . $cat_order . "\" size=\"2\" /></td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td class=\"\">Parent Category</td>";
          $out .= "<td class=\"\">";
          $out .= "<select name=\"cat_sub_id\">";
          $out .= "<option value=\"0\" style=\"background-color: #D3D3D3;\">No parent category</option>";
          $out .= $subcategory_list;
          $out .= "</select>";
          $out .= "</td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td  colspan=\"2\">";
          $out .= "<input type=\"hidden\" name=\"cat_id\" value=\"" . $edit_cat_id . "\" /><input type=\"submit\" class=\"ink-button no_left_margin\" name=\"" . $page_mode . "\" value=\"" . $submit_name . "\" />";
          $out .= "</td>";
          $out .= "</tr>";
          $out .= "</table>";
          $out .= "</form></center>";
        }
      }
      else
      {
        if ($page == "delete")
        {
          $cat_id = isset($_GET['cat_id']) && ctype_digit($_GET['cat_id']) ? intval($_GET['cat_id']) : "";
          if ($cat_id <= 0)
          {
            $out .= get_notification_html("Invalid category id", ERROR);
            $redirect = PREVIOUS_PAGE_LONG;
          }
          else
          {
            if (!array_key_exists($cat_id, $categories))
            {
              $out .= get_notification_html("Invalid category id", ERROR);
              $redirect = PREVIOUS_PAGE_LONG;
            }
            else
            {
              $out .= "<form method=\"post\">";
              $out .= "<p style=\"text-align: center; font-weight: bold;\">Are you sure you want the delete the category: <u>" . $categories[$cat_id]['label'] . "</u>?</p>";
              $out .= "<p style=\"text-align: center;\"><input type=\"hidden\" name=\"cat_id\" value=\"" . $cat_id . "\"/><input type=\"hidden\" name=\"page\" value=\"delete_cat\"/><input type=\"submit\" class=\"ink-button\" name=\"yes\" value=\"Yes\"/> <input class=\"ink-button\" type=\"submit\" name=\"no\" value=\"No\"/></p>";
              $out .= "</form>";
            }
          }
        }
        else
        {
          if ($page == "delete_cat")
          {
            $yes = isset($_POST['yes']) ? "1" : "0";
            $no = isset($_POST['no']) ? "1" : "0";
            if (isset($_POST['cat_id']) && ctype_digit($_POST['cat_id']))
            {
              $cat_id = antiject($_POST['cat_id']);
            }
            else
            {
              $cat_id = 0;
            }
            if ($no != 1 && $cat_id != 0)
            {
              if (!array_key_exists($cat_id, $categories))
              {
                $out .= get_notification_html("Invalid category id", ERROR);
                $redirect = PREVIOUS_PAGE_LONG;
              }
              else
              {
                delete_category($cat_id);
                $out .= get_notification_html("Deleted the category named: " . $categories[$cat_id]["label"], SUCCESS);
                $redirect = CUSTOM_PAGE_SHORT;
                $redirecturl = $script_name . "?action=" . $_GET["action"];
                gamecp_log(3, $userdata -> username, "ADMIN - MANAGE CATEGORIES - DELETED - Category Name:  " . $categories[$cat_id]["label"] . " | Cat ID: " . $cat_id, 1);
              }
            }
            else
            {
              header("Location: {$script_name}?action=" . $_GET['action']);
            }
          }
          else
          {
            $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
            $redirect = INDEX_PAGE_SHORT;
          }
        }
      }
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
