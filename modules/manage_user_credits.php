<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Edit";
  $moduleLabel = "Manage User Credits";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $update = isset($_POST['update']) ? $_POST['update'] : "";
    $page_gen = isset($_GET['page_gen']) ? $_GET['page_gen'] : "1";
    $search_fun = isset($_POST['search_fun']) ? $_POST['search_fun'] : "";
    if ($search_fun == "" && isset($_GET["search_fun"]))
    {
      $search_fun = $_GET["search_fun"];
    }
    $query_p2 = "";
    $search_query = "";
    $exit_process = 0;
    $account_name = isset($_POST['account_name']) ? antiject($_POST['account_name']) : "";
    $account_serial = isset($_POST['account_serial']) ? $_POST['account_serial'] : "";
    if ($account_name == "" && isset($_GET["account_name"]))
    {
      $account_name = antiject($_GET["account_name"]);
    }
    $out .= "<form class=\"ink-form\" method=\"post\">";
    $out .= "<table class=\"\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\" align=\"center\">";
    $out .= "<tr>";
    $out .= "<td colspan=\"2\" style=\" font-weight: bold;\">Look up a user</td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td>Account Serial</td>";
    $out .= "<td><input type=\"text\" name=\"account_serial\" value=\"" . $account_serial . "\"/></td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td>Account Name</td>";
    $out .= "<td><input type=\"text\" name=\"account_name\" value=\"" . $account_name . "\"/></td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td colspan=\"2\"><input class=\"ink-button\" type=\"submit\" name=\"search_fun\" value=\"Look up\"/><input class=\"ink-button\" type=\"submit\" name=\"see_all\" value=\"Clear Search\"/></td>";
    $out .= "</tr>";
    $out .= "</table>";
    $out .= "</form>";
    $out .= "<br/>";
    if ($update != "")
    {
      $user_id = isset($_POST['user_id']) && is_array($_POST['user_id']) ? $_POST['user_id'] : "";
      $user_point = isset($_POST['add_points']) && is_array($_POST['add_points']) ? $_POST['add_points'] : "";
      $user_guild_point = isset($_POST['add_guild_points']) && is_array($_POST['add_guild_points']) ? $_POST['add_guild_points'] : "";
      if ($user_id != "" && $user_point != "" && $user_guild_point != "")
      {
        $count = count($user_id);
        $i = 0;
        while ($i < $count)
        {
          //should be user account id
          $userid = ctype_digit($user_id[$i]) ? intval($user_id[$i]) : "";
          $userpoint = isset($user_point[$i]) ? intval($user_point[$i]) : 0;
          $userGuildPoint = isset($user_guild_point[$i]) ? intval($user_guild_point[$i]) : 0;
          if ($userid != "" && isset($userpoint) && $userpoint != 0)
          {
            $attempt = add_user_credits($userid, $userpoint);
            if ($attempt["error"] == True)
            {
              $out .= get_notification_html("User Serial " . $userid . " Error: " . $attempt["errorMessage"], ERROR);
            }
            else
            {
              gamecp_log(0, $userdata -> username, "ADMIN - MANAGE USERS - UPDATED CREDITS - Account ID: " . $userid . " | Added Points: {$userpoint}", 0);
            }
          }
          if ($userid != "" && isset($userGuildPoint) && $userGuildPoint != 0)
          {
            $attempt = add_user_g_credits($userid, $userGuildPoint);
            if ($attempt["error"] == True)
            {
              $out .= get_notification_html("User Serial " . $userid . " Error: " . $attempt["errorMessage"], ERROR);
            }
            else
            {
              gamecp_log(0, $userdata -> username, "ADMIN - MANAGE USERS - UPDATED GUILD CREDITS - Account ID: " . $userid . " | Added Points: {$userGuildPoint}", 0);
            }
          }
          ++$i;
        }
        if ($exit_process != 1)
        {
          $out .= get_notification_html("Updated Novus Credits", SUCCESS);
        }
      }
    }
    if ($search_fun != "")
    {
      if ($account_name == "" && $account_serial == "")
      {
        $exit_process = 1;
        $out .= get_notification_html("Please fill in an account name or id", ERROR);
      }
      else if (isset($account_name) && $account_name != "")
      {
        $attempt = get_account_serial($account_name);
        $account_id = $attempt['serial'];
      }
      else
      {
        $account_id = $account_serial;
      }
      if ($exit_process == 0)
      {
        if ($account_name != "" or $account_serial != "")
        {
          $search_query .= " user_account_id = '{$account_id}' AND";
        }
      }
    }

    if ($exit_process == 0)
    {
      $out .= "<form class=\"ink-form\" method=\"post\">";
      $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\" align=\"center\">";
      $out .= "<tr>";
      $out .= "<td style=\"text-align: center;\">ID</td>";
      $out .= "<td>Account Name</td>";
      $out .= "<td>Current Credits</td>";
      $out .= "<td>Add Credits (can be negative)</td>";
      $out .= "<td>Guild Credits</td>";
      $out .= "<td>Add Guild Credits</td>";
      $out .= "</tr>";

      include( "./core/pagination.php" );
      $sql = "SELECT user_id, user_points, user_guild_points, user_account_id, convert(varchar,u.id) as name FROM gamecp_gamepoints LEFT JOIN {$mssql['user']['db']}.dbo.tbl_UserAccount u on serial = user_account_id WHERE " . $search_query;

      if ($search_query != "")
      {
        $search_query = "WHERE " . $search_query . " 1=1 ";
      }

      $sql .= " user_id NOT IN ( SELECT TOP [OFFSET] user_id FROM gamecp_gamepoints " . $search_query;
      $sql .= " ORDER BY user_id DESC) ORDER BY user_id DESC";
      $sql_count = "SELECT count(user_id) FROM gamecp_gamepoints " . $search_query;

      $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
      $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

      $pager = new Pagination(RFCP, $sql, $sql_count, $url, array (), array (), $page_size = 20, $links_to_show = 11);
      $results = $pager -> get_data();


      foreach ($results["rows"] as $key => $row)
      {
        $out .= "<tr>";
        $out .= "<td width=\"30\" style=\"text-align: center;\"><input type=\"hidden\" name=\"user_id[]\" value=\"" . $row['user_account_id'] . "\" />" . $row['user_account_id'] . "</td>";
        $out .= "<td>" . filter_string_for_html($row['name']) . "</td>";
        $out .= "<td style=\"text-align: left;\">" . $row['user_points'] . "</td>";
        $out .= "<td><input type=\"text\" name=\"add_points[]\" value=\"0\" /></td>";
        $out .= "<td style=\"text-align: left;\">" . $row['user_guild_points'] ?: 0 . "</td>";
        $out .= "<td><input type=\"text\" name=\"add_guild_points[]\" value=\"0\" /></td>";
        $out .= "</tr>";
      }
      if (0 < count($results["rows"]))
      {
        $out .= "<tr>";
        $out .= "<td colspan=\"6\" style=\"text-align: center;\"><input type=\"hidden\" name=\"account_name\" value=\"" . $account_name . "\"/><input class=\"ink-button\" type=\"submit\" name=\"update\" value=\"Update Points\" /></td>";
        $out .= "</tr>";
        $out .= "<tr>";
        $out .= "<td colspan=\"6\" style=\"text-align: center;\">" . $pager -> renderFullNav() . "</td>";
        $out .= "</tr>";
      }
      else
      {
        $out .= "<tr>";
        $out .= "<td colspan=\"3\" style=\"text-align: center;\">No users found</td>";
        $out .= "</tr>";
      }
      $out .= "</table>";

      if ($search_fun != "")
      {
        $out .= "<input type=\"hidden\" name=\"search_fun\" value=\"Look up\"/>";
        $out .= "<input type=\"hidden\" name=\"account_name\" value=\"" . $account_name . "\"/>";
      }

      $out .= "</form>";
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}

