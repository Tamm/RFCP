<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $permission = "player";
  return;
}
if ($this_script == $script_name)
{
  /**
   * @author Mauro Tamm   @ mauro.tamm@gmail.com
   * @author Agony @ agony@nxtdeveloper.com
   * @copyright 2014 http://nxtdeveloper.com/
   * @ver 1.0
   */
  if ($userdata -> loggedin == True)
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $new_ticket = isset($_POST['new_ticket']) ? $_POST['new_ticket'] : "";
    $ticket = isset($_POST['ticket']) ? $_POST['ticket'] : "";
    $title = isset($_POST['title']) ? $_POST['title'] : "";
    $type = isset($_POST['type']) && ctype_digit($_POST['type']) ? intval($_POST['type']) : 0;
    $typepermission = 0;
    $error = false;
    if (empty($page))
    {
      if ($new_ticket != "" && $ticket != "" && $title != "")
      {
        $psql = "SELECT permission FROM ticket_categories WHERE id = {$type}";
        $pstmt = sqlsrv_query(connectdb(RFCP), $psql);
        sqlsrv_fetch($pstmt);
        $typepermission = sqlsrv_get_field($pstmt, 0);

        $ticketentry = "INSERT INTO gamecp_ticket_entry (type, account, title, status, permission) VALUES (?,?,?,?,?)";
        $evalues = array ($type, $userdata -> username, $title, 0, $typepermission);
        $tadd_e = sqlsrv_query(connectdb(RFCP), $ticketentry, $evalues);
        if ($tadd_e === false)
        {
          $error = true;
          $out .= get_notification_html(array ('Error adding New Ticket Entry'), ERROR);
        }
        else
        {
          $sql = "SELECT TOP 1 id FROM gamecp_ticket_entry ORDER BY id DESC";
          $tdata = sqlsrv_query(connectdb(RFCP), $sql);
          sqlsrv_fetch($tdata);
          $id = sqlsrv_get_field($tdata, 0);
          if ($tdata === false)
          {
            $error = true;
            $out .= get_notification_html(array ('Error retrieving ID data'), ERROR);
          }
          else
          {
            $ticketdata = "INSERT INTO gamecp_ticket_data (entryid, isgm, name, ticket, replyid) VALUES (?,?,?,?,?)";
            $dvalues = array ($id, 0, $userdata -> username, $ticket, 0);

            $tadd_d = sqlsrv_query(connectdb(RFCP), $ticketdata, $dvalues);
            if ($tadd_d === false)
            {
              $error = true;
              $out .= get_notification_html(array ('Error adding New Ticket Data'), ERROR);
            }
            else
            {
              $url = "./" . $script_name . "?action=show_ticket&id=" . $id;
              header('Location: ' . $url);
            }
          }
        }
      }
      else if ($new_ticket != "" && ($ticket == "" or $title == ""))
      {
        if ($ticket == "")
        {
          $out .= get_notification_html(array ('Empty Description'), ERROR);
        }
        if ($title == "")
        {
          $out .= get_notification_html(array ('Empty Title'), ERROR);
        }
        $new_ticket = "";
      }
      if ($new_ticket == "" || $error)
      {
        $categories = get_ticket_category_list();
        $category = $categories['categories'];
        $csize = sizeof($category);

        $out .= "<div style = 'width: 1000px; margin: 25px 50px 25px 50px; height:auto; min-height:500px; border-top: solid #bbb; border-width:thin;'>";

        $out .= "<form class='ink-form' method='post' action='" . "./" . $script_name . "?action=" . $_GET['action'] . "' style = 'margin-left: 165px; width: 670px; height:auto; min-height:250px;'>";
        $out .= "<div style = 'width: 670px; padding-bottom: 10px; padding-top: 30px;'>";
        $out .= "<select name='type'>";
        for ($i = 0; $i < $csize; $i++)
        {
          $out .= "<option value='" . $category[$i]['id'] . "'>" . $category[$i]['category'] . "</option>";
        }
        $out .= "</select>";
        $out .= "</div>";
        $out .= "<div style = 'width: 670px; padding-bottom: 10px; padding-top: 10px;'>";
        $out .= "<input type='text'  name='title' placeholder='Title' style = 'width: 100%; resize:vertical; padding-bottom: 7px; padding-left: 10px; padding-right: 10px; padding-top: 7px;'/>";
        $out .= "</div>";
        $out .= "<div style = 'width: 670px; padding-bottom: 20px; padding-top: 0px;'>";
        $out .= "<textarea name='ticket' placeholder='Problem Description' id='postadd'></textarea>";
        $out .= "</div>";
        $out .= "<div style = 'width: 670px; height: 30px; margin-top: 10px; text-align: right;'>";
        $out .= "<input class ='ink-button' type='submit' name='new_ticket' value='Create Ticket'/>";
        $out .= "</div>";
        $out .= "</form>";

        $out .= "</div>";
        $out .= "<div style = 'clear:both;'></div>";

        $out .= "<link rel='stylesheet' type='text/css' href='../framework/jquery.cleditor.css' />";
        $out .= "<script src='https://code.jquery.com/jquery-2.1.1.js'></script>";
        $out .= "<script type='text/javascript'' src='../framework/jquery.cleditor.min.js''></script>";
        $out .= "<script type='text/javascript'>";
        $out .= "$(document).ready(function () { $('#postadd').cleditor(); });";
        $out .= "</script>";
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = PREVIOUS_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
