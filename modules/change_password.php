<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "User";
  $moduleLabel = "Change Password";
  $permission = "player";
  return;
}

if ($this_script == $script_name)
{
  $page = isset($_GET['page']) ? $_GET['page'] : "";
  $show_form = true;
  if ($userdata -> loggedin == True)
  {
    if ($page == "submit")
    {
      $oldpassword = antiject($_REQUEST['oldpassword']);
      $newpassword = antiject($_REQUEST['newpassword']);
      $comfpassword = antiject($_REQUEST['comfpassword']);
      $messages = array ();
      if (!preg_match(REGEX_PASSWORD, $oldpassword) || !preg_match(REGEX_PASSWORD, $newpassword) || !preg_match(REGEX_PASSWORD, $comfpassword))
      {
        $messages[] = "Invalid password. Passwords can contain letters, numbers, and special characters(<b>!@#$%^&*</b>)";
      }
      if ($newpassword != $comfpassword)
      {
        $messages[] = "New password and comfirm password fields must match.<br/>";
      }
      if (empty($newpassword) || empty($comfpassword) || empty($oldpassword))
      {
        $messages[] = "You left a blank field.<br/>";
      }
      if (strlen($newpassword) < 4 || 12 < strlen($newpassword))
      {
        $error_msg = "Your password must be between  4 and  12 characters long.<br/>";
      }

      $attempt = $userdata -> get_password();
      if ($attempt["error"] == True)
      {
        $messages[] = $attempt["errorMessage"];
      }
      if (trim($attempt['password']) != $oldpassword)
      {
        $messages[] = "Your old password did not match the one you specified<br/>";
      }
      if (trim($attempt['password']) == $newpassword)
      {
        $messages[] = "Your old password and new password are the same.<br/>";
      }

      if (count($messages) == 0)
      {
        $attempt = $userdata -> set_new_password($newpassword);
        if ($attempt["error"] == True)
        {
          $out .= get_notification_html($attempt["error"], ERROR);
        }
        else
        {
          $out .= get_notification_html("Password changed! You need to log in again (will be redirected)", SUCCESS);
          $show_form = false;
          exit_without_error();
          gamecp_log(1, $userdata -> username, "GAMECP - CHANGE PASSWORD", 1);
          $redirect = INDEX_PAGE_SHORT;
        }
      }
      else
      {
        $out .= get_notification_html($messages, ERROR);
      }
    }
    if ($show_form)
    {
      $out .= "<form class=\"ink-form\" method=\"POST\" action=\"" . $script_name . "?action=change_password&amp;page=submit\">";
      $out .= "<div><b>Between 4 and 12 characters. Can contain letters, numbers, and special characters(<b>!@#$%^&*</b>). Passwords are case sensitive!</b></div>";
      $out .= "<table border=\"0\">";
      $out .= "<tr>";
      $out .= "<td style=\"width: 200px;\">Old Password: </td>";
      $out .= "<td><input type=\"password\" name=\"oldpassword\" style=\"width: 200px;\"></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>New Password: </td>";
      $out .= "<td><input type=\"password\" name=\"newpassword\" style=\"width: 200px;\"></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Comfirm Password: </td>";
      $out .= "<td><input type=\"password\" name=\"comfpassword\" style=\"width: 200px;\"></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td colspan=\"2\" align=\"left\" style=\"padding-top: 5px;\"><input class=\"ink-button\" type=\"submit\" value=\"Change Password\" style=\"margin-left:0px;\"></td>";
      $out .= "</tr>";
      $out .= "</table>";
      $out .= "</form>";
    }
  }
}
