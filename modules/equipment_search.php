<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Support";
  $moduleLabel = "Equip Search";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{

  /**
   * @author Mauro Tamm   @ mauro.tamm@gmail.com
   * @author Agony @ agony@nxtdeveloper.com
   * @copyright 2014 http://nxtdeveloper.com/
   * @ver 1.0
   */
  if ($userdata -> has_permission($action))
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $page_size = isset($_GET['page_size']) && ctype_digit($_GET['page_size']) ? intval($_GET['page_size']) : 10;
    $do_search = isset($_GET['do_search']) ? $_GET['do_search'] : "";
    $item_slots = isset($_GET['item_slots']) && ctype_digit($_GET['item_slots']) ? antiject($_GET['item_slots']) : 0;
    $item_upgrades = isset($_GET['talics']) ? $_GET['talics'] : array ();
    $item_id = isset($_GET['item_id']) ? antiject(trim($_GET['item_id'])) : "";
    $item_amount_max = isset($_GET['item_amount_max']) && ctype_digit($_GET['item_amount_max']) ? intval($_GET['item_amount_max']) : 0;
    $item_amount_min = isset($_GET['item_amount_min']) && ctype_digit($_GET['item_amount_max']) ? intval($_GET['item_amount_min']) : 0;
    $search_query = "";
    $enable_itemsearch = false;
    $item_query = "";
    $item_upgrade = 268435455;
    $equipment_names = array ("Upper", "Lower", "Gloves", "Shoes", "Head", "Shield",
      "Weapon", "Cloak");
    $accessores_names = array (0 => "-", 8 => "Ring", 9 => "Amulet", 10 => "Ammo");
    $character_serial = isset($_GET['character_serial']) && ctype_digit($_GET['character_serial']) ? intval($_GET['character_serial']) : 0;
    $character_name = isset($_GET['character_name']) ? $_GET['character_name'] : "";

    if (empty($page))
    {
      $out .= "<form class='ink-form' method='get' action='" . $script_name . "?action=" . $_GET['action'] . "'>";
      $out .= "<table class='ink-form' cellpadding='3' cellspacing='1' border='0' width='100%' style='font-size: 12pt;'>";
      $out .= "<tr>";
      $out .= "<td colspan='2' style=''><b>Search Equipment</b></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Character Serial:</td>";
      $out .= "<td ><input type='text' name='character_serial' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Character Name:</td>";
      $out .= "<td ><input type='text' name='character_name' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Item Code:</td>";
      $out .= "<td ><input type='text' name='item_id' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Upgrades:</td>";
      $out .= "<td>";
      $out .= generate_upgrade_input_html(array ());
      $out .= "</td>";
      $out .= "<tr>";
      $out .= "<td>Characters per page:</td>";
      $out .= "<td ><select name='page_size'>";
      $v10 = "";
      if ($page_size == 10)
      {
        $v10 = "selected";
      }
      $out .= "<option value='10'" . $v10 . ">10</option>";
      $v15 = "";
      if ($page_size == 15)
      {
        $v15 = "selected";
      }
      $out .= "<option value='15'" . $v15 . ">15</option>";
      $v25 = "";
      if ($page_size == 25)
      {
        $v25 = "selected";
      }
      $out .= "<option value='25'" . $v25 . ">25</option>";
      $v50 = "";
      if ($page_size == 50)
      {
        $v50 = "selected";
      }
      $out .= "<option value='50'" . $v50 . ">50</option>";
      $v100 = "";
      if ($page_size == 100)
      {
        $v100 = "selected";
      }
      $out .= "<option value='100'" . $v100 . ">100</option>";
      $out .= "</select></td> ";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td colspan='2'><input type='hidden' name='action' value='" . $_GET['action'] . "'><input class='ink-button' type='submit' value='Search' name='do_search' style='margin-left: 0px;'/></td>";
      $out .= "</tr>";
      $out .= "</table>";
      $out .= "</form>";

      $messages = array ();
      if ($do_search != "")
      {
        $out .= "<br/><br/>";

        if ($item_id == "" && $character_serial == 0 && $character_name == "" && $item_slots == 0)
        {
          $messages[] = "Please fill in either the Account Serial, Account Name or Item ID";
        }
        else if ($character_serial > 0 && $item_slots > 0 or $character_name != "" && $item_slots > 0)
        {
          $messages[] = "Cannot search by slots and character name/id at the same time";
        }
        else if ($character_serial > 0 && $item_id != "" or $character_name != "" && $item_id != "")
        {
          $messages[] = "Cannot search by Item ID and character name/id at the same time";
        }
        else if (count($messages) == 0)
        {
          $item_id = str_replace(" ", "", $item_id);
          $item_id = antiject($item_id);
          $item_kind = parse_item_code($item_id);

          $search_query .= " WHERE ";
          if ($character_name != "")
          {
            $attempt = get_character_info($character_name);
            $character_serial = $attempt["char"]["Serial"];
            $messages[] = $attempt["errorMessage"];
            $search_query .= "B.Name = '{$character_name}'";
          }
          else if ($character_serial > 0)
          {
            $search_query .= "B.Serial = '{$character_serial}'";
          }
          if ($item_id != "")
          {
            $attempt = get_item_info_from_code($item_id);
            $item_id_start = ceil(65536 * $item_id + ( $item_kind * 256));
            $item_db_id = $attempt['item']["item_id"];
            if ($attempt["error"] == True)
            {
              $messages[] = $attempt["errorMessage"];
            }
            else
            {
              if ($character_name != "" or $character_serial > 0 && $item_slots == 0)
              {
                $search_query .= " OR ";
              }

              if ($item_kind <= 7)
              {
                $item_query .= " B.EK{$item_kind} = '{$item_db_id}' ";
              }
              else if ($item_kind >= 8 && $item_kind <= 10 && $item_slots == 0)
              {
                $search_query .= "(G.EK0 = '{$item_id_start}' OR G.EK1 = '{$item_id_start}'";
                $search_query .= " OR G.EK2 = '{$item_id_start}' OR G.EK3 = '{$item_id_start}'";
                $search_query .= " OR G.EK4 = '{$item_id_start}' OR G.EK5 = '{$item_id_start}') ";
              }
              else
              {
                $item_query .= " B.EK0 = '{$item_db_id}'";
              }
              if ($item_slots == 0)
              {
                $search_query .= $item_query;
              }
              $enable_itemsearch = true;
            }
          }
          if (count($item_upgrades) > 0)
          {
            $empty = array ();
            $item_ups = 0;
            $attempt = get_item_info_from_code($item_id);
            $temp_id = $attempt['item']["item_id"];
            foreach ($item_upgrades as $index => $talic)
            {
              $temp = intval($talic);
              if ($temp < 0 || $temp == 14 || $temp > 15)
              {
                $messages[] = "Talic " . ($index + 1) . " is not a valid talic.";
              }

              if ($temp > 0 && $temp < 14 && count($empty) > 0)
              {
                $messages[] = "Talic " . ($index + 1) . " cannot be set if all previous talics are not set.";
              }
              else if ($temp > 0 && $temp < 14)
              {
                $item_ups++;
              }

              if ($temp == 15)
              {
                $empty[] = $index;
              }
            }

            if ($item_slots < $item_ups)
            {
              $item_ups = $item_slots;
            }
            if ($item_slots > 7)
            {
              $item_slots = 7;
            }
            if (count($item_upgrades) < 7)
            {
              $i = count($item_upgrades);
              while ($i < 7)
              {
                $item_upgrades[] = 15;
                $i++;
              }
            }

            $item_upgrade = $item_slots . "";
            foreach (array_reverse($item_upgrades) as $index => $talic)
            {
              $item_upgrade .= dechex($talic);
            }
            $item_upgrade = hexdec($item_upgrade);
            $search_query .= "(";
            for ($i = 0; $i < 8; $i++)
            {
              if (!$enable_itemsearch)
              {
                $append_k = " AND B.EK{$i}  != '-1'";
              }
              else
              {
                $append_k = " AND B.EK{$i}  = {$temp_id}";
              }

              $search_query .= "( B.EU{$i} = {$item_upgrade}" . $append_k . ")";
              if ($i < 7)
              {
                $search_query .= " OR ";
              }
            }
            $search_query .= ")";
            $enable_itemsearch = true;
          }
          include( "./core/pagination.php" );

          $sql = "SELECT B.Serial,B.Name,B.Race,B.Lv,B.Account,B.AccountSerial,B.EK0,B.EK1,B.EK2,B.EK3,B.EK4,B.EK5,B.EK6,B.EK7,B.EU0,B.EU1,B.EU2,B.EU3,B.EU4,B.EU5,B.EU6,B.EU7,";
          $sql .= "G.EK0 AS GK0, G.EK1 AS GK1, G.EK2 AS GK2, G.EK3 AS GK3, G.EK4 AS GK4, G.EK5 AS GK5, G.ED0,G.ED1,G.ED2,G.ED3,G.ED4,G.ED5 ";
          $sql .= "FROM tbl_base AS B INNER JOIN tbl_general AS G ON B.Serial = G.Serial {$search_query} ";
          $sql .= "AND B.Serial NOT IN ( SELECT TOP [OFFSET] B.Serial FROM tbl_base AS B ";
          $sql .= "INNER JOIN tbl_general AS G ON B.Serial = G.Serial {$search_query} ORDER BY B.Serial DESC) ORDER BY B.Serial DESC";

          $sql_count = "SELECT COUNT(B.Serial) FROM tbl_base AS B INNER JOIN tbl_general AS G ON B.Serial = G.Serial  {$search_query}";

          $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
          $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

          $pager = new Pagination(DATA, $sql, $sql_count, $url, array (), array (), $page_size, $links_to_show = 11);
          $results = $pager -> get_data();


          $out .= "<table class = 'ink-table hover'>";
          $out .= "<thead>";
          $out .= "<tr>";
          $out .= "<th class='align-left'>Slot</th>";
          $out .= "<th class='align-left'>Item Code</th>";
          $out .= "<th class='align-left'>Item Icon</th>";
          $out .= "<th class='align-left'>Item Name</th>";
          $out .= "<th class='align-left'>Upgrades</th>";
          $out .= "<th class='align-left'>Amount</th>";
          $out .= "</tr>";
          $out .= "</thead>";
          $out .= "<tbody>";

          foreach ($results["rows"] as $key => $row)
          {
            $item_details = array ();
            $item_info = array ();

            $out .= "<thead>";
            $out .= "   <tr>";
            $out .= "       <th class ='thead'  colspan='6'>Character Serial: " . $row['Serial'] . " " . "Charcter Name: " . $row['Name'] . "</th>";
            $out .= "   </tr>";
            $out .= "</thead>";

            for ($i = 0; $i < 8; $i++)
            {
              $ek_value = $row["EK{$i}"];
              $eu_value = $row["EU{$i}"];

              if ($ek_value < 0)
              {
                $item_id = -1;
                $item_code = "-";
                $images = "";
                $item_image = "";
                $item_name = "<i>No item equipped</i>";
              }
              else
              {
                $item_details = get_item_details($ek_value, $eu_value, $i);
                if ($item_details["type"] < 36 && $i > -1)
                {
                  $item_info = get_item_info_from_id($item_details["type"], $item_details["id"]);
                  $item_id = $item_details["id"];
                  $item_code = $item_info["item"]["item_code"];
                  $item_name = $item_info["item"]["item_name"];
                  $images = $item_details["images"];
                  $icon_id = $item_info["item"]["item_icon_id"];
                  $item_path = glob("./images/items/" . get_image_folder_name($item_details["type"]) . "/(" . $item_info["item"]["item_icon_id"] . "){.jpg,.JPG,.gif,.GIF,.png,.PNG}", GLOB_BRACE);
                  if (file_exists(@$item_path[0]))
                  {
                    $item_image = "<img id='' class='market_item_image' src='" . $item_path[0] . "' style='height: 40px; width: 40px;'>";
                  }
                  else
                  {
                    $item_image = "";
                  }
                }
                else
                {
                  $item_name = "<i>No item equipped</i>";
                  $item_code = "-";
                }
              }
              if ($eu_value == BASE_CODE or $eu_value == -1 or $images == "")
              {
                $images = "No Upgrade";
              }

              $out .= "<tr>";
              if ($item_id != -1)
              {
                if ($item_code == strtolower($_GET['item_id']) && $item_slots == 0)
                {
                  $out .= "<td>" . $equipment_names[$i] . "</td>";
                  $out .= "<td>" . $item_code . "</td>";
                  $out .= "<td>" . $item_image . "</td>";
                  $out .= "<td>" . $item_name . "</td>";
                  $out .= "<td>" . $images . "</td>";
                  $out .= "<td>-</td>";
                }
                else if ($eu_value == $item_upgrade && $item_slots > 0 && $item_code != "-" && strtolower($_GET['item_id']) == "")
                {
                  $out .= "<td>" . $equipment_names[$i] . "</td>";
                  $out .= "<td>" . $item_code . "</td>";
                  $out .= "<td>" . $item_image . "</td>";
                  $out .= "<td>" . $item_name . "</td>";
                  $out .= "<td>" . $images . "</td>";
                  $out .= "<td>-</td>";
                }
                else if ($eu_value == $item_upgrade && $item_slots > 0 && $item_code != "-" && $item_code == strtolower($_GET['item_id']))
                {
                  $out .= "<td>" . $equipment_names[$i] . "</td>";
                  $out .= "<td>" . $item_code . "</td>";
                  $out .= "<td>" . $item_image . "</td>";
                  $out .= "<td>" . $item_name . "</td>";
                  $out .= "<td>" . $images . "</td>";
                  $out .= "<td>-</td>";
                }
                else if ($character_serial > 0 or $character_name != "")
                {
                  $out .= "<td>" . $equipment_names[$i] . "</td>";
                  $out .= "<td>" . $item_code . "</td>";
                  $out .= "<td>" . $item_image . "</td>";
                  $out .= "<td>" . $item_name . "</td>";
                  $out .= "<td>" . $images . "</td>";
                  $out .= "<td>-</td>";
                }
              }
              else if ($item_id == -1)
              {
                if ($character_serial > 0 or $character_name != "")
                {
                  $out .= "<td>" . $equipment_names[$i] . "</td>";
                  $out .= "<td>" . $item_code . "</td>";
                  $out .= "<td>" . $item_image . "</td>";
                  $out .= "<td>" . $item_name . "</td>";
                  $out .= "<td>" . $images . "</td>";
                  $out .= "<td>-</td>";
                }
              }
              $out .= "</tr>";
            }

            for ($i = 0; $i < 6; $i++)
            {
              $k_value = $row["GK{$i}"];
              if ($k_value < 0)
              {

                $item_id = "-";
                $item_code = "-";
                $images = "";
                $item_image = "";
                $item_name = "<i>No item equipped</i>";
              }
              else
              {
                $item_details = get_item_details($k_value, 0);
                if ($item_details["type"] < 36)
                {
                  $item_info = get_item_info_from_id($item_details["type"], $item_details["id"]);
                  $item_code = $item_info["item"]["item_code"];
                  $item_name = $item_info["item"]["item_name"];
                  $icon_id = $item_info["item"]["item_icon_id"];
                  $item_path = glob("./images/items/" . get_image_folder_name($item_details["type"]) . "/(" . $item_info["item"]["item_icon_id"] . "){.jpg,.JPG,.gif,.GIF,.png,.PNG}", GLOB_BRACE);
                  if (file_exists(@$item_path[0]))
                  {
                    $item_image = "<img id='' class='market_item_image' src='" . $item_path[0] . "' style='height: 40px; width: 40px;'>";
                  }
                  else
                  {
                    $item_image = "";
                  }
                }
                else
                {
                  $item_name = "<i>No item equipped</i>";
                  $item_code = "-";
                }
                $out .= "<tr>";
              }

              if ($k_value != -1)
              {

                if ($item_code == strtolower($_GET['item_id']))
                {
                  $out .= "<td nowrap>" . $accessores_names[$item_details["type"]] . "</td>";
                  $out .= "<td nowrap>" . $item_code . "</td>";
                  $out .= "<td nowrap>" . $item_image . "</td>";
                  $out .= "<td nowrap>" . $item_name . "</td>";
                  $out .= "<td nowrap>-</td>";
                  $out .= "<td nowrap>" . $row["ED{$i}"] . "</td>";
                }
                else if ($character_serial > 0 or $character_name != "")
                {
                  $out .= "<td nowrap>" . $accessores_names[$item_details["type"]] . "</td>";
                  $out .= "<td nowrap>" . $item_code . "</td>";
                  $out .= "<td nowrap>" . $item_image . "</td>";
                  $out .= "<td nowrap>" . $item_name . "</td>";
                  $out .= "<td nowrap>-</td>";
                  $out .= "<td nowrap>" . $row["ED{$i}"] . "</td>";
                }
              }
              else if ($k_value == -1)
              {
                if ($character_serial > 0 or $character_name != "")
                {
                  $out .= "<td nowrap>-</td>";
                  $out .= "<td nowrap>" . $item_code . "</td>";
                  $out .= "<td nowrap>" . $item_image . "</td>";
                  $out .= "<td nowrap>" . $item_name . "</td>";
                  $out .= "<td nowrap>-</td>";
                  $out .= "<td nowrap>" . $row["ED{$i}"] . "</td>";
                }
              }
              $out .= "</tr>";
            }
          }

          if ($enable_itemsearch == true)
          {
            $out .= "</tbody>";
            $out .= "<tfoot>";
            if (count($results["rows"]) <= 0)
            {
              $out .= "<tr>";
              $out .= "<td  colspan='9' style='text-align: center; font-weight: bold;'>No items  found.</td>";
              $out .= "</tr>";
            }
            else
            {
              $out .= "<tr>";
              $out .= "<td  colspan='9' style='text-align: center; font-weight: bold;'>" . $pager -> renderFullNav() . "</td>";
              $out .= "</tr>";
            }
            $out .= "</tfoot>";
          }
          $out .= "</table>";
          if (!isset($item_code))
          {
            $item_code = "";
          }
          gamecp_log(0, $userdata -> username, "ADMIN - EQUIP SEARCH - Searched for: {$character_name} or {$character_serial} or {$item_code}", 1);
        }
        if (count($messages) > 0)
        {
          $out .= get_notification_html($messages, ERROR);
        }
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = PREVIOUS_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
