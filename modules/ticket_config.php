<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Administration";
  $moduleLabel = "Ticket Config";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  /**
   * @author Mauro Tamm   @ mauro.tamm@gmail.com
   * @author Agony @ agony@nxtdeveloper.com
   * @copyright 2014 http://nxtdeveloper.com/
   * @ver 1.0
   */
  $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
  if ($userdata -> has_permission($action))
  {
    if (empty($page))
    {
      $conf = get_ticketconfig();
      $permission = get_tpermission_data();
      $permissiondata = $permission["data"];
      if ($conf['error'] == "" && $permission['error'] == "")
      {
        $out .= "<div style = 'width: 1100px; margin-top:5px; padding-bottom:5px'>";
        $out .= "<b>Ticket Categories</b>";
        $out .= "<div style = 'width: 1100px; border-top: solid #bbb; border-width:thin; margin-top:5px; padding-top:15px'>";
        $out .= "<div  style='padding: 7px; width: 800px; margin-left: 195px; height: auto'>";
        $out .= "<span style='text-align: left; float:left; width: 230px'><b>Ticket Category</b></span>";
        $out .= "<span style='text-align: left; float:left; width: 230px'><b>Category ID</b></span>";
        $out .= "<span style='text-align: left; float:left; width: 230px'><b>Permission Rank</b></span>";
        $out .= "</div >";
        $out .= "<form id = 'categorydata' style='width: 800px; margin-left: 195px; height: auto' method='post' class='ink-form'>";

        $category = $conf['categories'];
        $csize = sizeof($category);

        for ($i = 0; $i < $csize; $i++)
        {
          $out .= "<div  style='padding: 7px; float: left; width: 230px'>";
          $out .= "<input type='text' name='category[" . $i . "]' size='40' value='" . $category[$i]['category'] . "' onfocusout='update_category(this.value," . $category[$i]['id'] . ")'/>";
          $out .= "</div >";
          $out .= "<div  style=' padding: 7px; float: left; width: 230px'>";
          $out .= "<input type='text' name='id[" . $i . "]' size='40' value='" . $category[$i]['id'] . "' onfocusout='update_id(this.value," . $category[$i]['id'] . ")'/>";
          $out .= "</div >";
          $out .= "<div  style='padding: 7px; float: left; width: 230px'>";
          $out .= "<input type='text' name='permission[" . $i . "]' size='40' value='" . $category[$i]['permission'] . "' onfocusout='update_permission(this.value," . $category[$i]['id'] . ")'/>";
          $out .= "</div >";
          $out .= "<div style='padding: 7px 1px 1px 15px; float: left; width: 30px; margin-top: 6px'><a class='icon_link " . BASE_COLOR . " 'href='" . $_SERVER['REQUEST_URI'] . "&page=delete&id=" . $category[$i]['id'] . "'><i class='icon-trash icon-large' title='Delete Category'></i></a>";
          $out .= "</div >";
        }
        $out .= "</form>";
        $out .= "<div style = 'clear:both;'></div>";
        $out .= "<div style='width: 800px; margin-left: 195px; margin-top: 35px; width: 30px'><a class='icon_link " . BASE_COLOR . " 'href='" . $_SERVER['REQUEST_URI'] . "&page=add'><i class='icon-plus icon-large' title='Add Category'></i></a></div>";
        $out .= "</div>";

        $out .= "<b>Ticket Permissions</b>";
        $out .= "<div style = 'width: 1100px; border-top: solid #bbb; border-width:thin; margin-top:5px; padding-top:15px'>";
        $out .= "<div  style='padding: 7px; width: 800px; margin-left: 195px; height: auto'>";
        $out .= "<span style='text-align: left; float:left; width: 230px'><b>Account Serial</b></span>";
        $out .= "<span style='text-align: left; float:left; width: 230px'><b>Permission Rank</b></span>";
        $out .= "</div >";

        $out .= "<form id = 'permissiondata' style='width: 800px; margin-left: 195px; height: auto' method='post' class='ink-form'>";

        $psize = sizeof($permissiondata);
        for ($i = 0; $i < $psize; $i++)
        {
          $out .= "<div  style='padding: 7px; float: left; width: 230px'>";
          $out .= "<input type='text' name='aserial[" . $i . "]' size='40' value='" . $permissiondata[$i]['aserial'] . "' onfocusout='update_aserial(this.value," . $permissiondata[$i]['aserial'] . ")'/>";
          $out .= "</div >";
          $out .= "<div  style=' padding: 7px; float: left; width: 230px'>";
          $out .= "<input type='text' name='rank[" . $i . "]' size='40' value='" . $permissiondata[$i]['rank'] . "' onfocusout='update_rank(this.value," . $permissiondata[$i]['aserial'] . ")'/>";
          $out .= "</div >";
          $out .= "<div  style='padding: 7px; float: left; width: 230px'>";
          $out .= "<div style='padding: 7px 1px 1px 15px; float: left; width: 30px; margin-top: 6px'><a class='icon_link " . BASE_COLOR . " 'href='" . $_SERVER['REQUEST_URI'] . "&page=pdelete&aserial=" . $permissiondata[$i]['aserial'] . "'><i class='icon-trash icon-large' title='Remove Account'></i></a></div>";
          $out .= "</div >";
        }
        $out .= "</form>";
        $out .= "<div style = 'clear:both;'></div>";
        $out .= "<div style='width: 800px; margin-left: 195px; margin-top: 35px; width: 30px'><a class='icon_link " . BASE_COLOR . " 'href='" . $_SERVER['REQUEST_URI'] . "&page=padd'><i class='icon-plus icon-large' title='Add Permissions'></i></a></div>";

        $out .= "</div >";
        $out .= "<div style = 'width: 1100px; border-top: solid #bbb; border-width:thin; margin-top:15px; padding-top:15px'></div>";
        $out .= "</div>";
      }
      else
      {
        $out .= get_notification_html($conf['error'], ERROR);
        $redirect = INDEX_PAGE_SHORT;
      }
    }
    else
    {
      if ($page == "add")
      {
        $csql = "INSERT INTO ticket_categories VALUES ((SELECT (MAX(id)) + 1 FROM ticket_categories),?,?)";
        $cdata = sqlsrv_query(connectdb(RFCP), $csql, array ('Category Name', 1));
        if ($cdata === false)
        {
          $out .= get_notification_html(array ('Error adding category'), ERROR);
        }
        gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - ADDED Ticket Category", 1);
        $url = $script_name . "?action=" . $_GET['action'];
        header('Location: ./' . $url);
      }
      elseif ($page == "padd")
      {
        $csql = "INSERT INTO ticket_permission VALUES ((SELECT (MAX(aserial)) + 200000 FROM ticket_permission),0)";
        $cdata = sqlsrv_query(connectdb(RFCP), $csql);
        if ($cdata === false)
        {
          $out .= get_notification_html(array ('Error adding permissions'), ERROR);
        }
        gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - ADDED Ticket Permissions", 1);
        $url = $script_name . "?action=" . $_GET['action'];
        header('Location: ./' . $url);
      }
      elseif ($page == "delete")
      {
        $csql = "DELETE FROM  ticket_categories WHERE id = ?";
        $cdata = sqlsrv_query(connectdb(RFCP), $csql, array ($_GET['id']));
        if ($cdata === false)
        {
          $out .= get_notification_html(array ('Error deleting category'), ERROR);
        }
        gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - DELETED Ticket Category", 1);
        $url = $script_name . "?action=" . $_GET['action'];
        header('Location: ./' . $url);
      }
      elseif ($page == "pdelete")
      {
        $csql = "DELETE FROM  ticket_permission WHERE aserial = ?";
        $cdata = sqlsrv_query(connectdb(RFCP), $csql, array ($_GET['aserial']));
        if ($cdata === false)
        {
          $out .= get_notification_html(array ('Error deleting permissions'), ERROR);
        }
        gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - DELETED Permissions", 1);
        $url = $script_name . "?action=" . $_GET['action'];
        header('Location: ./' . $url);
      }
      else
      {
        $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
        $redirect = INDEX_PAGE_SHORT;
      }
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}