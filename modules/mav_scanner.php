<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Support";
  $moduleLabel = "MAV Scanner";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {

    function display_mav_section($list, $header, $value_label, $search_param)
    {
      global $out, $script_name;

      $out .= "<h5>" . $header . "</h5>";
      $out .= "<table class=\"ink-table ink-table-condensed hover\">";
      $out .= "	<thead>";
      $out .= "		<tr>";
      $out .= "			<th>" . $value_label . "</th>";
      $out .= "			<th>Count</th>";
      $out .= "		</tr>";
      $out .= "	</thead>";
      $out .= "	</tbody>";

      foreach ($list as $key => $values)
      {
        $link = $script_name . "?action=account_search&do_search=Search&page_size=100&condensed=yes&" . $search_param . "=" . trim($values[0]);
        $out .= "	<tr onclick=\"window.open('" . $link . "', '_blank');\">";
        $out .= "		<td>" . trim($values[0]) . "</td>";
        $out .= "		<td>" . $values[1] . "</td>";
        $out .= "	</tr>";
      }

      $out .= "	</tbody>";
      $out .= "</table><br><br>";
    }

    $mav_stats = get_mav_suspects();

    display_mav_section($mav_stats["passwords"], "Common Passwords", "Password", "account_pwd");
    display_mav_section($mav_stats["lastips"], "Common Last Connect IPs", "Last Connect IP", "last_ip");
    display_mav_section($mav_stats["createips"], "Common Create IPs", "Create IP", "create_ip");
    display_mav_section($mav_stats["fireguard_passwords"], "Common Fireguard Passwords", "Fireguard Password", "fg_pwd");
    display_mav_section($mav_stats["fireguard_hints"], "Common Fireguard Hints", "Fireguard Hint", "fg_ans");
    display_mav_section($mav_stats["bank_passwords"], "Common Bank Passwords", "Bank Password", "bank_pwd");
    display_mav_section($mav_stats["bank_hints"], "Common Bank Hints", "Bank Hint", "bank_ans");
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
