<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Server";
  $moduleLabel = "Server Stats";
  $permission = "all";
  return;
}

if ($this_script == $script_name)
{
  $percent_inc = 1;
  $stats = get_server_stats(24);
  $racenum = $stats["latest"];

  $out .= "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>";
  $out .= "<div id=\"server_stats\" style=\"float: left; margin-top: 21px; margin-left: 300px;\">";
  $out .= "<p style=\"font-size: 14px;\"><strong>Server Stats</strong></p>";
  $out .= "<table border=\"0\" cells[acong=\"5\" cellpadding=\"5\"><tr>";
  $out .= "<td width=\"160\" align=\"left\">Average Users</td>";
  $out .= "<td colspan=\"2\">" . round($racenum['nAverageUser'] * $percent_inc) . "<td>";
  $out .= "</tr><tr>";
  $out .= "<td width=\"160\" align=\"left\">Max Users Online</td>";
  $out .= "<td colspan=\"2\">" . round($racenum['nMaxUser'] * $percent_inc) . "</td>";
  $out .= "</tr><tr>";
  $out .= "<td width=\"160\" align=\"left\">Online Users</td>";
  $out .= "<td colspan=\"2\">" . ( round($racenum['nBellaUser'] * $percent_inc) + round($racenum['nCoraUser'] * $percent_inc) + round($racenum['nAccUser'] * $percent_inc) ) . "<td>";
  $out .= "</tr><tr>";
  $out .= "<td width=\"160\" align=\"left\">Total Accounts</td>";
  $out .= "<td colspan=\"2\">" . $stats["accounts"] . "<td>";
  $out .= "</tr><tr>";
  $out .= "<td width=\"160\" align=\"left\">Total Characters</td>";
  $out .= "<td colspan=\"2\">" . $stats["characters"] . "</td>";
  $out .= "</tr></table>";
  $out .= "</div>";

  $racenum['nBellaUser'] = round($racenum['nBellaUser'] * $percent_inc);
  $racenum['nCoraUser'] = round($racenum['nCoraUser'] * $percent_inc);
  $racenum['nAccUser'] = round($racenum['nAccUser'] * $percent_inc);

  $out .= "<div id=\"race_stats\" style=\"width: 400px; height: 240px; float: left;\">";
  $out .= "<script type=\"text/javascript\">" . "\n";
  $out .= "google.load(\"visualization\", \"1\", {packages:[\"corechart\"]}); " . "\n";
  $out .= "google.setOnLoadCallback(drawChart_stats); " . "\n";
  $out .= "function drawChart_stats() { " . "\n";
  $out .= "	var data = google.visualization.arrayToDataTable([ " . "\n";
  $out .= "		['Race', 'Players'], " . "\n";
  $out .= "		['Bellato', " . $racenum['nBellaUser'] . "]," . "\n";
  $out .= "		['Cora', " . $racenum['nCoraUser'] . "]," . "\n";
  $out .= "		['Accretian', " . $racenum['nAccUser'] . "]" . "\n";
  $out .= "	]); " . "\n";

  $out .= "	var options = { " . "\n";
  $out .= "		title: 'Race Population', " . "\n";
  $out .= "		titleTextStyle: { color: '#555555' }, " . "\n";
  $out .= "		legend: { textStyle: { color: '#555555' } }, " . "\n";
  $out .= "		sliceVisibilityThreshold: 1/25000, " . "\n";
  $out .= "		fontName: 'Yanone Kaffeesatz', " . "\n";
  $out .= "		fontSize: 14, " . "\n";
  $out .= "		pieSliceText: 'value', " . "\n";
  $out .= "		slices: { " . "\n";
  $out .= "			0: { color: '#CC6699' }," . "\n";
  $out .= "			1: { color: '#9933CC' }," . "\n";
  $out .= "			2: { color: '#CCCCCC' }" . "\n";
  $out .= "		}" . "\n";
  $out .= "	}; " . "\n";

  $out .= "	var pie_chart = new google.visualization.PieChart(document.getElementById('race_stats')); " . "\n";
  $out .= "	pie_chart.draw(data, options); " . "\n";
  $out .= "}";
  $out .= "</script>";
  $out .= "</div>";

  $out .= "<div id=\"chart_div\" style=\"width: 1100px; height: 500px; clear: both;\"></div>";

  $out .= "<script type=\"text/javascript\">";
  $out .= "google.load(\"visualization\", \"1\", {packages:[\"corechart\"]}); ";
  $out .= "google.setOnLoadCallback(drawChart); ";
  $out .= "function drawChart() { ";
  $out .= "	var data = google.visualization.arrayToDataTable([ ";
  $out .= "		['Hour', 'Max', 'Avg'], ";

  foreach (array_reverse($stats["rows"]) as $key => $row)
  {
    $out .= "	['" . $row['Date'] . "', " . $row['nMaxUser'] . ", " . $row['nAverageUser'] . "]";

    if ($key < count($stats["rows"]) - 1)
    {
      $out .= ", ";
    }
  }

  $out .= "	]); ";
  $out .= "	var options = { ";
  $out .= "		title: 'Server Population', ";
  $out .= "		titleTextStyle: { color: '#555555' }, " . "\n";
  $out .= "		legend: { textStyle: { color: '#555555' } }, " . "\n";
  $out .= "		hAxis: {title: 'Hour'}, ";
  $out .= "		fontName: 'Yanone Kaffeesatz', " . "\n";
  $out .= "		fontSize: 14, " . "\n";
  $out .= "		colors: ['#91B1F3', '#D87962'] " . "\n";
  $out .= "	}; ";

  $out .= "	var chart = new google.visualization.ColumnChart(document.getElementById('chart_div')); ";
  $out .= "	chart.draw(data, options); ";
  $out .= "}";
  $out .= "</script>";
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
