<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Support";
  $moduleLabel = "Char Lookup";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $charserial = isset($_REQUEST['charserial']) && ctype_digit($_REQUEST['charserial']) ? intval($_REQUEST['charserial']) : 0;
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $do_search = isset($_GET['do_search']) ? $_GET['do_search'] : "";
    $enable_exit = false;
    if (empty($page))
    {
      $out .= "<table class=\"ink-form\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\" style=\"font-size: 12pt;\">";
      $out .= "<form class=\"ink-form\" method=\"get\" action=\"" . $script_name . "?action=support_charlookup\">";
      $out .= "<tr>";
      $out .= "<td colspan=\"2\" style=\"\"><b>Look up a user</b></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<tr>";
      $out .= "<td>Account Name:</td>";
      $out .= "<td ><input type=\"text\" name=\"account_name\" size=\"40\"/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Account Serial:</td>";
      $out .= "<td ><input type=\"text\" name=\"account_serial\" size=\"40\"/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Character Serial:</td>";
      $out .= "<td ><input type=\"text\" name=\"account_charserial\" size=\"40\"/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Character Name:<br/><span style=\"font-size: 10pt;\">Use % as a wild card. <b>DO NOT MAKE THE SEARCH TOO GENERAL!</b></span></td>";
      $out .= "<td ><input type=\"text\" name=\"account_char\" size=\"40\"/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Delete Name:<br/><span style=\"font-size: 10pt;\">Use % as a wild card. <b>DO NOT MAKE THE SEARCH TOO GENERAL!</b></span></td>";
      $out .= "<td ><input type=\"text\" name=\"account_delchar\" size=\"40\"/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td colspan=\"2\"><input type=\"hidden\" name=\"action\" value=\"" . $_GET['action'] . "\"><input class=\"ink-button\" type=\"submit\" value=\"Search\" name=\"do_search\" style=\"margin-left: 0px;\"/></td>";
      $out .= "</tr>";
      $out .= "</table>";
      $out .= "</form>";
      if ($do_search != "")
      {
        $out .= "<br/><br/>";
        $out .= "<table class=\"tborder\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
        $out .= "<tr>";
        $out .= "<td class=\"thead\"style=\"padding: 4px;\" nowrap><b>C Ser</b></td>";
        $out .= "<td class=\"thead\"style=\"padding: 4px;\" nowrap><b>A Ser</b></td>";
        $out .= "<td class=\"thead\"style=\"padding: 4px;\" nowrap><b>A Name</b></td>";
        $out .= "<td class=\"thead\"style=\"padding: 4px;\" nowrap><b>Char Name</b></td>";
        $out .= "<td class=\"thead\"style=\"padding: 4px;\" nowrap><b>Del Name</b></td>";
        $out .= "<td class=\"thead\"style=\"padding: 4px;\" nowrap><b>Level</b></td>";
        $out .= "<td class=\"thead\"style=\"padding: 4px;\" nowrap><b>Guild</b></td>";
        $out .= "<td class=\"thead\"style=\"padding: 4px;\" nowrap><b>Create Time</b></td>";
        $out .= "<td class=\"thead\"style=\"padding: 4px;\" nowrap><b>Last Conn Time</b></td>";
        $out .= "<td class=\"thead\"style=\"padding: 4px;\" nowrap><b>Options</b></td>";
        $out .= "</tr>";
        $account_serial = isset($_GET['account_serial']) && ctype_digit($_GET['account_serial']) ? intval($_GET['account_serial']) : 0;
        $account_name = isset($_GET['account_name']) ? $_GET['account_name'] : "";
        $account_char = isset($_GET['account_char']) ? $_GET['account_char'] : "";
        $account_delchar = isset($_GET['account_delchar']) ? $_GET['account_delchar'] : "";
        $account_charserial = isset($_GET['account_charserial']) && ctype_digit($_GET['account_charserial']) ? intval($_GET['account_charserial']) : 0;
        if ($account_serial == 0 && $account_name == "" && $account_char == "" && $account_delchar == "" && $account_charserial == 0)
        {
          $enable_exit = true;
          $out .= "<p align='center'><b>Sorry, make sure you filled in either the name or account serial or character name or deleted char name</b></p>";
          $out .= "</table>";
        }
        if ($account_name != "")
        {
          $attempt = get_account_serial($account_name);
          $account_serial = $attempt["serial"];
          $out .= $attempt["errorMessage"];
        }
        if ($account_char != "")
        {
          if (!preg_match("/%/", $account_char))
          {
            $search_query = "B.Name = '" . $account_char . "'";
          }
          else
          {
            $search_query = "B.Name LIKE '" . str_replace("'", "''", $account_char) . "'";
          }
        }
        else if ($account_delchar != "")
        {
          if (!preg_match("/%/", $account_delchar))
          {
            $search_query = "B.DeleteName = '" . $account_delchar . "'";
          }
          else
          {
            $search_query = "B.DeleteName LIKE '" . str_replace("'", "''", $account_delchar) . "'";
          }
        }
        else if ($account_charserial != 0)
        {
          $search_query = "B.Serial = '" . $account_charserial . "'";
        }
        else
        {
          $search_query = "B.AccountSerial = '" . $account_serial . "'";
        }
        if ($enable_exit != true)
        {
          $account_serial = antiject($account_serial);
          $account_name = antiject($account_name);
          $attempt = search_characters($search_query);

          foreach ($attempt["rows"] as $key => $row)
          {
            $charname = $row['Name'];
            $icons = "";

            if ($row['DCK'] == 1)
            {
              $icons .= "<a class=\"icon_link " . BASE_COLOR . "\" href=\"" . $_SERVER['REQUEST_URI'] . "&page=restore&charserial=" . $row['Serial'] . "\"><i class=\"icon-magic icon-large\" title=\"Restore Character\"></i></a>";
            }
            else
            {
              $icons .= "<a class=\"icon_link " . BASE_COLOR . "\" href=\"" . $_SERVER['REQUEST_URI'] . "&page=delete&charserial=" . $row['Serial'] . "\"><i class=\"icon-trash icon-large\" title=\"Delete Character\"></i></a>";
            }
            $icons .= "<a class=\"icon_link " . BASE_COLOR . "\" href=\"" . $_SERVER['REQUEST_URI'] . "&page=edit&charserial=" . $row['Serial'] . "\"><i class=\"icon-wrench icon-large\" title=\"Edit Character\"></i></a>";
            $icons .= "<a class=\"icon_link " . BASE_COLOR . "\" href=\"" . $_SERVER['REQUEST_URI'] . "&page=delete_npc&charserial=" . $row['Serial'] . "\"><i class=\"icon-eraser icon-large\" title=\"Delete NpcData Row\"></i></a>";
            $icons .= "<a class=\"icon_link " . BASE_COLOR . "\" href=\"" . $script_name . "?action=manage_user_credits&search_fun=search&account_name=" . $row['Account'] . "\"><i class=\"icon-dollar icon-large\" title=\"Manage Credits\"></i></a>";

            $lastconntime = $row['LastConnTime'];
            if (0 < $lastconntime)
            {
              if (strlen($lastconntime) <= 9)
              {
                $lastconntime = "0" . $lastconntime;
                $prepend_etime = "20";
              }
              else
              {
                $prepend_etime = "20";
              }
              $lastconntime = str_split($lastconntime, 2);
              $lastconntime = @gmmktime(@$lastconntime[3], @$lastconntime[4], 0, @ltrim(@$lastconntime[1], "0"), @$lastconntime[2], @$prepend_etime . @$lastconntime[0]);
              $lastconntimex = $lastconntime;
              if (isset($config['gamecp_logs_url']) && !empty($config['gamecp_logs_url']) && $config['gamecp_logs_url'] != " ")
              {
                $generate_item_url = $config['gamecp_logs_url'] . "?";
                $generate_item_url .= "y=" . date("Y", $lastconntime) . "&";
                $generate_item_url .= "m=" . date("m", $lastconntime) . "&";
                $generate_item_url .= "d=" . date("d", $lastconntime) . "&";
                $generate_item_url .= "h=" . date("G", $lastconntime) . "&";
                $generate_item_url .= "serial=" . $row['Serial'];
                $lastconntimex = date("M d Y h:iA", $lastconntimex);
                $lastconntime = "<a href=\"" . $generate_item_url . "\" target=\"logs\">" . $lastconntimex . "</a>";
              }
              else
              {
                $lastconntime = date("M d Y h:iA", $lastconntimex);
              }
            }
            else
            {
              $lastconntime = "--";
            }
            if (isset($row['GuildSerial']) && $row['GuildSerial'] != "*")
            {
              $attempt = get_guild_name($row['GuildSerial']);
              $guild = $attempt["id"];
            }
            $attempt2 = get_classes();
            $base_class = "";
            $first_class = "";
            $second_class = "";
            $third_class = "";
            foreach ($attempt2["rows"] as $key => $classinfo)
            {
              $race = substr($classinfo['class_code'], 0, 1);
              if ($race == "B")
              {
                $classinfo['class_name'] = "Bell " . $classinfo['class_name'];
              }
              else if ($race == "A")
              {
                $classinfo['class_name'] = "Accretian " . $classinfo['class_name'];
              }
              else if ($race == "C")
              {
                $classinfo['class_name'] = "Cora " . $classinfo['class_name'];
              }
              $base_class = $base_class == "" ? $row['Class0'] == $classinfo['class_id'] ? $classinfo['class_name'] : "" : $base_class;
              $first_class = $first_class == "" ? $row['Class1'] == $classinfo['class_id'] ? $classinfo['class_name'] : "" : $first_class;
              $second_class = $second_class == "" ? $row['Class'] == $classinfo['class_code'] ? $classinfo['class_name'] : "" : $second_class;
              $third_class = $third_class == "" ? $row['Class2'] == $classinfo['class_id'] ? $classinfo['class_name'] : "" : $third_class;
            }
            $xyz = $row['X'] . " " . $row['Y'] . " " . $row['Z'];
            if (!preg_match("/\\./", $xyz))
            {
              $xyz = "<span style=\"color: red; font-weight: bold;\">" . $xyz . "</span>";
            }
            $out .= "<tr>";
            $out .= "<td style=\"font-size: 12pt;\" nowrap>" . $row['Serial'] . "</td>";
            $out .= "<td style=\"font-size: 12pt;\" nowrap>" . $row['AccountSerial'] . "</td>";
            $out .= "<td style=\"font-size: 12pt;\" nowrap>" . $row['Account'] . "</td>";
            $out .= "<td style=\"font-size: 12pt;\" nowrap>" . $row['Name'] . "</td>";
            $out .= "<td style=\"font-size: 12pt;\" nowrap>" . $row['DeleteName'] . "</td>";
            $out .= "<td style=\"font-size: 12pt;\" nowrap>" . $row['Lv'] . "</td>";
            $out .= "<td style=\"font-size: 12pt;\" nowrap>" . ( isset($guild) ? "<a href=\"" . $script_name . "?action=support_guild_search&amp;guild_serial=" . $row['GuildSerial'] . "&amp;search_fun=Search\">" . $guild . "</a> (" . $row['GuildSerial'] . ")" : "*" ) . "</td>";
            $out .= "<td style=\"font-size: 12pt;\" nowrap>" . $row['CreateTime'] -> format('M d Y H:iA') . "</td>";
            $out .= "<td style=\"font-size: 12pt;\" nowrap>" . $lastconntime . "</td>";
            $out .= "<td style=\"font-size: 12pt;\" nowrap>" . $icons . "</td>";
            $out .= "</tr>";
          }
          $out .= "</table>";
          gamecp_log(0, $userdata -> username, "ADMIN - CHAR LOOK UP - Searched for: {$account_name} or {$account_serial} or {$account_char} or {$account_delchar}", 1);
        }
      }
    }
    else
    {
      if ($page == "delete")
      {
        if ($charserial != 0)
        {
          $attempt = delete_character($charserial);
          if ($attempt["error"] == True)
          {
            $out .= get_notification_html($attempt["errorMessage"], ERROR);
          }
          else
          {
            $out .= get_notification_html("Deleted Character", SUCCESS);
            gamecp_log(4, $userdata -> username, "ADMIN - DELETE CHARACTER - Character Serial: {$charserial}", 1);
          }
        }
        $redirect = PREVIOUS_PAGE_SHORT;
      }
      else if ($page == "restore")
      {
        if ($charserial != 0)
        {
          $attempt = restore_character($charserial);
          if ($attempt["error"] == True)
          {
            $out .= get_notification_html($attempt["errorMessage"], ERROR);
          }
          else
          {
            $out .= get_notification_html("Restored Character", SUCCESS);
            gamecp_log(3, $userdata -> username, "ADMIN - RESTORE CHARACTER - Character Serial: {$charserial}", 1);
          }
        }
        $redirect = PREVIOUS_PAGE_SHORT;
      }
      else if ($page == "delete_npc")
      {
        if ($charserial != 0)
        {
          $attempt = delete_char_npcdata($charserial);
          if ($attempt["error"] == True)
          {
            $out .= get_notification_html($attempt["errorMessage"], ERROR);
          }
          else
          {
            $out .= get_notification_html("Deleted NpcData for serial: " . $charserial, SUCCESS);
            gamecp_log(4, $userdata -> username, "ADMIN - DELETE CHAR_NPCDATA - Character Serial: {$charserial}", 1);
          }
        }
        $redirect = PREVIOUS_PAGE_SHORT;
      }
      else if ($page == "edit")
      {
        if ($charserial != 0)
        {
          $attempt = get_character_full_info($charserial);
          if ($attempt["error"] == True)
          {
            $out .= get_notification_html($attempt["errorMessage"], ERROR);
          }
          else if (isset($_POST["edit_submit"]))
          {
            $dck = isset($_POST['dck']) ? 1 : 0;
            $name = isset($_POST['name']) ? $_POST['name'] : "";
            $account_serial = isset($_POST['account_serial']) && ctype_digit($_POST['account_serial']) ? intval($_POST['account_serial']) : "";
            $account = isset($_POST['account']) ? $_POST['account'] : "";
            $map = isset($_POST['map']) && ctype_digit($_POST['map']) ? intval($_POST['map']) : "";
            $slot = isset($_POST['slot']) && ctype_digit($_POST['slot']) ? intval($_POST['slot']) : "";
            $race = isset($_POST['race']) && ctype_digit($_POST['race']) ? intval($_POST['race']) : "";
            $base_class = isset($_POST['base_class']) && $_POST['race'] ? $_POST['base_class'] : "";
            $first_class = isset($_POST['first_class']) && $_POST['first_class'] ? $_POST['first_class'] : "";
            $second_class = isset($_POST['second_class']) && $_POST['second_class'] ? $_POST['second_class'] : "";
            $final_class = isset($_POST['final_class']) ? $_POST['final_class'] : "";
            $level = isset($_POST['level']) && ctype_digit($_POST['level']) ? intval($_POST['level']) : "";
            $gold = isset($_POST['gold']) && ctype_digit($_POST['gold']) ? intval($_POST['gold']) : "";
            $dalant = isset($_POST['dalant']) && ctype_digit($_POST['dalant']) ? intval($_POST['dalant']) : "";
            $baseshape = isset($_POST['baseshape']) && ctype_digit($_POST['baseshape']) ? intval($_POST['baseshape']) : "";
            $maxlevel = isset($_POST['MaxLevel']) && ctype_digit($_POST['MaxLevel']) ? intval($_POST['MaxLevel']) : "";
            $exp = isset($_POST['Exp']) && ctype_digit($_POST['Exp']) ? $_POST['Exp'] : "";
            $lossexp = isset($_POST['LossExp']) && ctype_digit($_POST['LossExp']) ? $_POST['LossExp'] : "";
            $pvppoint = isset($_POST['pvppoint']) && ctype_digit($_POST['pvppoint']) ? $_POST['pvppoint'] : "";
            $guildserial = isset($_POST['guildserial']) && ctype_digit($_POST['guildserial']) ? intval($_POST['guildserial']) : "";
            $totalplaymin = isset($_POST['totalplaymin']) && ctype_digit($_POST['totalplaymin']) ? intval($_POST['totalplaymin']) : "";

            if (2000000000 < $dalant)
            {
              $dalant = 2000000000;
            }
            if (500000 < $gold)
            {
              $gold = 500000;
            }
            if ($dalant < 0)
            {
              $dalant = 0;
            }
            if ($gold < 0)
            {
              $gold = 0;
            }

            $write_log = "Name: {$name} | Account Serial: {$account_serial} | Account: {$account} | Level: {$level} | MaxLevel: {$maxlevel} | Gold: {$gold} | Dalant: {$dalant} | PVP Point: {$pvppoint} | Total Play Min: {$totalplaymin} | Base Class: {$base_class} | First Sub Class: {$first_class} | Second Sub Class: {$second_class} | Final Class: {$final_class}";

            update_character($charserial, $dck, $name, $account_serial, $account, $map, $slot, $race, $base_class, $first_class, $second_class, $final_class, $level, $gold, $dalant, $maxlevel, $baseshape, $exp, $lossexp, $pvppoint, $guildserial, $totalplaymin);

            delete_char_npcdata($charserial);

            $out .= get_notification_html("Successfully updated character", SUCCESS);
            gamecp_log(0, $userdata -> username, "ADMIN - CHARACTER EDIT - Updated: (" . $write_log . ")", 1);
            $redirect = PREVIOUS_PAGE_SHORT;
          }
          else
          {
            $row = $attempt["char"];
            $dck_checked = "";
            if ($row['DCK'] == 1)
            {
              $dck_checked = "checked";
            }

            $classes = get_classes();
            $classes = $classes["classes"];
            $base_class = "<select name=\"base_class\">";
            $first_class = "<select name=\"first_class\">";
            $second_class = "<select name=\"second_class\">";
            $final_class = "<select name=\"final_class\">";
            $base_class .= "<option value=\"-1\">Not yet selected</option>";
            $first_class .= "<option value=\"-1\">Not yet selected</option>";
            $second_class .= "<option value=\"-1\">Not yet selected</option>";

            foreach ($classes as $id => $class_info)
            {
              $class_code_id = -1;
              $class_selected = "";
              $class_base_selected = "";
              $class_first_selected = "";
              $class_second_selected = "";
              $current_race = substr($class_info["class_code"], 0, 1);
              $current_race2 = strtolower(substr($class_info["class_code"], 2, 2));

              if ($row["Class"] == $class_info["class_code"])
              {
                $class_selected = "selected";
                $class_code_id = $id;
              }

              if ($row["Class0"] == $class_info["class_id"])
              {
                $class_base_selected = "selected";
              }
              if ($row["Class1"] == $class_info["class_id"])
              {
                $class_first_selected = "selected";
              }
              if ($row["Class2"] == $class_info["class_id"])
              {
                $class_second_selected = "selected";
              }

              if ($current_race == "B")
              {
                $class_info['class_name'] = "Bell " . $class_info['class_name'];
              }
              else if ($current_race == "C")
              {
                $class_info['class_name'] = "Cora " . $class_info['class_name'];
              }
              else if ($current_race == "A")
              {
                $class_info['class_name'] = "Accretian " . $class_info['class_name'];
              }

              if ($current_race2 == "b0")
              {
                $base_class .= "<option value=\"" . $class_info['class_id'] . "\" " . $class_base_selected . ">" . $class_info['class_name'] . "</option>";
              }
              if ($current_race2 != "b0")
              {
                $first_class .= "<option value=\"" . $class_info['class_id'] . "\" " . $class_first_selected . ">" . $class_info['class_name'] . "</option>";
                $second_class .= "<option value=\"" . $class_info['class_id'] . "\" " . $class_second_selected . ">" . $class_info['class_name'] . "</option>";
              }

              $final_class .= "<option value=\"" . $class_info['class_code'] . "\" " . $class_selected . ">" . $class_info['class_name'] . "</option>";
            }
            $final_class .= "</select>";
            $second_class .= "</select>";
            $first_class .= "</select>";
            $base_class .= "</select>";

            $race = "<select name=\"race\">";
            $races = array ("Belleto Male", "Belleto Female", "Cora Male", "Cora Female",
              "Accretian");
            $i = 0;
            while ($i < 5)
            {
              $race_selected = "";
              if ($row['Race'] == $i)
              {
                $race_selected = "selected";
              }

              $race .= "<option value=\"" . $i . "\" " . $race_selected . ">" . $races[$i] . "</option>";
              ++$i;
            }
            $race .= "</select>";

            $attempt = get_account_serial($row['Account']);
            $account_serial = $attempt["serial"] != -1 ? $attempt["serial"] : "No such account";

            $out .= "<form class=\"ink-form\" method=\"post\">";
            $out .= "<table class=\"\" width=\"100%\">";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold; width:200px;\" nowrap>Serial</td>";
            $out .= "<td width=\"100%\">" . $row['Serial'] . "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>DCK</td>";
            $out .= "<td><input name=\"dck\" type=\"checkbox\" " . $dck_checked . "/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Name</td>";
            $out .= "<td><input name=\"name\" type=\"text\" value=\"" . $row['Name'] . "\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Account Serial</td>";
            $out .= "<td><input name=\"account_serial\" type=\"text\" value=\"" . $row['AccountSerial'] . "\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Account</td>";
            $out .= "<td><input name=\"account\" type=\"text\" value=\"" . $row['Account'] . "\"/> (Serial: " . $account_serial . ")</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Slot</td>";
            $out .= "<td><input name=\"slot\" type=\"text\" value=\"" . $row['Slot'] . "\" size=\"2\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Level</td>";
            $out .= "<td><input name=\"level\" type=\"text\" value=\"" . $row['Lv'] . "\" size=\"2\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Max Level</td>";
            $out .= "<td><input name=\"MaxLevel\" type=\"text\" value=\"" . $row['MaxLevel'] . "\" size=\"2\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Exp</td>";
            $out .= "<td><input name=\"Exp\" type=\"text\" value=\"" . sprintf("%.0f", $row['Exp']) . "\" size=\"40\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap disabled>Lost Exp</td>";
            $out .= "<td><input name=\"LossExp\" type=\"text\" value=\"" . $row['LossExp'] . "\" size=\"20\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Map</td>";
            $out .= "<td>";
            $out .= "<select name=\"map\">";

            $maps = get_maps();
            $maps = $maps["maps"];
            foreach ($maps as $map_id => $map_info)
            {
              $selected = "";
              if ($map_id == $row['Map'])
              {
                $selected = " selected=\"selected\"";
              }

              $out .= "<option value=\"" . $map_id . "\"" . $selected . ">" . $map_info["map_name"] . "</option>";
            }

            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Race</td>";
            $out .= "<td>" . $race . "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Base Class</td>";
            $out .= "<td>" . $base_class . "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>1st Sub Class</td>";
            $out .= "<td>" . $first_class . " - This class is your 1st up</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>2nd Sub Class</td>";
            $out .= "<td>" . $second_class . " - This is NOT the 2nd up class. It is an un-used class that allows REAL cross-classing</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Final Class</td>";
            $out .= "<td>" . $final_class . " - This is the final or 2nd class up. If no 1st class is selected, this is the base class</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Gold</td>";
            $out .= "<td><input name=\"gold\" type=\"text\" value=\"" . $row['Gold'] . "\" size=\"12\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Dalant</td>";
            $out .= "<td><input name=\"dalant\" type=\"text\" value=\"" . $row['Dalant'] . "\" size=\"12\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Melee PT</td>";
            $out .= "<td><input name=\"melee_pt\" type=\"text\" value=\"" . $row['WM0'] . "\" size=\"12\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Range PT</td>";
            $out .= "<td><input name=\"melee_pt\" type=\"text\" value=\"" . $row['WM1'] . "\" size=\"12\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Defence PT</td>";
            $out .= "<td><input name=\"melee_pt\" type=\"text\" value=\"" . $row['DM'] . "\" size=\"12\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Shield PT</td>";
            $out .= "<td><input name=\"melee_pt\" type=\"text\" value=\"" . $row['PM'] . "\" size=\"12\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Base Shape</td>";
            $out .= "<td><input name=\"baseshape\" type=\"text\" value=\"" . $row['Baseshape'] . "\" size=\"6\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>PVP Points</td>";
            $out .= "<td><input name=\"pvppoint\" type=\"text\" value=\"" . $row['PvpPoint'] . "\" size=\"6\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Guild Serial</td>";
            $out .= "<td><input name=\"guildserial\" type=\"text\" value=\"" . $row['GuildSerial'] . "\" size=\"6\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td style=\"padding: 4px; font-weight: bold;\" nowrap>Total Play Min</td>";
            $out .= "<td><input name=\"totalplaymin\" type=\"text\" value=\"" . $row['TotalPlayMin'] . "\" size=\"6\"/></td>";
            $out .= "</tr>";
            $out .= "<input type=\"hidden\" name=\"char_serial\" value=\"" . $row['Serial'] . "\"/>";
            $out .= "<input type=\"hidden\" name=\"page\" value=\"edit\"/>";
            $out .= "<tr>";
            $out .= "<td colspan=\"2\"><input class=\"ink-button\" type=\"submit\" name=\"edit_submit\" value=\"Update\"/></td>";
            $out .= "</tr>";
            $out .= "</table>";
            $out .= "</form>";
          }
        }
        else
        {
          $out .= get_notification_html("Invalid Character Serial", ERROR);
          $redirect = PREVIOUS_PAGE_SHORT;
        }
      }
      else
      {
        $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
        $redirect = PREVIOUS_PAGE_SHORT;
      }
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
