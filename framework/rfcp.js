function center_pager()
{
  var temp = 0;
  if ($.browser.mozilla)
    temp = 2;
  $(".main_pager").find("li").each(function (index, element) {
    temp += $(element).width();
  });
  $(".main_pager").width(temp + 10);
}

function center_pills()
{
  var temp = 0;
  $(".module_nav").find("li").each(function (index, element) {
    temp += $(element).width() + 15;
  });
  $(".module_nav").width(temp);
}

function normalize_category_height()
{
  var maxHeight = 0;
  $(".category_box").each(function (index, element) {
    if ($(element).outerHeight() > maxHeight)
      maxHeight = $(element).outerHeight();
  });
  $(".category_box").height(maxHeight);

  $(".category_box").each(function (index, element) {
    var textElement = $(element).find(".category_text");
    var currentWidth = $(textElement).width();
    $(textElement).width(900);
    var textHeight = $(textElement).outerHeight();
    var diff = $(element).outerHeight() - textHeight;
    $(textElement).width(currentWidth);

    $(element).attr("style", $(element).attr("style") + " padding-top:" + (diff / 2).toString() + "px !important;");
  });
}

function onSlotsChanged(element)
{
  var targetDiv = $(element).parent().parent();
  var slots = parseInt($(targetDiv).find(".item_slots").val());
  var talics = $(targetDiv).find(".talic");
  var index = -1;

  if (multiTalicSelectors)
    index = parseInt($(targetDiv).closest(".item_give_row").attr("id").split("_")[1]);

  if (slots < talics.length)
  {
    var elementsToRemove = [];
    for (var iX = slots; iX < talics.length; iX++)
    {
      $(talics[iX]).msDropDown().data("dd").destroy();
      elementsToRemove.push(talics[iX]);
    }
    $(elementsToRemove).remove();
  } else if (slots > talics.length)
  {
    for (var iX = talics.length; iX < slots; iX++)
    {
      var newElement = $(getTalicHTML(index));
      $(targetDiv).append(newElement);
      $(newElement).msDropdown(
      {
        on: {
          contextmenu: onTalicContextMenu
        }
      });
    }
  }
}

function onTalicChange(element)
{
  var targetDiv = $(element).parent().parent();
  var oDropdown = $(element).msDropdown().data("dd");
  var talics = $(targetDiv).find(".talic");
  var selectedVal = $(element).val();
  if (selectedVal != 15)
  {
    $.each(talics, function (index, talic) {
      if (talic != element && $(talic).val() == 15)
      {
        $(talic).msDropdown().data("dd").set("value", selectedVal);
      } else if (talic == element)
      {
        return false;
      }
    });
  } else if (selectedVal == 15)
  {
    var found = false;
    $.each(talics, function (index, talic) {
      if (found && $(talic).val() != 15)
      {
        $(talic).msDropdown().data("dd").set("value", 15);
      }
      if (element == talic)
      {
        found = true;
      }
    });
  }
}

function onTalicContextMenu(type, event)
{
  var event = event || this.event || window.event;
  target_element = $(event.target.parentElement.parentElement.parentElement).prev().find("select");
  var oDropdown = $(target_element).msDropdown().data("dd");
  oDropdown.set("value", 15);

  if (event.stopPropagation)
    event.stopPropagation();
  if (event.cancelBubble)
    event.cancelBubble = true;
  if (event.preventDefault)
    event.preventDefault();
  if (event.returnValue)
    event.returnValue = false;

  onTalicChange(target_element[0]);

  return false;
}

var talicsToDisplay = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15];
var multiTalicSelectors = false;
function getTalicHTML(index)
{
  var extra = multiTalicSelectors ? "[" + index + "][]" : "[]";
  var HTML = "";
  HTML += "<select class=\"talic\" name=\"talics" + extra + "\" style=\"width:18px;\" onchange=\"onTalicChange(this);\">";

  $.each(talicsToDisplay, function (index, talic) {
    var talic_index = talic.toString();
    var selected = "";
    if (talic < 10)
      talic_index = "0" + talic_index;
    if (talic == 15)
      selected = " selected=selected";
    HTML += "<option value=\"" + talic.toString() + "\" data-image=\"./images/talics/t-" + talic_index + ".png\" data-title=\"" + get_talic_name(talic) + "\"" + selected + "></option>";
  });

  HTML += "</select>";
  return HTML;
}

function get_talic_name(id)
{
  switch (id)
  {
    case 0:
      return "Ignorant";
      break;
    case 1:
      return "Destruction";
      break;
    case 2:
      return "Darkness";
      break;
    case 3:
      return "Chaos";
      break;
    case 4:
      return "Hatred";
      break;
    case 5:
      return "Favor";
      break;
    case 6:
      return "Wisdom";
      break;
    case 7:
      return "Sacred Flame";
      break;
    case 8:
      return "Belief";
      break;
    case 9:
      return "Guard";
      break;
    case 10:
      return "Glory";
      break;
    case 11:
      return "Grace";
      break;
    case 12:
      return "Mercy";
      break;
    case 13:
      return "Rebirth";
      break;
    case 15:
      return "No Talic";
      break;
    default:
      return "";
      break;
  }
}

function activate_tooltips()
{
  $(document).ready(function () {
    Ink.requireModules(['Ink.UI.Tooltip_1'], function (Tooltip) {
      new Tooltip('.tooltip', {where: 'down', fade: .2, delay: .2, color: 'blue'})
    });
  });
}


/*
 *
 *	Shopping Cart
 *
 */
var available_novus_credits = 0;
var checkout_options = {
  type: "SendForm",
  url: null,
  character: null
}
marketPage = '';

function initialize_cart(url, guild)
{
  checkout_options.url = url;

  simpleCart({
    cartColumns: [
      //A custom cart column for putting the quantity and increment and decrement items in one div for easier styling.
      {view: function (item, column) {
          return	"<span>" + item.get('quantity') + "</span>" +
          "<div>" +
          "<a href='javascript:;' class='simpleCart_increment'><img src='framework/img/increment.png' title='+1' alt='arrow up'/></a>" +
          "<a href='javascript:;' class='simpleCart_decrement'><img src='framework/img/decrement.png' title='-1' alt='arrow down'/></a>" +
          "</div>";
        }, attr: 'custom'},
      //Name of the item
      {attr: "name", label: false},
      //Subtotal of that row (quantity of that item * the price)
      {view: 'currency', attr: "total", label: false}
    ],
    cartStyle: 'div',
    checkout: checkout_options
  });

  simpleCart.currency({
    code: guild ? "GC" : "NC",
    symbol: guild ? " GC" : " NC",
    name: guild ? "Guild Credits" : "Novus Credits",
    after: true
  });

  simpleCart.bind("beforeAdd", cart_before_add);
  simpleCart.bind("beforeIncrement", cart_before_add_check);
  simpleCart.bind("beforeCheckout", cart_before_checkout);

  simpleCart.bind("update", check_prices);

  $(".cart_info").click(function (element) {
    if ($($(".cart_info")).hasClass("open"))
    {
      $("#cartPopover").hide();
      $($(".cart_info")).removeClass("open");
    } else
    {
      $("#cartPopover").show();
      $($(".cart_info")).addClass("open");
    }
  });

  if (simpleCart.total() > available_novus_credits){
    simpleCart.empty();
  }
}

function cart_before_add(item)
{
  var item_id = item.get("rfcp_id");

  if (cart_before_add_check(item))
  {
    $("#market_item_image_" + item_id).animate_from_to(".cart_info", {
      pixels_per_second: 1700,
      square: "height",
      initial_css: {
        image: $("#market_item_image_" + item_id).attr("src") || ""
      }
    });
    return true;
  }

  return false;
}

function cart_before_add_check(item)
{
  var currentTotal = simpleCart.total();
  var itemPrice = item.get("price");

  if (itemPrice + currentTotal > available_novus_credits)
    return false;
  else
    return true;
}

function cart_before_checkout(data)
{
  $("#cart_error").hide();

  var items = simpleCart.items();
  var char_serial = parseInt($("#character_select").val().split("_")[0]);
  var char_race = $("#character_select").val().split("_")[1];
  var items_removed = 0;

  $.each(items, function (index, item) {
    if (!char_can_purchase(item.options().item_race, char_race))
    {
      item.remove();
      items_removed++;
    } else
    {
      data["item_rfcp_id_" + (index + 1).toString()] = item.options()["rfcp_id"];
    }
  });

  data.char_serial = char_serial;

  if (items_removed > 0)
  {
    $("#cart_error").addClass("error");
    $("#cart_error").removeClass("success");
    $("#cart_error").show();
    $("#cart_error").html("Removed " + items_removed.toString() + " item" + (items_removed > 1 ? "s " : " ") +
    "from your cart that were not compatible with the character you selected.  " +
    "If you are satisfied, press \"Confirm Items\" again");
    $(".cart_confirm_button").show();
    $(".cart_checkout_button").hide();
    return false;
  } else
    return true;
}

function check_items()
{
  $("#cart_error").hide();

  var items = simpleCart.items();
  var char_serial = parseInt($("#character_select").val().split("_")[0]);
  var char_race = $("#character_select").val().split("_")[1];
  var items_removed = 0;

  $.each(items, function (index, item) {
    if (!char_can_purchase(item.options().item_race, char_race))
    {
      item.remove();
      items_removed++;
    }
  });

  if (items_removed > 0)
  {
    $("#cart_error").addClass("error");
    $("#cart_error").removeClass("success");
    $("#cart_error").show();
    $("#cart_error").html("Removed " + items_removed.toString() + " item" + (items_removed > 1 ? "s " : " ") +
    "from your cart that were not compatible with the character you selected.  " +
    "If you are satisfied, press \"Confirm Items\" again");
    $(".cart_confirm_button").show();
    $(".cart_checkout_button").hide();
  } else
  {
    $("#cart_error").removeClass("error");
    $("#cart_error").addClass("success");
    $("#cart_error").show();
    $("#cart_error").html("All items in the cart can be purchased by the selected account. " +
    "Please press \"Checkout\" to confirm your order. You will not receive a refund if you make a mistake!");
    $(".cart_confirm_button").hide();
    $(".cart_checkout_button").show();
  }
}

function char_can_purchase(item_race, char_race)
{
  if (item_race == 0)
    return true;

  switch (char_race)
  {
    case "C":
      return item_race == 2 || item_race == 4;
    case "B":
      return item_race == 1 || item_race == 4;
    case "A":
      return item_race == 3;
  }

  return false;
}

function show_cart()
{
  $(".item_market_wrapper").hide();

  var HTML = "";
  HTML += "<button class=\"ink-button\" onclick=\"hide_cart();\">Return to Market</button><br/>";
  HTML += "<div class=\"simpleCart_items\"></div>";
  $(".item_market_cart_wrapper").html(HTML);
  $(".item_market_cart_wrapper").show();
  simpleCart.writeCart(".simpleCart_items");
}

function hide_cart()
{
  $(".item_market_wrapper").show();
  $(".item_market_cart_wrapper").hide();
}

function check_prices()
{
  $(".cart_confirm_button").show();
  $(".cart_checkout_button").hide();
  $("#cart_error").hide();
  var usable_credits = available_novus_credits - simpleCart.total();
  $(".item_price").each(function (key, element) {
    var price = parseFloat($(element).text());
    if (price < usable_credits)
      $(element).removeClass("not_enough");
    else if (price > usable_credits && !$(element).hasClass("not_enough"))
      $(element).addClass("not_enough");
  });
}

function addGiveItemField() {

  var max_row_id = $("#max_row_id").val();
  var index = parseInt(max_row_id.split("_")[1]) + 1;
  var new_row_id = "row_" + index;
  var html = $("#" + max_row_id).html();
  var re = new RegExp("results_" + max_row_id, "g");
  html = html.replace(re, "results_" + new_row_id);

  var newElement = $("<tr id='" + new_row_id + "' class='item_give_row'>" + html + "</tr>");
  $(newElement).find("script").remove();
  $(newElement).find("#results_" + new_row_id).html("-1");
  var temp = $($(newElement).find(".item_slots").parent());
  temp.find(".item_slots").val(0);
  $(newElement).find(".item_talics").html(temp);

  $("#" + max_row_id).after(newElement);
  $("#max_row_id").val(new_row_id);
}

var prev_itemname_request = null;
function check_itemname(sourceElement, targetElementId)
{
  var item_code = $(sourceElement).val();
  if (item_code.length > 0)
  {
    if (prev_itemname_request != null)
    {
      prev_itemname_request.abort();
      prev_itemname_request = null;
    }

    $.ajax({
      url: get_item_name_url,
      type: "GET",
      data: {item_code: item_code},
      success: function (data) {
        try {
          data = JSON.parse(data);

        } catch (e) {
        }
        if (data["name"])
          $("#" + targetElementId).html(data["name"]);
        else
          $("#" + targetElementId).html("-1");
        prev_itemname_request = null;
      },
      error: function (xhr, status, msg) {
        $("#" + targetElementId).html("-1");
        prev_itemname_request = null;
      }
    });
  } else
  {
    $("#" + targetElementId).html("-1");
    if (prev_itemname_request != null)
    {
      prev_itemname_request.abort();
      prev_itemname_request = null;
    }
  }
}

function on_select_armor_level()
{
  var level = $("select[name=armor_level]").val();
  $("select").each(function (index, element) {
    var name = $(element).attr("name").split("_");
    if (name && name.length > 1 && name.indexOf("acc") > -1 || name.indexOf("bell") > -1 || name.indexOf("cora") > -1)
    {
      var race = name[0];
      var search_type = "Ranger";
      var type = name[1];
      search_type = type == "war" ? "Warrior" : search_type;
      search_type = type == "for" ? "Force" : search_type;
      search_type = type == "lau" ? "Launcher" : search_type;
      var level_text = "Level " + level;
      var new_val = 0;
      switch (race)
      {
        case "acc":
          new_val = $(element).find("option[label=Accretian]:first")
          .nextAll("option[label=" + search_type + "]:first")
          .nextAll("option[label='" + level_text + "']:first").attr("value");
          $(element).val(new_val);
          break;
        case "bell":
          new_val = $(element).find("option[label=Bellato]:first")
          .nextAll("option[label=" + search_type + "]:first")
          .nextAll("option[label='" + level_text + "']:first").attr("value");
          $(element).val(new_val);
          break;
        case "cora":
          new_val = $(element).find("option[label=Cora]:first")
          .nextAll("option[label=" + search_type + "]:first")
          .nextAll("option[label='" + level_text + "']:first").attr("value");
          $(element).val(new_val);
          break;
      }
    }
  });
}

function do_vote(siteID, siteName, siteLink)
{
  var HTML = "";
  HTML += "[" + siteName + "] Account Name: <input type=\"text\" id=\"vote_account\" name=\"vote_account\" value=\"" + current_username + "\"></input>";
  HTML += "<button class=\"ink-button\" onclick=\"window.open('" + siteLink + "', '_blank'); submit_vote_initial('" + siteID + "', '" + siteLink + "');\">Vote</button>";

  $("#vote_activity").html(HTML).show();
  $("#vote_account").focus();
}

function submit_vote_initial(siteID, siteLink)
{
  var username = $("#vote_account").val();

  if (username == "")
  {
    alert("Must enter a username");
    return;
  }

  $.ajax({
    url: vote_url,
    type: "POST",
    data: {
      vote_id: siteID,
      vote_account: username
    },
    success: function (data) {
      try
      {
        data = JSON.parse(data);
        if (data["error"] == false)
          vote_display_success()
        else
          $("#vote_activity").html("<span style=\"color: red;\">" + data["errorMessage"] + "</span>");
      } catch (e)
      {
        $("#vote_activity").html("<span style=\"color: red;\">Unknown Error!</span>");
      }
    },
    error: function (xhr, status, msg) {
      $("#vote_activity").html("<span style=\"color: red;\">Unknown Error!</span>");
    }
  });

}

function submit_vote_claim()
{
  $.ajax({
    url: vote_url,
    type: "POST",
    data: {
      "do": "claim_credits"
    },
    success: function (data) {
      try
      {
        data = JSON.parse(data);
        if (data["error"] == false)
          $("#vote_activity").html("<span style=\"color: green;\">You earned " + data["credits"] + " credits!</span>");
        else
          $("#vote_activity").html("<span style=\"color: red;\">" + data["errorMessage"] + "</span>");
      } catch (e)
      {
        $("#vote_activity").html("<span style=\"color: red;\">Unknown Error!</span>");
      }
    },
    error: function (xhr, status, msg) {
      $("#vote_activity").html("<span style=\"color: red;\">Unknown Error!</span>");
    }
  });

}

function vote_display_success()
{
  var count = 10;
  var HTML = "";
  HTML += "<button class=\"ink-button\" id=\"vote_claim\">10</button>";
  HTML += "<br><span style=\"color: red;\">Please take the time to vote so that you can help increase the population.  You can claim your credits when the timer completes!</span>";

  $("#vote_activity").html(HTML).show();

  var voteTimer = setInterval(timer, 1000);

  function timer()
  {
    count = count - 1;
    if (count <= 0)
    {
      $("#vote_claim").html("Claim Credits");
      $("#vote_claim").click(submit_vote_claim);
      clearInterval(voteTimer);
      return;
    }
    $("#vote_claim").html(count);
  }
}

function view_more(id)
{
  $('.extra_' + id).fadeToggle();

  var icon = $("#view_more_" + id);
  if (icon.hasClass("icon-plus"))
  {
    icon.removeClass("icon-plus");
    icon.addClass("icon-minus");
  } else
  {
    icon.removeClass("icon-minus");
    icon.addClass("icon-plus");
  }

}

function numberWithCommas(x)
{
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function changeStatus(value, id)
{
  $("#status_message").html("");
  $.ajax
  ({
    url: 'index.php?action=ticket_ajax',
    type: 'post',
    data: {'status': value, 'id': id},
    dataType: "json",
    success: function (data) {
      if (data && data.error == false)
        $("#status_message").html("Saved").css("color", "green");
      else if (data && data.statuserror)
        $("#status_message").html(data.statuserror).css("color", "red");
    }
  });
}

function navigation(Url)
{
  document.location.href = Url;
}

function update_category(value, id)
{
  $.ajax({
    url: 'index.php?action=ticket_ajax',
    type: 'post',
    data: {'category': value, 'defaultid': id, 'page': 'update'},
    dataType: "json",
    success: function (data) {
      console.log(data.response);
    }
  });
}
function update_id(value, id)
{
  $.ajax({
    url: 'index.php?action=ticket_ajax',
    type: 'post',
    data: {'id': value, 'defaultid': id, 'page': 'update'},
    dataType: "json",
    success: function (data) {
      console.log(data.response);
    }
  });
}
function update_permission(value, id)
{
  $.ajax({
    url: 'index.php?action=ticket_ajax',
    type: 'post',
    data: {'permission': value, 'defaultid': id, 'page': 'update'},
    dataType: "json",
    success: function (data) {
      console.log(data.response);
    }
  });
}
function update_aserial(value, id)
{
  $.ajax({
    url: 'index.php?action=ticket_ajax',
    type: 'post',
    data: {'aserial': value, 'defaultaserial': id, 'page': 'update'},
    dataType: "json",
    success: function (data) {
      console.log(data.response);
    }
  });
}
function update_rank(value, id)
{
  $.ajax({
    url: 'index.php?action=ticket_ajax',
    type: 'post',
    data: {'rank': value, 'defaultaserial': id, 'page': 'update'},
    dataType: "json",
    success: function (data) {
      console.log(data.response);
    }
  });
}

function premiumPurchase()
{
  $.ajax({
    url: 'index.php?action=premium_subscribe',
    type: "POST",
    data: $("#subscribeForm").serialize(),
    dataType: "json",
    success: function (data) {
      if (data['error'] == true)
      {
        $('#subscribeError').children('p').text(data['data']);
        $('#subscribeError').show();
      } else
      {
        $('#subscribeDate').text(data['date']);
        $('#ncAmount').text(data['ncTotal']);
        $('#premiumButton').val('Extend premium service');
        $('#premiumNotice').show();
      }
    }
  });
}