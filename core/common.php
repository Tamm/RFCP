<?php

ini_set("display_errors", 1);
ini_set("track_errors", 1);
ini_set("html_errors", 1);
error_reporting(E_ALL);

if (!defined("BASE_PATH"))
{
  define("BASE_PATH", ".");
}

include( BASE_PATH . "/config/config.php" );
include( BASE_PATH . "/core/cache.php" );
include( BASE_PATH . "/core/definitions.php" );
include( BASE_PATH . "/core/helper_functions.php" );
include( BASE_PATH . "/core/ui.php" );
include( BASE_PATH . "/core/db.php" );
include( BASE_PATH . "/core/logging.php" );
include( BASE_PATH . "/core/user.php" );
require_once(BASE_PATH . "/includes/captcha/recaptchalib.php");

if (!defined("IN_GAMECP_SALT58585"))
{
  exit("Hacking Attempt");
  exit();
  return;
}

function getip($ip)
{
  if (isset($_SERVER['HTTP_CLIENT_IP']) && filter_var($ip, FILTER_VALIDATE_IP))
  {
    return $_SERVER['HTTP_CLIENT_IP'];
  }
  if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && filter_var($ip, FILTER_VALIDATE_IP))
  {
    return $_SERVER['HTTP_X_FORWARDED_FOR'];
  }
  if (isset($_SERVER['HTTP_X_FORWARDED']) && filter_var($ip, FILTER_VALIDATE_IP))
  {
    return $_SERVER['HTTP_X_FORWARDED'];
  }
  if (isset($_SERVER['HTTP_FORWARDED_FOR']) && filter_var($ip, FILTER_VALIDATE_IP))
  {
    return $_SERVER['HTTP_FORWARDED_FOR'];
  }
  if (isset($_SERVER['HTTP_FORWARDED']) && filter_var($ip, FILTER_VALIDATE_IP))
  {
    return $_SERVER['HTTP_FORWARDED'];
  }
  if (isset($_SERVER['HTTP_X_FORWARDED']) && filter_var($ip, FILTER_VALIDATE_IP))
  {
    return $_SERVER['HTTP_X_FORWARDED'];
  }
  return $_SERVER['REMOTE_ADDR'];
}

if (!isset($_SERVER['REQUEST_URI']))
{
  $_SERVER['REQUEST_URI'] = "/" . substr($_SERVER['PHP_SELF'], 1);
  if (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != "")
  {
    $_SERVER .= "REQUEST_URI";
  }
}
if (!isset($_SERVER['DOCUMENT_ROOT']) && isset($_SERVER['SCRIPT_FILENAME']))
{
  $_SERVER['DOCUMENT_ROOT'] = str_replace("\\", "/", substr($_SERVER['SCRIPT_FILENAME'], 0, 0 - strlen($_SERVER['PHP_SELF'])));
}
if (!isset($_SERVER['DOCUMENT_ROOT']) && isset($_SERVER['PATH_TRANSLATED']))
{
  $_SERVER['DOCUMENT_ROOT'] = str_replace("\\", "/", substr(str_replace("\\\\", "\\", $_SERVER['PATH_TRANSLATED']), 0, 0 - strlen($_SERVER['PHP_SELF'])));
}

$ip = getip($_SERVER['REMOTE_ADDR']);

//Load config elements from database
load_configuration();

$script_name = isset($config['gamecp_filename']) ? $config['gamecp_filename'] : "index.php";
$program_name = isset($config['gamecp_programname']) ? $config['gamecp_programname'] : "Game CP";
$super_admin = explode(",", $admin['super_admin']);
setlocale(LC_TIME, "en_US");
$out = "";
$isuser = false;
$notuser = true;
$exit_login = false;
$scripts = $_SERVER['PHP_SELF'];
$scripts = explode(chr(47), $scripts);
$this_script = $scripts[count($scripts) - 1];

$out = "";
$out_json = array ();
$out_type = OUTPUT_HTML;
$title = "";
$exit_message = "";
$userdata = new RFUser();
$redirect = "";
$redirecturl = "";

if (!isset($config['security_salt']) || empty($config['security_salt']))
{
  exit("Cannot run the script without the security_salt set to a value!");
}
if (isset($_COOKIE["rfcp_username"]) && isset($_COOKIE["rfcp_session"]))
{
  $cookie_username = $_COOKIE["rfcp_username"];
  $cookie_session = $_COOKIE["rfcp_session"];
  if (!preg_match(REGEX_USERNAME, $cookie_username))
  {
    $exit_login = true;
    $exit_message = "<p style=\"text-align: center; font-weight: bold;\">Invalid login usage supplied!</p>";
  }
  else
  {
    $session_attempt = validate_session($cookie_session, $cookie_username);
    if ($session_attempt["error"] === False)
    {
      if (is_user_banned($userdata -> get_property("serial")))
      {
        $out .= "<p style=\"text-align: center; font-weight: bold;\">Your account has been blocked! Please contact an Administrator.</p>";
      }
      else
      {
        $user_load = $userdata -> load($session_attempt["serial"]);
        if ($user_load["error"] === True)
        {
          $exit_login = true;
          $exit_message = $user_load["errorMessage"];
          exit_with_error($user_load["errorMessage"]);
        }
        else
        {
          if ($userdata -> serial == "")
          {
            exit_with_error("");
          }
          else
          {
            $isuser = true;
            $notuser = false;
          }
        }
      }
    }
  }
}

if ($isuser == true && in_array($userdata -> username, $super_admin))
{
  $is_superadmin = true;
}
else
{
  $is_superadmin = false;
}

$vote_page = get_vote_html();
