<?php

//////////////////////////////////////////////////
//	RFCP: RF Online Control Panel		        //
//	Module: config.php							//
//	Copyright (C) www.nxtdeveloper.com			//
//////////////////////////////////////////////////

if (!defined("IN_GAMECP_SALT58585"))
{
  die("Hacking Attempt");
  exit;
  return;
}

# Administrative Options
$admin = array ();
$admin['super_admin'] = 'mauro';

# Get our list of possible ban reasons
$ban_reasons = array ('Multiple Account Voting', 'Duper', 'PayPal Related', 'Insulting Players',
  'Fraud', '3 Day Ban', 'Not obeying a GM', 'Crashing the server', 'Fraud Related',
  'Multiple Account Warning', 'Misuse of Chat/Spamming', 'Miuse of Chat/Spamming more than Once',
  'Glitching Guard Towers', 'Guard Towers in Core', 'Guard Tower "Port In" Location',
  'Abusing Race Leader Powers', 'Safe Zone Debuffing/Healing', 'Animus Safe Zone Attacking',
  'Glitching Animus', 'Disrespecting a GM', 'Scamming', 'Verbally Harassing a Player',
  'Harassing a Play', 'Speed Hacking', 'Damage Hacking', 'Fly Hacking', 'Terrain Exploiting',
  'Settlement/Wharf Tower Exploiting', 'Multiple use of Third Party Programs', 'Shooting Over Crag Mine Barricades',
  'Multiple Rates Jades', 'Impersonating a GM', 'Using a Nuke in the Core', 'Real Money Trading(RMT)',
  'Partying with a Cheater', 'PayPal Restrictions Bypass', 'Conspiracy', 'Aiding a hacker',
  "Account trading or sharing", "Auto-chat abuse", "Spamming in chat", "Trading in public chat",
  'Non-English in public chat', 'Contact a GM regarding BAN', 'Piloting Accounts',
  'TEMP');
sort($ban_reasons);
$reasons_count = @count($ban_reasons);

# Configurable Variables [DON'T TOUCH IF YOU DONT KNOW WHATS GOING ON!]
$dont_allow = array (".", "..", "index.html", "pagination", "library", "libchart",
  "gamecp_license.txt", "generated", "donations_wall.php");

# Configure the user table
define("TABLE_LUACCOUNT", "tbl_RFTestAccount");  // i.e. tbl_LUAccount or tbl_rfaccount, the latter for 2.2.3+

define("EXT_STORAGE", TRUE); // TRUE if server supports Extended Storage, else FALSE.
# Max currency for a character
define("MAX_CURRENCY", 2000000000);

//blue, black, green, grey, orange, red, or white
define("BASE_COLOR", "blue");

# Database Settings (BE ADVISED: MAKE NEW USERNAMES AND PASSWORDS FOR THE GAMECP, DO NOT USE YOUR MASTER)
$mssql = array ();
$mssql['user']['host'] = '127.0.0.1,61433';
$mssql['user']['db'] = 'RF_USER';
$mssql['user']['username'] = 'gamecp';
$mssql['user']['password'] = '80QEHdupNA47yln';

$mssql['data']['host'] = '127.0.0.1,61433';
$mssql['data']['db'] = 'RF_WORLD';
$mssql['data']['username'] = 'gamecp'; // If user has only 'read' access
$mssql['data']['password'] = '80QEHdupNA47yln'; // Item Edit and Delete characters wont work

$mssql['rfcp']['host'] = '127.0.0.1,61433';
$mssql['rfcp']['db'] = 'RF_GAMECP';
$mssql['rfcp']['username'] = 'gamecp';
$mssql['rfcp']['password'] = '80QEHdupNA47yln';

$mssql['items']['host'] = '127.0.0.1,61433';
$mssql['items']['db'] = 'RF_ITEMSDB';
$mssql['items']['username'] = 'gamecp';
$mssql['items']['password'] = '80QEHdupNA47yln';

$mssql['history']['host'] = '127.0.0.1,61433';
$mssql['history']['db'] = 'RF_HISTORY';
$mssql['history']['username'] = 'gamecp';
$mssql['history']['password'] = '80QEHdupNA47yln';

$mssql['billing']['host'] = '127.0.0.1,61433';
$mssql['billing']['db'] = 'BILLING';
$mssql['billing']['username'] = 'gamecp';
$mssql['billing']['password'] = '80QEHdupNA47yln';

$armor_set_levels = array (35, 37, 39, 41, 43, 45, 47, 50, 53, 55, 57, 60, 63, 65);
$weapon_set_levels = array (35, 40, 45, 50, 55, 60, 65);
