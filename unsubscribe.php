<?php

define("IN_GAMECP_SALT58585", true);
include( "./core/common.php" );

$email = isset($_REQUEST['email']) ? antiject($_REQUEST['email']) : "";
$key = isset($_REQUEST['key']) ? antiject($_REQUEST['key']) : "";

if ($email == "" || $key == "")
{
  $out .= get_notification_html("Follow the link in the email", ERROR);
}
else
{
  $salt = $config['security_salt'];
  $generate_key = md5($email . ' ' . $salt);
  if ($generate_key == $key)
  {
    if (filter_var($email, FILTER_VALIDATE_EMAIL))
    {
      $attempt = add_optout($email);
      if ($attempt["error"] == True)
      {
        $out .= get_notification_html($attempt["errorMessage"], ERROR);
      }
      else
      {
        $out .= get_notification_html("You have successfully unsubscribed " . filter_string_for_html($email), SUCCESS);
      }
    }
    else
    {
      $out .= get_notification_html("Invalid email address", ERROR);
    }
  }
  else
  {
    $out .= get_notification_html("Use the exact link from your email", ERROR);
  }
}

gamecp_nav();
$output = gamecp_template("gamecp");
print_outputs($output);
